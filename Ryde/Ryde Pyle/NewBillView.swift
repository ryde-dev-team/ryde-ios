//
//  NewBillView.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/29/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class NewBillView: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate, UITextFieldDelegate {
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		
		NotificationCenter.default.addObserver(self, selector: #selector(NewTransactionView.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(NewTransactionView.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
	}
	
	
	@IBOutlet var swipeRight: UISwipeGestureRecognizer!
	
	@IBOutlet var billList: UIPickerView!
	
	@IBOutlet var accountList: UIPickerView!
	@IBOutlet var nameField: UITextField!
	
	@IBOutlet var dueDatePick: UIDatePicker!
	
	@IBOutlet var amountField: UITextField!
	
	@IBOutlet var hasFee: UISwitch!
	
	@IBOutlet var feeField: UITextField!
	
	@IBOutlet var shouldRemind: UISwitch!
	
	var bill: Bill!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		swipeRight.addTarget(self, action: #selector(backToBill(_:)))
		self.view.addGestureRecognizer(swipeRight)
		self.view.isUserInteractionEnabled = true
		
		billList.delegate = self
		billList.dataSource = self
		
		accountList.delegate = self
		accountList.dataSource = self
		
		nameField.delegate = self
		amountField.delegate = self
		feeField.delegate = self
		
		self.view.backgroundColor = Color.nBack
		
		if GroupData.get("billSelected") != nil {
			self.bill = User.currentBills[GroupData.get("billSelected") as! Int]
			
			var num = 0
			
			for b in User.bills {
				for t in b.transactions {
					if t.id == bill.tranId {
						nameField.text! = t.memo
						amountField.text! = Currency.format( t.value.description)!
						billList.selectRow(num, inComponent: 0, animated: false)
						dueDatePick.date = Date(timeIntervalSince1970: bill.due)
					}
				}
				num += 1
			}
			
			GroupData.clear("billSelected")
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	func numberOfComponents(in: UIPickerView) -> Int {
		return 1
	}
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		if pickerView.isEqual(billList) {
			return User.bills.count
		} else {
			return User.banks.count + User.cards.count
		}
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		if pickerView.isEqual(billList) {
			return User.bills[row].name
		} else {
			if row < User.banks.count {
				return User.banks[row].name
			} else {
				return User.cards[row - User.banks.count].name
			}
		}
	}
	
	func backToBill(_ sender: AnyObject) {
		LoadingController.setNav(6)
		self.dismiss(animated: false, completion: nil)
		
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if amountField.isEditing {
			amountField.text! = Double(Currency.remove( amountField.text!)!).description
			
			if Double(amountField.text!)! > Double(0) {
				amountField.text! = (-Double(amountField.text!)!).description
			}
			
			amountField.text! = Currency.format( amountField.text!)!
		}
		
		self.view.endEditing(true)
		return false
	}
	
	func keyboardWillShow(notification: NSNotification) {
		if nameField.isEditing {
			return
		}
		
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y == 0{
				self.view.frame.origin.y -= keyboardSize.height
			}
			else {
				
			}
		}
		
	}
	
	func keyboardWillHide(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y != 0 {
				self.view.frame.origin.y += keyboardSize.height
			}
			else {
				
			}
		}
	}
	
	@IBAction func payNow(_ sender: Any) {
		
		let accSelect = accountList.selectedRow(inComponent: 0)
		var payAcc: Account!
		
		if accSelect < User.banks.count {
			payAcc = User.banks[accSelect]
		} else {
			payAcc = User.cards[accSelect - User.banks.count]
		}
		
		if bill == nil {
			Ecosystem.logLocal("[TODO] Instant Pay")
		} else {
			Ecosystem.logLocal("Pay Bill")
			var tran: Transaction!
			
			for b in User.bills {
				for t in b.transactions {
					if t.id == bill.tranId {
						tran = t
						let newName = nameField.text!
						let newAmount = amountField.text!
						
						bill.due = dueDatePick.date.timeIntervalSince1970
						
						t.memo = newName
						t.value = Currency.remove( newAmount)!
						
						t.save()
						break
					}
				}
				if tran != nil {
					let cloneTran = Transaction(id: Database.getNextTransactionId(), time: Date().timeIntervalSince1970, value: -tran.value, memo: "Payment to " + tran.memo, cleared: 0, reconciled: 0)
					
					let bankTran = Transaction(id: cloneTran.id + 1, time: Date().timeIntervalSince1970, value: -cloneTran.value, memo: "Payment to " + tran.memo, cleared: 0, reconciled: 0)
					
	
					b.adjustBalance(tran: cloneTran)
					payAcc.adjustBalance(tran: bankTran)
					
					bill.pay()
					
					b.save()
					payAcc.save()
					bill.save()
					break
				}
			}
			
			
			
			
		}
		
		LoadingController.setNav(6)
		self.dismiss(animated: false, completion: nil)
	}
	
	@IBAction func payLater(_ sender: Any) {
		amountField.text = Currency.validate(amountField.text)
		
		if bill == nil {
		
			let selected: Int = billList.selectedRow(inComponent: 0)
			let id = User.bills[selected].id
		
			var value: Double = Currency.remove( amountField.text!)!
		
			if value > Double(0) {
				value = -value
			}
		
			let tran = Transaction(id: Database.getNextTransactionId(), time: Date().timeIntervalSince1970, value: value, memo: nameField.text!, cleared: 0, reconciled: 0)
		
			tran.setLocation(lat: User.currentLocation[0], long: User.currentLocation[1])
		
		
			User.record(tran: tran, accountId: id, categoryId: -1, config: 4)
			
			let bill = Bill(id: Database.getNextBillId(), tranId: tran.id, paid: 0, due: dueDatePick.date.timeIntervalSince1970, late: 0)
			User.currentBills.append(bill)
			bill.save()
		} else {
			Ecosystem.logLocal("Update Bill")
			
			for b in User.bills {
				for t in b.transactions {
					if t.id == bill.tranId {
						let newName = nameField.text!
						let newAmount = amountField.text!
						
						t.memo = newName
						t.value = Currency.remove( newAmount)!
						
						bill.due = dueDatePick.date.timeIntervalSince1970
						
						t.save()
						bill.save()
						break
					}
				}
			}
			
			
		}
		
		LoadingController.setNav(6)
		self.dismiss(animated: false, completion: nil)
	}
	
	
	
}

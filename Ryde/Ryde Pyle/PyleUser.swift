//
//  PyleUser.swift
//  Ryde
//
//  Created by Ryan Dean on 1/18/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import Foundation

extension User {
	static var capital: Double = 0.0 //Sum of checking and savings
	static var checking: Double = 0.0 //Sum of checking accounts
	static var savings: Double = 0.0 //SUm of savings accounts
	static var debt: Double = 0.0 //Sum of all debt and credit cards
	static var bill: Double = 0.0
	static var budget: Double = 0.0
	static var asset: Double = 0.0 //Sum of all assets
	static var usable: Double = 0.0
	static var net: Double = 0.0
	
	static var banks: [Account] = [] //A bank is created for every checking and savings accounts
	static var debts: [Account] = [] //Every debt account
	static var assets: [Account] = [] //Owned items and investment accounts
	static var cards: [Account] = [] //All credit cards
	
	static var bills: [Account] = [] //Every bill
	static var goals: [Account] = [] //All created goals
	static var budgets: [Account] = [] //All created budgets
	
	static var merchants: [Account] = [] //Collection of all past merchants
	
	static var currentBills: [Bill] = []
	
	static var minTran = 9999999
	static var maxTran = 0
	
	static var doLogout = 0
	
	static func update() {
		
		capital = 0
		checking = 0
		savings = 0
		debt = 0
		asset = 0
		bill = 0
		budget = 0
		usable = 0
		net = 0
		
		
		for b in banks {
			capital += b.balance
			if b.type == 0 {
				checking += b.balance
			} else {
				savings += b.balance
			}
		}
		
		usable += capital
		net += capital
		
		for d in debts {
			debt += d.balance
		}
		
		for c in cards {
			debt += c.balance
		}
		
		net += debt
		
		for a in assets {
			asset += a.balance
		}
		
		net += asset
		
		for bi in bills {
			//bi.checkDue()
			
			bill += bi.balance
		}
		
		net += bill
		
		for g in goals {
			budget += g.balance
		}
		
		for b in budgets {
			budget += b.balance
		}
		
		usable -= budget
		
	}
	
	static func load() {
		verifyAccess()
		Ecosystem.logLocal("Load Pyle Info")
		
		if downloadUserInfo() == false {
			Ecosystem.logLocal("Server Logout")
			logout()
		}
		
		
		banks = Database.accounts(0) + Database.accounts(1)
		debts = Database.accounts(2)
		cards = Database.accounts(3)
		budgets = Database.accounts(4)
		bills = Database.accounts(5)
		goals = Database.accounts(6)
		assets = Database.accounts(7)
		
		currentBills = Database.bills()
		
		places = Database.places()
		
		let accounts = banks + debts + cards + budgets + bills + goals + assets
		
		for a in accounts {
			a.transactions = Database.transactions(a)
		}
		
		update()
		return
	}
	
	static func save() {
		Ecosystem.logLocal("Pyle Info Saved")
		
		let accounts: [Account] = banks + debts + assets + cards + bills + goals + budgets
		
		for account in accounts {
			account.save()
		}
		
		for bill in currentBills {
			bill.save()
		}
		
	}
	
	static func addLocationName(name: String, location: [Float]){
		var found = false
		for m in merchants {
			if m.name == name {
				found = true
				//m.addLocation(location: location)
				break
			}
		}
		
		if !(found) {
			//merchants.append(Merchant(name: name,location: location))
		}
	}
	
	
	static func record(tran: Transaction, accountId: Int, categoryId: Int, config: Int) {
		Ecosystem.logLocal("Transcation Recorded")
		
		var account: Account!
		var category: Account!
		
		let accounts: [Account] = banks + debts + assets + cards + bills + goals + budgets
		
		for ac in accounts {
			if ac.id == accountId {
				account = ac
			}
			if ac.id == categoryId {
				category = ac
			}
		}
		
		if account == nil {
			print("[TODO] Make alert concerning no account")
			return
		}
		
		
		switch (config) {
		case 0:
			if category == nil {
				print("[TODO] Make alert concerning no category")
				return
			}
			print(account.name + ": " + Currency.format( tran.value.description)!)
			account.adjustBalance(tran: tran)
			
			let cloneTran = Transaction(id: tran.id + 1, time: tran.timestamp, value: tran.value, memo: tran.memo, cleared: 0, reconciled: 0)
			print(category.name + ": " + Currency.format( cloneTran.value.description)!)
			category.adjustBalance(tran: cloneTran)
			break
		case 1:
			if category == nil {
				print("[TODO] Make alert concerning no category")
				return
			}
			print(category.name + ": " + Currency.format( tran.value.description)!)
			category.adjustBalance(tran: tran)
			
			let cloneTran = Transaction(id: tran.id + 1, time: tran.timestamp, value: -tran.value, memo: tran.memo, cleared: 0, reconciled: 0)
			print(account.name + ": " + Currency.format( cloneTran.value.description)!)
			account.adjustBalance(tran: cloneTran)
			break
		case 2:
			print(account.name + ": " + Currency.format( tran.value.description)!)
			account.adjustBalance(tran: tran)
			break
		case 3:
			print(account.name + ": " + Currency.format( tran.value.description)!)
			account.adjustBalance(tran: tran)
			break
		case 4:
			account.adjustBalance(tran: tran)
		case 5:
			if category == nil {
				print("[TODO] Make alert concerning no category")
				return
			}
			category.adjustBalance(tran: tran)
			let cloneTran = Transaction(id: tran.id + 1, time: tran.timestamp, value: -tran.value, memo: tran.memo, cleared: 0, reconciled: 0)
			account.adjustBalance(tran: cloneTran)
		case 6:
			account.adjustBalance(tran: tran)
		default:
			print("INVALID CONFIG FOR USER transaction RECORD")
		}
		
		if account != nil {
			account.save()
		}
		
		if category != nil {
			category.save()
		}
	}
}

//
//  budgetingCell.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/22/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class BillingCell: UITableViewCell {
	
	@IBOutlet var amount: UITextField!
	@IBOutlet var name: UILabel!
	
	@IBOutlet var back: UIView!
	override func awakeFromNib() {
		super.awakeFromNib()
		
		
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}
	
	func update() {
		if Currency.remove( amount.text!) != nil {
			if Float(Currency.remove( amount.text!)!) < Float(0) {
				amount.textColor = Color.nListText
				back.backgroundColor = Color.nListBack
			} else {
				amount.textColor = Color.pListText
				back.backgroundColor = Color.pListBack
			}
		}
		
		if User.usable < Double(0) {
			self.backgroundColor = Color.nBack
		} else {
			self.backgroundColor = Color.pBack
		}
		
		back.layer.shadowColor = UIColor.black.cgColor
		back.layer.shadowOpacity = 0.7
		back.layer.shadowOffset = CGSize(width: 1.0, height: 3.0)
		back.layer.shadowRadius = 3
		
		back.layer.shouldRasterize = false
		
		self.clipsToBounds = false
		self.layer.masksToBounds = false
	}
}

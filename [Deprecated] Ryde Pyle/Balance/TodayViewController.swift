//
//  TodayViewController.swift
//  Balance
//
//  Created by Ryan Dean on 10/26/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
	
	@IBOutlet var checkingBalance: UILabel!
	@IBOutlet var savingsBalance: UILabel!
	@IBOutlet var capitalGraph: LineGraphModule!
	@IBOutlet var usableGraph: LineGraphModule!
	@IBOutlet var budgetGraph: LineGraphModule!
	@IBOutlet var billsGraph: LineGraphModule!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
		
		self.extensionContext?.widgetLargestAvailableDisplayMode = NCWidgetDisplayMode.expanded
		
		Timer.scheduledTimer(timeInterval: 0.1,
		                                    target: self,
		                                    selector: #selector(self.animate),
		                                    userInfo: nil,
		                                    repeats: true)
		
		self.capitalGraph.setNeedsDisplay()
		self.usableGraph.setNeedsDisplay()
		self.budgetGraph.setNeedsDisplay()
		self.billsGraph.setNeedsDisplay()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: @escaping ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
		
		update()
		
        completionHandler(NCUpdateResult.newData)
    }
	
	func animate () {
		if self.capitalGraph.idle != true {
			self.capitalGraph.animate()
		}
		if self.usableGraph.idle != true {
			self.usableGraph.animate()
		}
		if self.budgetGraph.idle != true {
			self.budgetGraph.animate()
		}
		if self.billsGraph.idle != true {
			self.billsGraph.animate()
		}
	}
	
	func update () {
		User.rydeId = Database.verifySesssion()
		
		User.load()
		
		checkingBalance.text = Currency.format(User.usable.description)
		savingsBalance.text = Currency.format(User.capital.description)
		
		self.capitalGraph.updateData(User.banks, balance: User.capital)
		self.capitalGraph.setNeedsDisplay()
		
		let revBudg = User.budgets
		
		for r in revBudg {
			for t in r.transactions {
				t.value = -t.value
			}
		}
		
		self.usableGraph.updateData(User.banks + revBudg, balance: User.usable)
		self.usableGraph.setNeedsDisplay()
		
		self.budgetGraph.updateData(User.budgets + User.goals, balance: User.budget)
		self.budgetGraph.setNeedsDisplay()
		
		self.billsGraph.updateData(User.bills, balance: User.bill)
		self.billsGraph.setNeedsDisplay()
		
		
		if User.usable < Double(0) {
			savingsBalance.textColor = UIColor.red
		} else {
			savingsBalance.textColor = UIColor.green
		}
		
		if User.capital < Double(0) {
			checkingBalance.textColor = UIColor.red
		} else {
			checkingBalance.textColor = UIColor.green
		}
		
		if User.usable < Double(0) {
			self.view.backgroundColor = Color.nBack
		} else {
			self.view.backgroundColor = Color.pBack
		}
	}
	
	func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
		if (activeDisplayMode == NCWidgetDisplayMode.compact) {
			self.preferredContentSize = maxSize
			self.capitalGraph.isHidden = true
			self.usableGraph.isHidden = true
			self.budgetGraph.isHidden = true
			self.billsGraph.isHidden = true
		}
		else {
			self.preferredContentSize = CGSize(width: maxSize.width, height: 600)
			self.capitalGraph.isHidden = false
			self.usableGraph.isHidden = false
			self.budgetGraph.isHidden = false
			self.billsGraph.isHidden = false
		}
	}
	
	@IBAction func openBudget(_ sender: AnyObject) {
		GroupData.set("widgetButton", data: "budget")
		
		let myAppUrl = NSURL(string: "ryde-pyle://")!
		extensionContext?.open(myAppUrl as URL, completionHandler: { (success) in
			if (!success) {
				// let the user know it failed
			}
		})
	}
	
	@IBAction func openGoals(_ sender: AnyObject) {
		GroupData.set("widgetButton", data: "bills")
		
		let myAppUrl = NSURL(string: "ryde-pyle://")!
		extensionContext?.open(myAppUrl as URL, completionHandler: { (success) in
			if (!success) {
				// let the user know it failed
			}
		})
	}
	
	@IBAction func openReports(_ sender: AnyObject) {
		GroupData.set("widgetButton", data: "debt")
		
		let myAppUrl = NSURL(string: "ryde-pyle://")!
		extensionContext?.open(myAppUrl as URL, completionHandler: { (success) in
			if (!success) {
				// let the user know it failed
			}
		})
	}
	
	
	
	
}

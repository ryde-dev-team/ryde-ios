//
//  DashView.swift
//  Ryde
//
//  Created by Ryan Dean on 1/22/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class DashView: UIView {
	
	@IBOutlet var view:	UIView!
	
	@IBOutlet var usableLabel: UILabel!
	@IBOutlet var capitalGraph: LineGraphModule!
	@IBOutlet var debtGraph: LineGraphModule!
	@IBOutlet var assetGraph: LineGraphModule!
	@IBOutlet var billsGraph: LineGraphModule!
	@IBOutlet var budgetGraph: LineGraphModule!
	@IBOutlet var userNameLabel: UILabel!
	@IBOutlet var placeNameLabel: UILabel!
	@IBOutlet var netGraph: LineGraphModule!
	
	@IBOutlet var incomeButton: UIButton!
	@IBOutlet var spendButton: UIButton!
	@IBOutlet var transferButton: UIButton!
	
	
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		_ = Bundle.main.loadNibNamed("DashView", owner: self, options: nil)![0] as! UIView
		self.addSubview(view)
		view.frame = self.bounds
	}
	
}

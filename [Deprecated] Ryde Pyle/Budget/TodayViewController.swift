//
//  TodayViewController.swift
//  Budget
//
//  Created by Ryan Dean on 1/16/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding, UITableViewDataSource, UITableViewDelegate {
        
	@IBOutlet var budgetLeft: UILabel!
	@IBOutlet var budgetList: UITableView!
	
	var accounts: [Account] = []
	
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
		
		self.extensionContext?.widgetLargestAvailableDisplayMode = NCWidgetDisplayMode.expanded
		
		budgetList.delegate = self
		budgetList.dataSource = self
		
		update()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
		
		update()
		
        completionHandler(NCUpdateResult.newData)
    }
	
	func update () {
		User.rydeId = Database.verifySesssion()
		
		User.load()
		
		accounts = User.budgets + User.goals
		accounts.sort {$0.balance > $1.balance}
		
		
		budgetLeft.text = Currency.format( User.usable.description)
		
		if User.usable < 0 {
			budgetLeft.textColor = Color.nText
			self.view.backgroundColor = Color.nBack
			budgetList.backgroundColor = Color.nBack
		} else {
			budgetLeft.textColor = Color.pText
			self.view.backgroundColor = Color.pBack
			budgetList.backgroundColor = Color.pBack
		}
		
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return User.budgets.count + User.goals.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetingCell", for: indexPath as IndexPath) as! BudgetingCell
		
		accounts = User.budgets + User.goals
		
		accounts.sort {$0.balance > $1.balance}
		
		if indexPath.row < accounts.count {
			let bill = accounts[indexPath.row]
			cell.name.text = bill.name + ""
			cell.amount.text = Currency.format( bill.balance.description)
			cell.name.isUserInteractionEnabled = false
		}
		
		cell.update()
		
		
		
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let c = tableView.cellForRow(at: indexPath) as! BudgetingCell
		self.view.endEditing(true)
		
		if indexPath.row == User.budgets.count + User.goals.count {
			if c.name.text != "" {
				let name = c.name.text!
				let id = Database.getNextAccountId()
				let na = Account(id: id, type: 4, name: name, balance: 0.00, clear: 0, rec: 0)
				na.save()
				User.budgets.append(na)
				budgetList.reloadData()
				c.amount.isEnabled = true
				c.amount.isUserInteractionEnabled = true
				c.amount.becomeFirstResponder()
			}
		} else {
			//Go to account transaction view
		}
		
		print(indexPath.description)
	}
	
	func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
		if (activeDisplayMode == NCWidgetDisplayMode.compact) {
			self.preferredContentSize = maxSize
		}
		else {
			self.preferredContentSize = CGSize(width: maxSize.width, height: 300)
			
		}
	}
    
}

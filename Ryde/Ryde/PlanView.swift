//
//  PlanView.swift
//  Ryde
//
//  Created by Ryan Dean on 1/20/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class PlanView: UIView {
	
	@IBOutlet var view: UIView!
	@IBOutlet var subscriptionLabel: UILabel!
	@IBOutlet var nextPaymentProgress: UIProgressView!
	@IBOutlet var nextPaymentDescription: UILabel!
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		_ = Bundle.main.loadNibNamed("PlanView", owner: self, options: nil)![0] as! UIView
		self.addSubview(view)
		view.frame = self.bounds
	}
}

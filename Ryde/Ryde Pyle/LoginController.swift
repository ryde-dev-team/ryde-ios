//
//  LoginView.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 1/7/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class LoginController: UIViewController, UITextFieldDelegate {
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
		
		Timer.scheduledTimer(timeInterval: 0.5,
		                                   target: self,
		                                   selector: #selector(self.update),
		                                   userInfo: nil,
		                                   repeats: true)
	}
	
	
	var loginView: LoginView!
	
	//@IBOutlet var activityInd: UIActivityIndicatorView!
	
	var firstIn = true
	
	func update () {
		if self.loginView != nil {
			if self.loginView.nav != false {
				self.dismiss(animated: false, completion: nil)
			}
			if self.loginView.feedback != nil {
				self.present(self.loginView.feedback, animated: true, completion: nil)
				self.loginView.feedback = nil
			}
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.loginView.checkSystem()
		
		if self.loginView.skip {
			self.loginView.titleLabel.isHidden = true
			self.loginView.logoLabel.isHidden = true
			self.loginView.emailField.isHidden = true
			self.loginView.passwordField.isHidden = true
			self.loginView.loginButton.isHidden = true
			self.loginView.loginIssueButton.isHidden = true
			self.loginView.versionLabel.isHidden = true
			self.loginView.motdLabel.isHidden = true
			
			//activityInd.isHidden = false
			//activityInd.startAnimating()
		} else {
			self.loginView.titleLabel.isHidden = false
			self.loginView.logoLabel.isHidden = false
			self.loginView.emailField.isHidden = false
			self.loginView.passwordField.isHidden = false
			self.loginView.loginButton.isHidden = false
			self.loginView.loginIssueButton.isHidden = false
			self.loginView.versionLabel.isHidden = false
			self.loginView.motdLabel.isHidden = false
			
			//activityInd.isHidden = true
			//activityInd.stopAnimating()
			
			if firstIn {
				self.loginView.animIn()
				firstIn = false
			}
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if self.loginView.skip {
			User.load()
			self.dismiss(animated: false, completion: nil)
			//self.performSegue(withIdentifier: "login2root", sender: self)
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.loginView = self.view as! LoginView
		
		self.loginView.view.backgroundColor = Color.pBack
		self.loginView.emailField.backgroundColor = Color.pChartBack
		self.loginView.passwordField.backgroundColor = Color.pChartBack
		
		self.loginView.loginButton.backgroundColor = Color.pButton
		
		self.loginView.logoLabel.text = FileManager.default.displayName(atPath: Bundle.main.bundlePath)
		self.loginView.logoLabel.textColor = Color.pChartBack
		
		self.loginView.emailField.delegate = self
		self.loginView.passwordField.delegate = self
		
		self.loginView.versionLabel.text = System.version()
		self.loginView.motdLabel.text = ""
		
		self.loginView.loginButton.layer.cornerRadius = 5
		self.loginView.loginButton.layer.shadowOpacity = 0.7
		self.loginView.loginButton.layer.shadowRadius = 5
		self.loginView.loginButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		self.loginView.loginButton.layer.shadowColor = UIColor.black.cgColor
		
		self.loginView.emailField.layer.masksToBounds = false
		self.loginView.emailField.layer.shadowOpacity = 0.7
		self.loginView.emailField.layer.shadowRadius = 5
		self.loginView.emailField.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		self.loginView.emailField.layer.shadowColor = UIColor.black.cgColor
		
		self.loginView.passwordField.layer.masksToBounds = false
		self.loginView.passwordField.layer.shadowOpacity = 0.7
		self.loginView.passwordField.layer.shadowRadius = 5
		self.loginView.passwordField.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		self.loginView.passwordField.layer.shadowColor = UIColor.black.cgColor
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	
	
	
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.endEditing(true)
		self.view.endEditing(true)
		return false
	}
	
	func keyboardWillShow(notification: NSNotification) {
		
		
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y == 0{
				self.view.frame.origin.y -= keyboardSize.height / 2
				
			}
			else {
				
			}
			
			
		}
		
	}
	
	func keyboardWillHide(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y != 0 {
				self.view.frame.origin.y += keyboardSize.height / 2
				
			}
			else {
				
			}
		}
	}
	
	
	
}

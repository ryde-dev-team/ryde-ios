//
//  ViewController.swift
//  Ryde
//
//  Created by Ryan Dean on 1/17/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class LoadingController: UIViewController {
	
	var loadingView: LoadingView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		loadingView = self.view as! LoadingView
		
		self.loadingView.view.backgroundColor = Color.pBack
	}
	
	func setText(_ text: String) {
		self.loadingView.descriptionLabel.text = text
		self.loadingView.descriptionLabel.setNeedsDisplay()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		setText("Verifying Database Session")
		User.rydeid = Database.verifySesssion()
		
		if User.rydeid > -1 {
			if GroupData.get("pyleNav") == nil {
				setText("Key Verified - Loading User")
				User.load()
				setText("User Loaded")
				LoadingController.setNav(1)
			}
		} else {
			setText("No Key Cache Found - Login Required")
			LoadingController.setNav(0)
		}
		
		navigate(GroupData.get("pyleNav") as! Int?)
		GroupData.clear("pyleNav")
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func navigate(_ id: Int?) {
		var id = id
		if id == nil {
			id = 0
		}
		
		switch id! {
		case 1:
			goToDash()
		case 2:
			goToBilling()
		case 3:
			goToBudgeting()
		case 4:
			goToBanks()
		case 5:
			goToDebts()
		case 6:
			goToBills()
		case 7:
			goToBudgets()
		case 8:
			goToAssets()
		case 9:
			goToAccount()
		case 10:
			goToSheet()
		case 11:
			goToNewTran()
		case 12:
			goToNewBill()
		default:
			goToLogin()
		}
		
		
	}
	
	func goToLogin () {
		print("Go to login")
		self.performSegue(withIdentifier: "login", sender: self)
	}
	
	func goToDash () {
		print("Go to dash")
		self.performSegue(withIdentifier: "dash", sender: self)
	}
	
	func goToBilling () {
		print("Go to billing")
		self.performSegue(withIdentifier: "billing", sender: self)
	}
	
	func goToBudgeting () {
		print("Go to budgeting")
		self.performSegue(withIdentifier: "budgeting", sender: self)
	}
	
	func goToBanks () {
		print("Go to banks")
		self.performSegue(withIdentifier: "banks", sender: self)
	}
	
	func goToDebts () {
		print("Go to debts")
		self.performSegue(withIdentifier: "debts", sender: self)
	}
	
	func goToBills () {
		print("Go to bills")
		self.performSegue(withIdentifier: "bills", sender: self)
	}
	
	func goToBudgets () {
		print("Go to budgets")
		self.performSegue(withIdentifier: "budgets", sender: self)
	}
	
	func goToAssets () {
		print("Go to assets")
		self.performSegue(withIdentifier: "assets", sender: self)
	}
	
	func goToAccount () {
		print("Go to account")
		self.performSegue(withIdentifier: "account", sender: self)
	}
	
	func goToSheet () {
		print("Go to sheet")
		self.performSegue(withIdentifier: "sheet", sender: self)
	}
	
	func goToNewTran () {
		print("Go to new tran")
		self.performSegue(withIdentifier: "newTran", sender: self)
	}
	
	func goToNewBill () {
		print("Go to new bill")
		self.performSegue(withIdentifier: "newBill", sender: self)
	}
	
	static func setNav(_ nav: Int) {
		GroupData.set("pyleNav", data: nav)
	}
}


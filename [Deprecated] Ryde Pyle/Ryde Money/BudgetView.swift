//
//  DebtView.swift
//  Ryde Money
//
//  Created by ryan.dean on 11/1/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class BudgetView: UIViewController, UITableViewDataSource, UITableViewDelegate{
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		Timer.scheduledTimer(timeInterval: 1.0,
		                     target: self,
		                     selector: #selector(self.update),
		                     userInfo: nil,
		                     repeats: true)
		
		Timer.scheduledTimer(timeInterval: 0.1,
		                     target: self,
		                     selector: #selector(self.animate),
		                     userInfo: nil,
		                     repeats: true)
	}
	
	
    @IBOutlet var swipeRight: UISwipeGestureRecognizer!
    @IBOutlet weak var budgetGraph: LineGraphModule!
    @IBOutlet weak var accountTable: UITableView!
	@IBOutlet var leftToBudget: UILabel!
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var addBudgetButton: UIButton!
	@IBOutlet var budgetButton: UIButton!
	
	var viewing = 0
    
	@IBAction func budgetButton(_ sender: Any) {
		animBudget()
	}
	
	@IBAction func addAccount(_ sender: Any) {
		let alertController = UIAlertController(title: "New Budget Account", message: "", preferredStyle: .alert)
		
		let checkingAction = UIAlertAction(title: "Budget", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertController.textFields![0] as UITextField
			
			let acc = Account(id: Database.getNextAccountId(), type: 4, name: firstTextField.text!, balance: 0, clear: 0, rec: 0)
			User.budgets.append(acc)
			acc.save()
			self.accountTable.reloadData()
		})
		
		let savingsAction = UIAlertAction(title: "Goal", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertController.textFields![0] as UITextField
			
			let acc = Account(id: Database.getNextAccountId(), type: 6, name: firstTextField.text!, balance: 0, clear: 0, rec: 0)
			User.goals.append(acc)
			acc.save()
			self.accountTable.reloadData()
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		
		alertController.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = "Account Name"
			textField.textAlignment = .center
		}
		
		alertController.addAction(checkingAction)
		alertController.addAction(savingsAction)
		alertController.addAction(cancelAction)
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	
    override func viewDidLoad() {
        super.viewDidLoad()
        
        swipeRight.addTarget(self, action: #selector(backToRoot(_:)))
        self.view.addGestureRecognizer(swipeRight)
        self.view.isUserInteractionEnabled = true
		
		self.addBudgetButton.backgroundColor = UIColor.gray
		self.budgetButton.backgroundColor = Color.pButton
		
		budgetButton.layer.shadowColor = UIColor.black.cgColor
		budgetButton.layer.shadowOpacity = 0.5
		budgetButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		budgetButton.layer.shadowRadius = 4
		budgetButton.layer.cornerRadius = 5
		
		addBudgetButton.layer.shadowColor = UIColor.black.cgColor
		addBudgetButton.layer.shadowOpacity = 0.5
		addBudgetButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		addBudgetButton.layer.shadowRadius = 4
		addBudgetButton.layer.cornerRadius = 5
        
        accountTable.delegate = self
        accountTable.dataSource = self
        
        update()
    }
	
	func animate() {
		if !budgetGraph.idle {
			budgetGraph.animate()
			return
		}
		
		
		let accounts: [Account] = User.budgets + User.goals
		
		for i in 0..<accounts.count {
			let cell = accountTable.cellForRow(at: [0,i]) as! AccountGraphCell?
			if cell != nil {
				if !cell!.accountGraph.idle {
					cell!.accountGraph.animate()
					return
				}
				
				if (cell!.contentView.window != nil) {
					cell!.accountGraph.isHidden = false
				} else {
					cell!.accountGraph.isHidden = true
				}
				
			}
		}
		
		if (self.isViewLoaded && (self.view.window != nil)) {
			budgetGraph.isHidden = false
		} else {
			budgetGraph.isHidden = true
		}
		
	}
	
	var firstIn = true
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if firstIn {
			animIn()
			firstIn = false
		} else {
			animIn()
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		accountTable.reloadData()
		accountTable.setNeedsDisplay()
		//run code on enter forground in app delegate
	}
    
    @IBAction func budgetSuggested(_ sender: Any) {
        if (accountTable.indexPathForSelectedRow?.row)! < User.budgets.count {
            //User.budgets[(accountTable.indexPathForSelectedRow?.row)!].addTranscation(tran: Transcation(merc: Merchant(name: "Test"),time: Date().timeIntervalSince1970,value: Float(GroupData.randomBetweenNumbers(firstNum: -45.00, secondNum: 45.00))))
        } else {
            //User.goals[(accountTable.indexPathForSelectedRow?.row)! - User.budgets.count].addTranscation(tran: Transcation(merc: Merchant(name: "Test"),time: Date().timeIntervalSince1970,value: Float(GroupData.randomBetweenNumbers(firstNum: -45.00, secondNum: 45.00))))
        }
        //User.bills[(accountTable.indexPathForSelectedRow?.row)!].payInFull()
        accountTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return User.budgets.count + User.goals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountGraphCell", for: indexPath as IndexPath) as! AccountGraphCell
		
		let accounts: [Account] = User.budgets + User.goals
		
		let bill = accounts[indexPath.row]
		
		cell.accountName.text = bill.name + ""
		
		cell.accountBalance.text = Currency.format( bill.balance.description)
		
		cell.accountGraph.updateData([bill], balance: bill.balance)
		
		if User.usable < 0 {
			cell.backgroundColor = Color.nBack
		} else {
			cell.backgroundColor = Color.pBack
		}
		
		cell.layer.masksToBounds = false
		cell.clipsToBounds = false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		GroupData.set("accountType", data: "3")
		GroupData.set("accountNum", data: indexPath[1].description)
		self.animAccount()
		
    }
    
    func update() {
		User.update()
		
		leftToBudget.text = Currency.format(User.usable.description)
		
		if User.usable < Double(0) {
			leftToBudget.textColor = Color.nText
			self.view.backgroundColor = Color.nBack
			accountTable.backgroundColor = Color.nBack
		} else {
			leftToBudget.textColor = Color.pText
			self.view.backgroundColor = Color.pBack
			accountTable.backgroundColor = Color.pBack
		}
		
		
		budgetGraph.updateData(User.budgets+User.goals, balance: User.budget)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func backToRoot(_ sender: AnyObject) {
        animOut()
        
    }
	
	func inFirst () {
		self.budgetGraph.frame.origin.x -= self.view.frame.width
		self.accountTable.frame.origin.x -= self.view.frame.width
		
		
		self.addBudgetButton.frame.origin.x -= self.view.frame.width
	}
	
	func inLast () {
		self.leftToBudget.frame.origin.x -= self.view.frame.width
		self.titleLabel.frame.origin.x -= self.view.frame.width
		
		self.budgetButton.frame.origin.x -= self.view.frame.width
		//self.addTranscationButton.center.x -= 260
	}
	
	func outFirst () {
		self.leftToBudget.frame.origin.x += self.view.frame.width
		self.titleLabel.frame.origin.x += self.view.frame.width
		
		self.budgetButton.frame.origin.x += self.view.frame.width
		//self.addTranscationButton.center.x += 260
	}
	
	func outLast () {
		self.budgetGraph.frame.origin.x += self.view.frame.width
		self.accountTable.frame.origin.x += self.view.frame.width
		
		
		self.addBudgetButton.frame.origin.x += self.view.frame.width
	}
	
	func animIn () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			if self.viewing == 0 {
				self.inFirst()
			} else {
				self.outFirst()
			}
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			if self.viewing == 0 {
				self.inLast()
			} else {
				self.outLast()
			}
		}) { (Finished) -> Void in
			self.viewing = 0
			self.view.layoutSubviews()
		}
	}
	
	func animOut () {
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.outFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outLast()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		
	}
	
	func animAccount () {
		viewing = 1
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.inFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.inLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "budget2transaction", sender: self)
		}
	}
	
	func animBudget () {
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.outFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "budget2budgeting", sender: self)
		}
	}
}

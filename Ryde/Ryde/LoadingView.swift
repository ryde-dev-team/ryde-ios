//
//  LoadingView.swift
//  Ryde
//
//  Created by Ryan Dean on 1/18/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class LoadingView: UIView {
	
	@IBOutlet var view: UIView!
	@IBOutlet var descriptionLabel: UILabel!
	
	@IBOutlet var loadingSpinner: UIActivityIndicatorView!
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		_ = Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)![0] as! UIView
		self.addSubview(view)
		view.frame = self.bounds
	}
	
	
}

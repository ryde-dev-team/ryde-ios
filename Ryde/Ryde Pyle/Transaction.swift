//
//  Transaction.swift
//  Ryde Money
//
//  Created by Ryan Dean on 10/29/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import Foundation

class Transaction {
	var id: Int
	var account: Account?
	var timestamp: TimeInterval
	var value: Double
	var memo: String
	var location: [Float]?
	var hidden = false
	
	var cleared = 0
	var reconciled = 0
	var deleted = 0
	
	init (time: TimeInterval, value: Double, memo: String, cleared: Int, reconciled: Int) {
		self.timestamp = time
		self.value = value
		id = Database.getNextTransactionId()
		self.memo = memo
		self.cleared = cleared
		self.reconciled = reconciled
	}
	
	init (id: Int, time: TimeInterval, value: Double, memo: String, cleared: Int, reconciled: Int) {
		self.timestamp = time
		self.value = value
		self.id = id
		self.memo = memo
		self.cleared = cleared
		self.reconciled = reconciled
	}
	
	func setLocation(lat: Float, long: Float){
		location = [lat,long]
	}
	
	func save() {
		Ecosystem.logLocal("Saving transaction [" + id.description + "] " + memo)
		
		Database.saveTransaction(self)
	}
	
	func toggleClear() {
		if cleared == 0 {
			account!.clearedBalance += value
			cleared = 1
			Ecosystem.logLocal("Cleared transaction " + id.description)
		} else {
			account!.clearedBalance -= value
			cleared = 0
			Ecosystem.logLocal("Uncleared transaction " + id.description)
		}
		save()
		account!.saveSlim()
	}
	
	func reconcile() {
		account!.reconciledBalance += value
		reconciled = 1
		Ecosystem.logLocal("Reconciled transaction " + id.description)
		save()
		account!.saveSlim()
		
		let indexOfA = account!.transactions.index{$0.id == id}
		account!.transactions.remove(at: indexOfA!)
	}
	
	func delete () {
		Ecosystem.logLocal("Delete transaction " + id.description)
		account!.balance -= value
		Database.deleteTransaction(self)
		account!.saveSlim()
		deleted = 1
		
		for b in User.currentBills {
			if b.tranId == id {
				b.deleteSlim()
			}
		}
		
		let indexOfA = account!.transactions.index{$0.id == id}
		account!.transactions.remove(at: indexOfA!)
	}
}

//
//  BudgetingView.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/22/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

//TODO:
// * When editing a budget it you scroll the budget out of view before saving, a null error is thrown

import UIKit


class BudgetingView: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
	
	@IBOutlet var swipeRight: UISwipeGestureRecognizer!
	
	@IBOutlet var budgetList: UITableView!
	@IBOutlet var budgetLeft: UILabel!
	@IBOutlet var titleLabel: UILabel!
	
	@IBOutlet var circle: CircleModule!
	@IBOutlet var listTop: NSLayoutConstraint!
	
	var animCellList: [BudgetingCell] = []
	
	var editingAccount: Account!
	
	var accounts: [Account] = []
	
	static var dismissTo = 1
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		NotificationCenter.default.addObserver(self, selector: #selector(BudgetingView.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BudgetingView.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
		
		Timer.scheduledTimer(timeInterval: 1.0,
		                     target: self,
		                     selector: #selector(self.update),
		                     userInfo: nil,
		                     repeats: true)
	}
	
	var firstIn = true
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if firstIn {
			animIn()
			firstIn = false
		} else {
			animIn()
		}
	}
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		swipeRight.addTarget(self, action: #selector(backToBudget(_:)))
		self.view.addGestureRecognizer(swipeRight)
		self.view.isUserInteractionEnabled = true
		
		budgetList.delegate = self
		budgetList.dataSource = self
		
		
		update()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	func backToBudget(_ sender: AnyObject) {
		LoadingController.setNav(BudgetingView.dismissTo)
		animOut()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return User.budgets.count + User.goals.count + 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetingCell", for: indexPath as IndexPath) as! BudgetingCell
		
		accounts = User.budgets + User.goals
		
		accounts.sort {$0.balance > $1.balance}
		
		if indexPath.row < accounts.count {
			let bill = accounts[indexPath.row]
			cell.name.text = bill.name + ""
			cell.amount.text = Currency.format( bill.balance.description)
			cell.name.isUserInteractionEnabled = false
			cell.amount.isUserInteractionEnabled = true
		} else {
			cell.name.text = ""
			cell.amount.text = ""
			cell.name.isUserInteractionEnabled = true
			cell.amount.isUserInteractionEnabled = false
		}
		
		cell.update()
		
		cell.name.delegate = self
		cell.amount.delegate = self
		
		
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let c = tableView.cellForRow(at: indexPath) as! BudgetingCell
		self.view.endEditing(true)
		
		if indexPath.row == User.budgets.count + User.goals.count {
			if c.name.text != "" {
				let name = c.name.text!
				let id = Database.getNextAccountId()
				let na = Account(id: id, type: 4, name: name, balance: 0.00, clear: 0, rec: 0)
				na.save()
				User.budgets.append(na)
				budgetList.reloadData()
				c.name.isUserInteractionEnabled = false
				c.amount.isEnabled = true
				c.amount.isUserInteractionEnabled = true
				c.amount.becomeFirstResponder()
			}
		} else {
			//Go to account transaction view
		}
		
		print(indexPath.description)
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.endEditing(true)
		self.view.endEditing(true)
		return false
	}
	
	func keyboardWillShow(notification: NSNotification) {
		
		
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			self.circle.isHidden = true
			if view.frame.origin.y == 0{
				UIView.animate(withDuration: 0.5, animations: { () -> Void in
					self.view.frame.origin.y -= keyboardSize.height
					self.listTop.constant -= 80
				}) { (Finished) -> Void in
					
				}
			}
			else {
				
			}
			
			
		}
		
	}
	
	func keyboardWillHide(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y != 0 {
				UIView.animate(withDuration: 0.5, animations: { () -> Void in
					self.view.frame.origin.y += keyboardSize.height
					self.listTop.constant += 80
				}) { (Finished) -> Void in
					self.circle.isHidden = false
				}
			}
			else {
				
			}
		}
	}
	
	func update() {
		accounts = User.budgets + User.goals
		accounts.sort {$0.balance > $1.balance}
		
		
		budgetLeft.text = Currency.format( User.usable.description)
		
		if User.usable < 0 {
			budgetLeft.textColor = Color.nText
			self.view.backgroundColor = Color.nBack
			budgetList.backgroundColor = Color.nBack
			circle.backgroundColor = Color.nBack
		} else {
			budgetLeft.textColor = Color.pText
			self.view.backgroundColor = Color.pBack
			budgetList.backgroundColor = Color.pBack
			circle.backgroundColor = Color.pBack
		}
		
		var i = 0

		var acc: Account!
		var cell: BudgetingCell!
		
		animCellList = []
		var balances: [CGFloat] = []
		
		while i <= accounts.count {
			if i < accounts.count {
				balances.append(CGFloat(accounts[i].balance))
			}
			if budgetList.cellForRow(at: [0,i]) != nil {
			
				let c = budgetList.cellForRow(at: [0,i]) as! BudgetingCell
				
				if i == accounts.count {
					
					if c.name.text! == "" {
						c.amount.isEnabled = false
					} else {
						//c.amount.isEnabled = true
					}
					if c.name.isEditing {
						editingAccount = nil
						print("Editing New " + i.description)
					} else if !c.name.isEditing && c.name.text != "" {
						self.view.endEditing(true)
						let name = c.name.text!
						let id = Database.getNextAccountId()
						let na = Account(id: id, type: 4, name: name, balance: 0.00, clear: 0, rec: 0)
						na.save()
						User.budgets.append(na)
						budgetList.reloadData()
					}
				} else {
					if c.amount.isEditing {
						print("Editing " + i.description)
						
						acc = accounts[i]
					} else {
						if editingAccount != nil {
							if accounts[i].id == editingAccount.id {
								cell = c
							}
						}
					}
				}
				
				c.update()
			}
			i += 1
		}
		
		if editingAccount != nil && acc != nil {
			if acc.id != editingAccount.id {
				
				updateAccount(cell: cell)
				
				editingAccount = acc
			}
		} else if acc != nil {
			editingAccount = acc
		} else if editingAccount != nil && acc == nil {
			updateAccount(cell: cell)
			
			editingAccount = nil
		}

		circle.data = balances
		circle.setNeedsDisplay()
	}
	
	func updateAccount(cell: BudgetingCell) {
		cell.amount.text = Currency.validate(cell.amount.text)
		
		let newBalance = Double(Currency.remove( cell.amount.text!)!)
		
		let diff = newBalance - editingAccount.balance
		
		
		if diff != 0 {
			editingAccount.adjustBalance(tran: Transaction(id: Database.getNextTransactionId(), time: Date().timeIntervalSince1970, value: diff, memo: "Budget to " + Currency.format( newBalance.description)!, cleared: 0, reconciled: 0))
			editingAccount.save()
		}
		
		cell.amount.text = Currency.format( editingAccount.balance.description)
		
		budgetList.reloadData()
	}
	
	func animIn () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.inFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			self.inLast()
		}) { (Finished) -> Void in
			
		}
	}
	
	func animOut () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outLast()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			self.outFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		
	}
	
	func inFirst () {
		self.budgetList.frame.origin.x -= self.view.frame.width
	}
	
	func inLast () {
		self.budgetLeft.frame.origin.x -= self.view.frame.width
		self.titleLabel.frame.origin.x -= self.view.frame.width
		self.circle.frame.origin.x -= self.view.frame.width
	}
	
	func outFirst () {
		self.budgetLeft.frame.origin.x += self.view.frame.width
		self.titleLabel.frame.origin.x += self.view.frame.width
		self.circle.frame.origin.x += self.view.frame.width
	}
	
	func outLast () {
		self.budgetList.frame.origin.x += self.view.frame.width
	}
}

//
//  CircleModule.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 1/5/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit
import CoreGraphics

class CircleModule: UIView {
	
	var data: [CGFloat] = []
	var shapes: [CAShapeLayer] = []
	
	override func didMoveToSuperview() {
		//print("yes")
		self.translatesAutoresizingMaskIntoConstraints = false
		
	}
	
	
	override func draw(_ dirtyRect: CGRect) {
		
		data.sort {abs($0) < abs($1)}
		
		var total = CGFloat(0)
		
		var i = 0
		while i < data.count {
			var c = data[i]
			if c < CGFloat(0) {
				c = -c
			}
			total += c
			i += 1
		}
		
		let oldShapes = shapes
		shapes = []
		
		i = 0
		var done = CGFloat(0)
		while i < data.count {
			var current = data[i]
			var fill = Color.pBack.cgColor
			
			if current < CGFloat(0) {
				current = -current
				fill = Color.nBack.cgColor
			}
			
			let percent = Float(current)/Float(total)
			
			
			let slice = drawSlice(percent, start: Float(done), fill: fill, stroke: fill)
			
			if i < oldShapes.count {
				layer.replaceSublayer(oldShapes[i], with: slice)
			} else {
				layer.insertSublayer(slice, at: 0)
			}
			
			shapes.append(slice)
			
			done += CGFloat(percent)
			i += 1
		}
		
		//drawSlice(0.5, start: 0.0, fill: Color.nChartFill.cgColor, stroke: Color.nChartLine.cgColor)
		//drawSlice(0.4, start: 0.5, fill: Color.pChartFill.cgColor, stroke: Color.pChartLine.cgColor)
		
		
	}
	
	func drawSlice(_ percent: Float, start: Float, fill: CGColor, stroke: CGColor) -> CAShapeLayer {
		let halfsize = min( bounds.size.width/2, bounds.size.height/2)
		let lineWidth = CGFloat(1)
		let radius = CGFloat(halfsize - (lineWidth/2))
		let center = CGPoint(x:halfsize,y:halfsize)
		let startAngle = CGFloat((M_PI * 2) * Double(start))
		let endAngle = CGFloat((M_PI * 2) * Double(percent)) + startAngle
		
		let path = UIBezierPath(arcCenter: center,radius: radius, startAngle: startAngle, endAngle:endAngle, clockwise: true)
		//path.stroke()
		
		//UIColor(cgColor: fill).setFill()
		//UIColor(cgColor: fill).setStroke()
		
		
		path.addLine(to: center)
		//path.fill()
		
		
		
		let shapeLayer = CAShapeLayer()
		shapeLayer.path = path.cgPath
		shapeLayer.fillColor = fill
		shapeLayer.strokeColor = stroke
		shapeLayer.lineWidth = lineWidth
		
		shapeLayer.shadowOpacity = 0.7
		shapeLayer.shadowColor = UIColor.black.cgColor
		shapeLayer.shadowOffset = CGSize(width: -1.0, height: 3.0)
		shapeLayer.shadowRadius = 3
		
		
		return shapeLayer
		
	}
}

//
//  System.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 1/13/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import Foundation

class System {
	static func version() -> String {
		let dictionary = Bundle.main.infoDictionary!
		let version = dictionary["CFBundleShortVersionString"] as! String
		let build = dictionary["CFBundleVersion"] as! String
		
		let scope = build.substring(to: build.index(build.startIndex, offsetBy: 1))
		let release = build.substring(from: build.index(build.startIndex, offsetBy: 1))
		
		return "v\(version) (s\(scope)r\(release))"
	}
	
	static func build() -> String {
		return Bundle.main.infoDictionary!["CFBundleVersion"] as! String
	}
}

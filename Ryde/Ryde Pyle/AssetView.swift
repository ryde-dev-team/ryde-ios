//
//  AssetView.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/24/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class AssetView: UIViewController, UITableViewDataSource, UITableViewDelegate{
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		Timer.scheduledTimer(timeInterval: 1.0,
		                     target: self,
		                     selector: #selector(self.update),
		                     userInfo: nil,
		                     repeats: true)
		
		Timer.scheduledTimer(timeInterval: 0.1,
		                     target: self,
		                     selector: #selector(self.animate),
		                     userInfo: nil,
		                     repeats: true)
	}
	
	
	@IBOutlet var swipeRight: UISwipeGestureRecognizer!
	@IBOutlet weak var debtGraph: LineGraphModule!
	@IBOutlet weak var accountTable: UITableView!
	@IBOutlet var investLabel: UILabel!
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var addAssetButton: UIButton!
	@IBOutlet var lossButton: UIButton!
	@IBOutlet var gainButton: UIButton!
	
	var viewing = 0
	
	@IBAction func addAccount(_ sender: Any) {
		let alertController = UIAlertController(title: "New Asset Account", message: "", preferredStyle: .alert)
		
		let checkingAction = UIAlertAction(title: "Asset", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertController.textFields![0] as UITextField
			
			let acc = Account(id: Database.getNextAccountId(), type: 7, name: firstTextField.text!, balance: 0, clear: 0, rec: 0)
			User.assets.append(acc)
			acc.save()
			self.accountTable.reloadData()
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		
		alertController.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = "Account Name"
			textField.textAlignment = .center
		}
		
		alertController.addAction(checkingAction)
		alertController.addAction(cancelAction)
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		swipeRight.addTarget(self, action: #selector(backToRoot(_:)))
		self.view.addGestureRecognizer(swipeRight)
		self.view.isUserInteractionEnabled = true
		
		self.addAssetButton.backgroundColor = UIColor.gray
		self.gainButton.backgroundColor = Color.pButton
		self.lossButton.backgroundColor = Color.nButton
		
		addAssetButton.layer.shadowColor = UIColor.black.cgColor
		addAssetButton.layer.shadowOpacity = 0.5
		addAssetButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		addAssetButton.layer.shadowRadius = 4
		addAssetButton.layer.cornerRadius = 5
		
		gainButton.layer.shadowColor = UIColor.black.cgColor
		gainButton.layer.shadowOpacity = 0.5
		gainButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		gainButton.layer.shadowRadius = 4
		gainButton.layer.cornerRadius = 5
		
		lossButton.layer.shadowColor = UIColor.black.cgColor
		lossButton.layer.shadowOpacity = 0.5
		lossButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		lossButton.layer.shadowRadius = 4
		lossButton.layer.cornerRadius = 5
		
		accountTable.delegate = self
		accountTable.dataSource = self
		
		update()
	}
	
	func animate() {
		if !debtGraph.idle {
			debtGraph.animate()
			return
		}
		
		let accounts: [Account] = User.assets
		
		for i in 0..<accounts.count {
			if accountTable.cellForRow(at: [0,i]) != nil {
				if !(accountTable.cellForRow(at: [0,i]) as! AccountGraphCell).accountGraph.idle {
					(accountTable.cellForRow(at: [0,i]) as! AccountGraphCell).accountGraph.animate()
					return
				}
				
			}
		}
		
	}
	
	var firstIn = true
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if firstIn {
			animIn()
			firstIn = false
		} else {
			animIn()
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		accountTable.reloadData()
		accountTable.setNeedsDisplay()
		
		//run code on enter forground in app delegate
	}
	
	@IBAction func payDebt(_ sender: Any) {
		if (accountTable.indexPathForSelectedRow?.row)! < User.debts.count {
			//User.debts[(accountTable.indexPathForSelectedRow?.row)!].addTranscation(tran: Transcation(merc: Merchant(name: "Test"),time: Date().timeIntervalSince1970,value: Float(GroupData.randomBetweenNumbers(firstNum: -450.00, secondNum: 450.00))))
		} else {
			//User.cards[(accountTable.indexPathForSelectedRow?.row)! - User.budgets.count].addTranscation(tran: Transcation(merc: Merchant(name: "Test"),time: Date().timeIntervalSince1970,value: Float(GroupData.randomBetweenNumbers(firstNum: -450.00, secondNum: 450.00))))
		}
		accountTable.reloadData()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return User.assets.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "AccountGraphCell", for: indexPath as IndexPath) as! AccountGraphCell
		
		if indexPath.row < User.assets.count {
			
			let debt = User.assets[indexPath.row]
			
			//cell.accountName.text = debt.name + ""
			cell.accountGraph.nameLabel.text = debt.name + ""
			
			//cell.accountBalance.text = Currency.format( debt.balance.description)
			
			cell.accountGraph.updateData([debt], balance: debt.balance)
		}
		
		if User.usable < 0 {
			cell.backgroundColor = Color.nBack
		} else {
			cell.backgroundColor = Color.pBack
		}
		
		cell.layer.masksToBounds = false
		cell.clipsToBounds = false
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		GroupData.set("accountType", data: "4")
		GroupData.set("accountNum", data: indexPath[1].description)
		LoadingController.setNav(9)
		self.animAccount()
	}
	
	func update() {
		if debtGraph != nil {
			debtGraph.updateData(User.assets, balance: User.asset)
			debtGraph.nameLabel.text = "Assets"
		}
		
		investLabel.text = Currency.format( User.usable.description)
		
		if User.usable < 0 {
			investLabel.textColor = Color.nText
			self.view.backgroundColor = Color.nBack
			accountTable.backgroundColor = Color.nBack
		} else {
			investLabel.textColor = Color.pText
			self.view.backgroundColor = Color.pBack
			accountTable.backgroundColor = Color.pBack
		}
		
		accountTable.setNeedsDisplay()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	func backToRoot(_ sender: AnyObject) {
		LoadingController.setNav(1)
		animOut()
	}
	
	func inFirst () {
		self.debtGraph.frame.origin.x -= self.view.frame.width
		self.accountTable.frame.origin.x -= self.view.frame.width
		
		
		self.addAssetButton.frame.origin.x -= self.view.frame.width
	}
	
	func inLast () {
		self.investLabel.frame.origin.x -= self.view.frame.width
		self.titleLabel.frame.origin.x -= self.view.frame.width
		
		self.gainButton.frame.origin.x -= self.view.frame.width
		self.lossButton.frame.origin.x -= self.view.frame.width
	}
	
	func outFirst () {
		self.investLabel.frame.origin.x += self.view.frame.width
		self.titleLabel.frame.origin.x += self.view.frame.width
		
		self.gainButton.frame.origin.x += self.view.frame.width
		self.lossButton.frame.origin.x += self.view.frame.width
	}
	
	func outLast () {
		self.debtGraph.frame.origin.x += self.view.frame.width
		self.accountTable.frame.origin.x += self.view.frame.width
		
		
		self.addAssetButton.frame.origin.x += self.view.frame.width
	}
	
	func animIn () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			if self.viewing == 0 {
				self.inFirst()
			} else {
				self.outFirst()
			}
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			if self.viewing == 0 {
				self.inLast()
			} else {
				self.outLast()
			}
		}) { (Finished) -> Void in
			self.viewing = 0
			self.view.layoutSubviews()
		}
	}
	
	func animOut () {
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.outLast()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		
	}
	
	func animAccount () {
		viewing = 1
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.outLast()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		
	}
	
	func animTran () {
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.outFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outLast()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
	}
	
	@IBAction func gain(_ sender: Any) {
		GroupData.set("newTranConfig", data: "2")
		LoadingController.setNav(11)
		animTran()
	}
	
	@IBAction func loss(_ sender: Any) {
		GroupData.set("newTranConfig", data: "6")
		LoadingController.setNav(11)
		animTran()
	}
	
	
	
}

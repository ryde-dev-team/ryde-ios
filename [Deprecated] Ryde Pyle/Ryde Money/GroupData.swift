//
//  Data.swift
//  Ryde Money
//
//  Created by Ryan Dean on 10/29/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class GroupData {
	static func set(_ key: String, data: Any) {
		UserDefaults.init(suiteName: "group.ryde.group-data")!.set(data,forKey: key)
	}
	
	static func get(_ key: String) -> Any? {
		return UserDefaults.init(suiteName: "group.ryde.group-data")!.object(forKey: key)
	}
	
	static func clear(_ key: String) {
		UserDefaults.init(suiteName: "group.ryde.group-data")!.removeObject(forKey: key)
	}
	
}

//
//  Database.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 1/13/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import Foundation
import CryptoSwift

extension String {
	
	func customEncrypt (key: String) throws -> String {
		let data = self.data(using: String.Encoding.utf8)
		let enc = try data!.encrypt(cipher: ChaCha20(key: key, iv: Database.appKey))
		return enc.base64EncodedString()
	}
	
	func customDecrypt (key: String) throws -> String {
		let data = Data(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0))
		let dec = try data!.decrypt(cipher: ChaCha20(key: key, iv: Database.appKey))
		return String(data: dec, encoding: String.Encoding.utf8)!
	}
	
}

class Database {
	static var appKey: String = "bqif74#k"
	
	static var URL_sessionRequest = "http://mdm.tcschools.com/scripts/ryde-pyle/requestSession.php"
	static var URL_sessionVerify = 	"http://mdm.tcschools.com/scripts/ryde-pyle/verifySession.php"
	static var URL_sessionRemove = 	"http://mdm.tcschools.com/scripts/ryde-pyle/removeSession.php"
	static var URL_user = 			"http://mdm.tcschools.com/scripts/ryde-pyle/user.php"
	static var URL_account = "http://mdm.tcschools.com/scripts/ryde-pyle/account.php"
	static var URL_accountByType = 	"http://mdm.tcschools.com/scripts/ryde-pyle/accountByType.php"
	static var URL_transactions = "http://mdm.tcschools.com/scripts/ryde-pyle/transactions.php"
	static var URL_transactionsByAccount = "http://mdm.tcschools.com/scripts/ryde-pyle/transactionByAccount.php"
	static var URL_bills = "http://mdm.tcschools.com/scripts/ryde-pyle/bills.php"
	static var URL_systemVerify = "http://mdm.tcschools.com/scripts/ryde-pyle/verifySystem.php"
	static var URL_feedback = "http://mdm.tcschools.com/scripts/ryde-pyle/feedback.php"
	
	
	static func debug () {
		
	}
	
	static func encrypt(_ string: String, key: String) -> String?{
		var r: String!
		do {
			r = try string.customEncrypt(key: key)
		} catch {
			r = "[ENCRYPTION ERROR]"
		}
		return r
	}
	
	static func decrypt(_ string: String, key: String) -> String?{
		var r: String!
		do {
			r = try string.customDecrypt(key: key)
		} catch {
			r = "[ENCRYPTION ERROR]"
		}
		return r
	}
	
	static func generateKey() -> String {
		let length = 32
		let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		let len = UInt32(letters.length)
		
		var randomString = ""
		
		for _ in 0 ..< length {
			let rand = arc4random_uniform(len)
			var nextChar = letters.character(at: Int(rand))
			randomString += NSString(characters: &nextChar, length: 1) as String
		}
		
		return randomString
	}
	
	static func verifySesssion() -> Int{
		print("Checking for key cache")
		if GroupData.get("sessionKey") == nil {
			print("No key cache found")
			return -1
		}
		
		let unverifiedKey = GroupData.get("sessionKey") as! String
		
		let phpRequest = URL_sessionVerify + "?key=" + unverifiedKey
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if r["response"] == nil || r["rydeid"] == nil {
				return -1
			}
			
			let response = r["response"]!
			let rydeid = Int(r["rydeid"]!)!
			
			if response != "VERIFIED" {
				return -1
			}
			
			print(response + " => " + rydeid.description)
			return rydeid
		}
		return -1
	}
	
	static func userInfo(_ id: Int) -> [String]? {
		if id < 0 {
			return nil
		}
		
		print("Getting user info for id " + id.description)
		
		let phpRequest = URL_user + "?id=" + id.description
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if r["name"] == nil || r["authority"] == nil {
				return nil
			}
			
			let name = r["name"]!
			let authority = r["authority"]!
			
			print(name + " [" + authority + "]")
			return [name,authority]
		}
		
		return nil
	}
	
	static func login(_ user: String, pass: String) -> [String]? {
		let loginKey = generateKey()
		var passCrypt = encrypt(pass, key: loginKey)
		
		if passCrypt == nil {
			return nil
		}
		
		while passCrypt!.contains("+") {
			passCrypt = encrypt(pass, key: loginKey)
		}
		
		print(user + ":" + passCrypt!)
		
		let phpRequest = URL_sessionRequest + "?key=" + loginKey + "&hash=" + passCrypt! + "&email=" + user.lowercased()
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if r["response"] == nil || r["rydeid"] == nil {
				return nil
			}
			
			let response = r["response"]
			let id = r["rydeid"]
			
			if response == "BAD HASH" {
				return nil
			}
			
			print("SESSION KEY: " + response!)
			GroupData.set("sessionKey", data: response!)
			return [response!,id!]
		}
		
		return nil
	}
	
	static func logout(_ key: String) {
		print("Removing local key cache")
		GroupData.clear("sessionKey")
		
		print("Removing remote key")
		let phpRequest = URL_sessionRemove + "?key=" + key
		_ = Network.dataOfJson(url: phpRequest)
	}
	
	static func accounts(_ type: Int) -> [Account]{
		let phpRequest = URL_accountByType + "?type=" + type.description + "&rydeid=" + User.rydeId.description
		
		var accs: [Account] = []
		
		for r in Network.dataOfJson(url: phpRequest)! {
			let account = Account(id: Int(r["id"]!)!, type: Int(r["type"]!)!, name: String(r["name"]!)!, balance: Double(r["balance"]!)!, limit: Double(r["limit"]!)!, clear: Double(r["cleared"]!)!, rec: Double(r["reconciled"]!)!)
			
			accs.append(account)
		}
		
		return accs
	}
	
	static func transactions(_ acc: Account) -> [Transaction] {
		let phpRequest = URL_transactionsByAccount + "?account=" + acc.id.description + "&rydeid=" + User.rydeId.description
		
		var trans: [Transaction] = []
		
		for t in Network.dataOfJson(url: phpRequest)! {
			let transaction = Transaction(id: Int(t["id"]!)!, time: Double(t["epoch"]!)!, value: Double(t["value"]!)!, memo: String(t["memo"]!)!, cleared: Int(t["cleared"]!)!, reconciled: Int(t["reconciled"]!)!)
			
			if transaction.reconciled == 0 {
				transaction.account = acc
				trans.append(transaction)
			}
		}
		
		return trans
	}
	
	static func bills() -> [Bill] {
		let phpRequest = URL_bills + "?rydeid=" + User.rydeId.description
		
		var blls: [Bill] = []
		
		for b in Network.dataOfJson(url: phpRequest)! {
			if Int(b["paid"]!)! == 0 {
				let bill = Bill(id: Int(b["id"]!)!, tranId: Int(b["tranId"]!)!, paid: Int(b["paid"]!)!, due: Double(b["due_epoch"]!)!, late: Int(b["late"]!)!)
				
				blls.append(bill)
			}
		}
		
		return blls
	}
	
	static func getNextTransactionId() -> Int{
		var highest = 0
		
		for t in Network.dataOfJson(url: URL_transactions)! {
			if Int(t["id"]!)! >= highest {
				highest = Int(t["id"]!)! + 1
			}
		}
		
		return highest
	}
	
	static func getNextBillId() -> Int{
		var highest = 0
		
		for t in Network.dataOfJson(url: URL_bills)! {
			if Int(t["id"]!)! >= highest {
				highest = Int(t["id"]!)! + 1
			}
		}
		
		return highest
	}
	
	static func getNextAccountId() -> Int{
		var highest = 0
		
		for t in Network.dataOfJson(url: URL_account)! {
			if Int(t["id"]!)! >= highest {
				highest = Int(t["id"]!)! + 1
			}
		}
		
		return highest
	}
	
	static func getNextFeedbackId() -> Int{
		var highest = 0
		
		for t in Network.dataOfJson(url: URL_feedback)! {
			if Int(t["id"]!)! >= highest {
				highest = Int(t["id"]!)! + 1
			}
		}
		
		return highest
	}
	
	static func systemVerify() -> [String]? {
		var access: Int!
		var motd: String!
		
		if Network.dataOfJson(url: URL_systemVerify + "?build=default") != nil {
			for r in Network.dataOfJson(url: URL_systemVerify + "?build=default")! {
				if r["access"] != nil {
					access = Int(r["access"]!)!
				}
				if r["motd"] != nil {
					motd = r["motd"]!
				}
			}
		}
		
		if access == nil {
			if Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build()) != nil {
				for r in Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build())! {
					if r["access"] != nil {
						access = Int(r["access"]!)!
					}
					if r["motd"] != nil {
						motd = r["motd"]!
					}
				}
			}
		} else {
			if Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build()) != nil {
				for r in Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build())! {
					if r["access"] != nil {
						if access != nil {
							if access > Int(r["access"]!)! {
								access = Int(r["access"]!)!
							}
						}
					}
				}
			} else {
				motd = "This version is no longer valid, please update."
				access = nil
			}
		}
		
		
		if motd! == "" {
			if Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build()) != nil {
				for r in Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build())! {
					if r["motd"] != nil {
						motd = r["motd"]!
					}
				}
			} else {
				motd = ""
			}
		}
		
		return [access.description,motd]
	}
	
	static func saveAccount(_ acc: Account) {
		let phpRequest = URL_account + "?rydeid=" + User.rydeId.description
		let phpUpdate = URL_account + "?action=update"
		let phpInsert = URL_account + "?action=insert"
		
		let phpData = "id="+acc.id.description+"&type="+acc.type.description+"&name="+acc.name+"&balance="+acc.balance.description+"&limit="+acc.limit.description+"&rydeid="+User.rydeId.description+"&cleared="+acc.clearedBalance.description+"&reconciled="+acc.reconciledBalance.description
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if Int(r["id"]!)! == acc.id {
				Network.sendJson(url: phpUpdate, data: phpData)
				return
			}
		}
		
		Network.sendJson(url: phpInsert, data: phpData)
	}
	
	static func deleteAccount(_ acc: Account) {
		let phpRequest = URL_account + "?action=delete"
		Network.sendJson(url: phpRequest, data: "id="+acc.id.description)
	}
	
	static func saveTransaction(_ tran: Transaction) {
		let phpRequest = URL_transactions + "?rydeid=" + User.rydeId.description
		let phpUpdate = URL_transactions + "?action=update"
		let phpInsert = URL_transactions + "?action=insert"
		
		let phpData = "id="+tran.id.description+"&account_id="+tran.account!.id.description+"&epoch="+tran.timestamp.description+"&value="+tran.value.description+"&memo="+tran.memo+"&rydeid="+User.rydeId.description+"&cleared="+tran.cleared.description+"&reconciled="+tran.reconciled.description
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if Int(r["id"]!)! == tran.id {
				Network.sendJson(url: phpUpdate, data: phpData)
				return
			}
		}
		
		Network.sendJson(url: phpInsert, data: phpData)
	}
	
	static func deleteTransaction(_ tran: Transaction) {
		let phpRequest = URL_transactions + "?action=delete"
		Network.sendJson(url: phpRequest, data: "id="+tran.id.description)
	}
	
	static func saveBill(_ bill: Bill) {
		let phpRequest = URL_bills + "?rydeid=" + User.rydeId.description
		let phpUpdate = URL_bills + "?action=update"
		let phpInsert = URL_bills + "?action=insert"
		
		let phpData = "id="+bill.id.description+"&tranId="+bill.tranId.description+"&paid="+bill.paid.description+"&late="+bill.late.description+"&rydeid="+User.rydeId.description+"&due_epoch="+bill.due.description
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if Int(r["id"]!)! == bill.id {
				Network.sendJson(url: phpUpdate, data: phpData)
				return
			}
		}
		
		Network.sendJson(url: phpInsert, data: phpData)
	}
	
	static func deleteBill(_ bill: Bill) {
		let phpRequest = URL_bills + "?action=delete"
		Network.sendJson(url: phpRequest, data: "id="+bill.id.description)
		let indexOfA = User.currentBills.index{$0.id == bill.id}
		User.currentBills.remove(at: indexOfA!)
	}
	
	static func sendFeedback(_ text: String) {
		let id = getNextFeedbackId()
		let phpRequest = URL_feedback + "?action=insert"
		let phpData = "id="+id.description+"&rydeid="+User.rydeId.description+"&epoch="+Date().timeIntervalSince1970.description+"&message="+text
		Network.sendJson(url: phpRequest, data: phpData)
	}
	
}

//
//  PyleDatabase.swift
//  Ryde
//
//  Created by Ryan Dean on 1/18/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import Foundation

extension Database {
	static func accounts(_ type: Int) -> [Account]{
		let phpRequest = URL_accountByType + "?type=" + type.description + "&rydeid=" + User.rydeid.description
		
		var accs: [Account] = []
		
		for r in Network.dataOfJson(url: phpRequest)! {
			let account = Account(id: Int(r["id"]!)!, type: Int(r["type"]!)!, name: String(r["name"]!)!, balance: Double(r["balance"]!)!, limit: Double(r["limit"]!)!, clear: Double(r["cleared"]!)!, rec: Double(r["reconciled"]!)!)
			
			accs.append(account)
		}
		
		return accs
	}
	
	static func transactions(_ acc: Account) -> [Transaction] {
		let phpRequest = URL_transactionsByAccount + "?account=" + acc.id.description + "&rydeid=" + User.rydeid.description
		
		var trans: [Transaction] = []
		
		for t in Network.dataOfJson(url: phpRequest)! {
			let transaction = Transaction(id: Int(t["id"]!)!, time: Double(t["epoch"]!)!, value: Double(t["value"]!)!, memo: String(t["memo"]!)!, cleared: Int(t["cleared"]!)!, reconciled: Int(t["reconciled"]!)!)
			
			if transaction.reconciled == 0 {
				transaction.account = acc
				trans.append(transaction)
			}
		}
		
		return trans
	}
	
	static func bills() -> [Bill] {
		let phpRequest = URL_bills + "?rydeid=" + User.rydeid.description
		
		var blls: [Bill] = []
		
		for b in Network.dataOfJson(url: phpRequest)! {
			if Int(b["paid"]!)! == 0 {
				let bill = Bill(id: Int(b["id"]!)!, tranId: Int(b["tranId"]!)!, paid: Int(b["paid"]!)!, due: Double(b["due_epoch"]!)!, late: Int(b["late"]!)!)
				
				blls.append(bill)
			}
		}
		
		return blls
	}
	
	static func getNextTransactionId() -> Int{
		var highest = 0
		
		for t in Network.dataOfJson(url: URL_transactions)! {
			if Int(t["id"]!)! >= highest {
				highest = Int(t["id"]!)! + 1
			}
		}
		
		return highest
	}
	
	static func getNextBillId() -> Int{
		var highest = 0
		
		for t in Network.dataOfJson(url: URL_bills)! {
			if Int(t["id"]!)! >= highest {
				highest = Int(t["id"]!)! + 1
			}
		}
		
		return highest
	}
	
	static func getNextAccountId() -> Int{
		var highest = 0
		
		for t in Network.dataOfJson(url: URL_account)! {
			if Int(t["id"]!)! >= highest {
				highest = Int(t["id"]!)! + 1
			}
		}
		
		return highest
	}
	
	
	
	
	static func saveAccount(_ acc: Account) {
		let phpRequest = URL_account + "?rydeid=" + User.rydeid.description
		let phpUpdate = URL_account + "?action=update"
		let phpInsert = URL_account + "?action=insert"
		
		let phpData = "id="+acc.id.description+"&type="+acc.type.description+"&name="+acc.name+"&balance="+acc.balance.description+"&limit="+acc.limit.description+"&rydeid="+User.rydeid.description+"&cleared="+acc.clearedBalance.description+"&reconciled="+acc.reconciledBalance.description
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if Int(r["id"]!)! == acc.id {
				Network.sendJson(url: phpUpdate, data: phpData)
				return
			}
		}
		
		Network.sendJson(url: phpInsert, data: phpData)
	}
	
	static func deleteAccount(_ acc: Account) {
		let phpRequest = URL_account + "?action=delete"
		Network.sendJson(url: phpRequest, data: "id="+acc.id.description)
	}
	
	static func saveTransaction(_ tran: Transaction) {
		let phpRequest = URL_transactions + "?rydeid=" + User.rydeid.description
		let phpUpdate = URL_transactions + "?action=update"
		let phpInsert = URL_transactions + "?action=insert"
		
		let phpData = "id="+tran.id.description+"&account_id="+tran.account!.id.description+"&epoch="+tran.timestamp.description+"&value="+tran.value.description+"&memo="+tran.memo+"&rydeid="+User.rydeid.description+"&cleared="+tran.cleared.description+"&reconciled="+tran.reconciled.description
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if Int(r["id"]!)! == tran.id {
				Network.sendJson(url: phpUpdate, data: phpData)
				return
			}
		}
		
		Network.sendJson(url: phpInsert, data: phpData)
	}
	
	static func deleteTransaction(_ tran: Transaction) {
		let phpRequest = URL_transactions + "?action=delete"
		Network.sendJson(url: phpRequest, data: "id="+tran.id.description)
	}
	
	static func saveBill(_ bill: Bill) {
		let phpRequest = URL_bills + "?rydeid=" + User.rydeid.description
		let phpUpdate = URL_bills + "?action=update"
		let phpInsert = URL_bills + "?action=insert"
		
		let phpData = "id="+bill.id.description+"&tranId="+bill.tranId.description+"&paid="+bill.paid.description+"&late="+bill.late.description+"&rydeid="+User.rydeid.description+"&due_epoch="+bill.due.description
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if Int(r["id"]!)! == bill.id {
				Network.sendJson(url: phpUpdate, data: phpData)
				return
			}
		}
		
		Network.sendJson(url: phpInsert, data: phpData)
	}
	
	static func deleteBill(_ bill: Bill) {
		let phpRequest = URL_bills + "?action=delete"
		Network.sendJson(url: phpRequest, data: "id="+bill.id.description)
		let indexOfA = User.currentBills.index{$0.id == bill.id}
		User.currentBills.remove(at: indexOfA!)
	}
	
	
}

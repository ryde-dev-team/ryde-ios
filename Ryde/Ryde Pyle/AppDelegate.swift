//
//  AppDelegate.swift
//  Ryde Money
//
//  Created by Ryan Dean on 10/26/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit
import CoreData
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	
	

	func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
		
		switch shortcutItem.type {
			case "com.rydesoft.ryde-pyle.money_out":
				print("Money Out")
				GroupData.set("newTranConfig", data: "0")
				LoadingController.setNav(11)
			case "com.rydesoft.ryde-pyle.money_in":
				print("Money In")
				GroupData.set("newTranConfig", data: "2")
				LoadingController.setNav(11)
			case "com.rydesoft.ryde-pyle.money_transfer":
				print("Money Transfer")
				GroupData.set("newTranConfig", data: "1")
				LoadingController.setNav(11)
		default:
			print("Shortut Error")
		}
		
		GroupData.set("shortcut", data: 1)
		
	}

	
	
	

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		
		Ecosystem.logLocal("Launched Pyle")
		
		OneSignal.initWithLaunchOptions(launchOptions, appId: "1e779d14-d225-4f26-ad54-e38de85e0214")
		
		OneSignal.idsAvailable({ (userId, pushToken) in
			User.playerId = userId!
		})
		
  		return true
	}
	
	// Handle remote notification registration.
	func application(_ application: UIApplication,
	                 didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
		// Forward the token to your server.
		//self.enableRemoteNotificationFeatures()
		//self.forwardTokenToServer(token: deviceToken)
	}
 
	func application(_ application: UIApplication,
	                 didFailToRegisterForRemoteNotificationsWithError error: Error) {
		// The token is not currently available.
		print("Remote notification support is unavailable due to error: \(error.localizedDescription)")
		//self.disableRemoteNotificationFeatures()
	}

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
		Ecosystem.logLocal("Exited Pyle")
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
		Ecosystem.logLocal("Opened Pyle")
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
		// Saves changes in the application's managed object context before the application terminates.
		self.saveContext()
		Ecosystem.logLocal("Ended Pyle")
	}

	// MARK: - Core Data stack

	lazy var persistentContainer: NSPersistentContainer = {
	    /*
	     The persistent container for the application. This implementation
	     creates and returns a container, having loaded the store for the
	     application to it. This property is optional since there are legitimate
	     error conditions that could cause the creation of the store to fail.
	    */
	    let container = NSPersistentContainer(name: "Ryde_Pyle")
	    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
	        if let error = error as NSError? {
	            // Replace this implementation with code to handle the error appropriately.
	            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	             
	            /*
	             Typical reasons for an error here include:
	             * The parent directory does not exist, cannot be created, or disallows writing.
	             * The persistent store is not accessible, due to permissions or data protection when the device is locked.
	             * The device is out of space.
	             * The store could not be migrated to the current model version.
	             Check the error message to determine what the actual problem was.
	             */
	            fatalError("Unresolved error \(error), \(error.userInfo)")
	        }
	    })
	    return container
	}()

	// MARK: - Core Data Saving support

	func saveContext () {
	    let context = persistentContainer.viewContext
	    if context.hasChanges {
	        do {
	            try context.save()
	        } catch {
	            // Replace this implementation with code to handle the error appropriately.
	            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	            let nserror = error as NSError
	            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
	        }
	    }
	}

}


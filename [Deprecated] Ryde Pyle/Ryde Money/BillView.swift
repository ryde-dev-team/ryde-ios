//
//  DebtView.swift
//  Ryde Money
//
//  Created by ryan.dean on 11/1/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class BillView: UIViewController, UITableViewDataSource, UITableViewDelegate{
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		Timer.scheduledTimer(timeInterval: 1.0,
		                     target: self,
		                     selector: #selector(self.update),
		                     userInfo: nil,
		                     repeats: true)
		
		Timer.scheduledTimer(timeInterval: 0.1,
		                     target: self,
		                     selector: #selector(self.animate),
		                     userInfo: nil,
		                     repeats: true)
	}
	
	
    @IBOutlet var swipeRight: UISwipeGestureRecognizer!
    @IBOutlet weak var billGraph: LineGraphModule!
    @IBOutlet weak var accountTable: UITableView!
	@IBOutlet var paymentLabel: UILabel!
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var addBillButton: UIButton!
	@IBOutlet var billButton: UIButton!
	@IBOutlet var payButton: UIButton!
	
	var viewing = 0
	
	@IBAction func addAccount(_ sender: Any) {
		let alertController = UIAlertController(title: "New Bill Account", message: "", preferredStyle: .alert)
		
		let checkingAction = UIAlertAction(title: "Bill", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertController.textFields![0] as UITextField
			
			let acc = Account(id: Database.getNextAccountId(), type: 5, name: firstTextField.text!, balance: 0, clear: 0, rec: 0)
			User.bills.append(acc)
			acc.save()
			self.accountTable.reloadData()
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		
		alertController.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = "Account Name"
			textField.textAlignment = .center
		}
		
		alertController.addAction(checkingAction)
		alertController.addAction(cancelAction)
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	
	
    override func viewDidLoad() {
        super.viewDidLoad()
        
        swipeRight.addTarget(self, action: #selector(backToRoot(_:)))
        self.view.addGestureRecognizer(swipeRight)
        self.view.isUserInteractionEnabled = true
        
		self.addBillButton.backgroundColor = UIColor.gray
		self.payButton.backgroundColor = Color.pButton
		self.billButton.backgroundColor = Color.nButton
		
		payButton.layer.shadowColor = UIColor.black.cgColor
		payButton.layer.shadowOpacity = 0.5
		payButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		payButton.layer.shadowRadius = 4
		payButton.layer.cornerRadius = 5
		
		billButton.layer.shadowColor = UIColor.black.cgColor
		billButton.layer.shadowOpacity = 0.5
		billButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		billButton.layer.shadowRadius = 4
		billButton.layer.cornerRadius = 5
		
		addBillButton.layer.shadowColor = UIColor.black.cgColor
		addBillButton.layer.shadowOpacity = 0.5
		addBillButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		addBillButton.layer.shadowRadius = 4
		addBillButton.layer.cornerRadius = 5
        
        accountTable.delegate = self
        accountTable.dataSource = self
        
        update()
    }
	
	func animate() {
		if !billGraph.idle {
			billGraph.animate()
			return
		}
		
		
		let accounts: [Account] = User.bills
		
		for i in 0..<accounts.count {
			if accountTable.cellForRow(at: [0,i]) != nil {
				if !(accountTable.cellForRow(at: [0,i]) as! AccountGraphCell).accountGraph.idle {
					(accountTable.cellForRow(at: [0,i]) as! AccountGraphCell).accountGraph.animate()
					return
				}
				
			}
		}
		
	}
	
	var firstIn = true
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if firstIn {
			animIn()
			firstIn = false
		} else {
			animIn()
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		accountTable.reloadData()
		accountTable.setNeedsDisplay()
		
		//run code on enter forground in app delegate
	}
    
    @IBAction func paySelectedBill(_ sender: Any) {
        //User.bills[(accountTable.indexPathForSelectedRow?.row)!].payInFull()
        accountTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return User.bills.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountGraphCell", for: indexPath as IndexPath) as! AccountGraphCell
        
            
        let bill = User.bills[indexPath.row]
        
        cell.accountName.text = bill.name + ""
		
		cell.accountBalance.text = Currency.format(bill.balance.description)
		
		cell.accountGraph.updateData([bill], balance: bill.balance)
		
		if User.usable < 0 {
			cell.backgroundColor = Color.nBack
		} else {
			cell.backgroundColor = Color.pBack
		}
		
		cell.layer.masksToBounds = false
		cell.clipsToBounds = false
		
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		GroupData.set("accountType", data: "2")
		GroupData.set("accountNum", data: indexPath[1].description)
		animAccount()
    }
    
    func update() {
        
        if billGraph != nil {
            billGraph.updateData(User.bills, balance: User.bill)
        }
		
		paymentLabel.text = Currency.format(User.usable.description)
		
		if User.usable < Double(0) {
			paymentLabel.textColor = Color.nText
			self.view.backgroundColor = Color.nBack
			accountTable.backgroundColor = Color.nBack
		} else {
			paymentLabel.textColor = Color.pText
			self.view.backgroundColor = Color.pBack
			accountTable.backgroundColor = Color.pBack
		}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
	@IBAction func recordBill(_ sender: Any) {
		animRecord()
	}
	
	@IBAction func payBill(_ sender: Any) {
		animPay()
	}
	
    func backToRoot(_ sender: AnyObject) {
        animOut()
        
    }
	
	func inFirst () {
		self.billGraph.center.x -= self.view.frame.width
		self.accountTable.center.x -= self.view.frame.width
		
		
		self.addBillButton.center.x -= self.view.frame.width
	}
	
	func inLast () {
		self.paymentLabel.center.x -= self.view.frame.width
		self.titleLabel.center.x -= self.view.frame.width
		
		self.payButton.center.x -= self.view.frame.width
		self.billButton.center.x -= self.view.frame.width
	}
	
	func outFirst () {
		self.paymentLabel.center.x += self.view.frame.width
		self.titleLabel.center.x += self.view.frame.width
		
		self.payButton.center.x += self.view.frame.width
		self.billButton.center.x += self.view.frame.width
	}
	
	func outLast () {
		self.billGraph.center.x += self.view.frame.width
		self.accountTable.center.x += self.view.frame.width
		
		
		self.addBillButton.center.x += self.view.frame.width
	}
	
	func animIn () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			if self.viewing == 0 {
				self.inFirst()
			} else {
				self.outFirst()
			}
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			if self.viewing == 0 {
				self.inLast()
			} else {
				self.outLast()
			}
		}) { (Finished) -> Void in
			self.viewing = 0
			self.view.layoutSubviews()
		}
	}
	
	func animOut () {
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.outFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outLast()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		
	}
	
	func animAccount () {
		viewing = 1
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.inFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.inLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "bill2transaction", sender: self)
		}
		
		
	}
	
	func animPay () {
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.outFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "bill2pay", sender: self)
		}
		
		
		
	}
	
	
	
	func animRecord () {
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.outFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "bill2record", sender: self)
		}
		
		
		
	}
}

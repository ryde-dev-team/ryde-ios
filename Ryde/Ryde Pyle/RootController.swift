//
//  ViewController.swift
//  Ryde Money
//
//  Created by Ryan Dean on 10/26/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit
import CoreLocation


class RootController: UIViewController, CLLocationManagerDelegate {
	
	deinit {
		NotificationCenter.default.removeObserver(self)
		updateTimer.invalidate()
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		
		updateTimer = Timer.scheduledTimer(timeInterval: 0.5,
		                                   target: self,
		                                   selector: #selector(self.update),
		                                   userInfo: nil,
		                                   repeats: true)
		
		animateTimer = Timer.scheduledTimer(timeInterval: 0.1,
		                                   target: self,
		                                   selector: #selector(self.animate),
		                                   userInfo: nil,
		                                   repeats: true)
		
		NotificationCenter.default.addObserver(self, selector: #selector(RootController.appReopened), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
		
		
		
		NotificationCenter.default.addObserver(self, selector: #selector(RootController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
		
		locationManager.requestAlwaysAuthorization()
		
		if CLLocationManager.locationServicesEnabled() {
			locationManager.delegate = self
			locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
			locationManager.startUpdatingLocation()
		}
	}
	
	var updateTimer: Timer!
	var animateTimer: Timer!
	
    let locationManager = CLLocationManager()

	
	@IBOutlet var dashView: DashView!
	
    //@IBOutlet weak var locationLabel: UILabel!
    //@IBOutlet weak var nameLabel: UILabel!
	
	//@IBOutlet var capitalGraph: LineGraphModule!
	//@IBOutlet var currentCapitalLabel: UILabel!
	
	
	//@IBOutlet var debtGraph: LineGraphModule!
	//@IBOutlet var billsGraph: LineGraphModule!
	//@IBOutlet var budgetGraph: LineGraphModule!
	//@IBOutlet var assetGraph: LineGraphModule!
	//@IBOutlet var netGraph: LineGraphModule!
	
	@IBOutlet var swipeUp: UISwipeGestureRecognizer!
	
	@IBOutlet var swipeDown: UISwipeGestureRecognizer!
	
	@IBOutlet var swipeLeft: UISwipeGestureRecognizer!
	
	@IBOutlet var swipeRight: UISwipeGestureRecognizer!
	
	@IBOutlet var graphTap: UITapGestureRecognizer!
	
	@IBOutlet var incomeButton: UIButton!
	@IBOutlet var spendButton: UIButton!
	@IBOutlet var transferButton: UIButton!
	
	
	
	var firstIn = true
	
    @IBAction func addTransaction(_ sender: Any) {
		LoadingController.setNav(11)
		animUp()
    }
	
	@IBAction func addIncome(_ sender: Any) {
		LoadingController.setNav(11)
		animDown()
	}
	
	@IBAction func addTransfer(_ sender: Any) {
		GroupData.set("newTranConfig", data: "1")
		LoadingController.setNav(11)
		self.dismiss(animated: false, completion: nil)
	}
	
	
	func animate() {
		if !dashView.capitalGraph!.idle {
			dashView.capitalGraph!.animate()
			return
		}
		if !dashView.debtGraph!.idle {
			dashView.debtGraph!.animate()
			return
		}
		if !dashView.billsGraph!.idle {
			dashView.billsGraph!.animate()
			return
		}
		if !dashView.budgetGraph!.idle {
			dashView.budgetGraph!.animate()
			return
		}
		if !dashView.assetGraph!.idle {
			dashView.assetGraph!.animate()
			return
		}
		if !dashView.netGraph!.idle {
			dashView.netGraph!.animate()
			return
		}
		return
	}
    
    func update()
    {
		
		User.update()
		
		if User.sessionKey == "" {
			self.dismiss(animated: false, completion: nil)
		}
		
		if GroupData.get("shortcut") != nil {
			self.dismiss(animated: false, completion: nil)
			GroupData.clear("shortcut")
		}
		
		let dateFormatter = DateFormatter()
		
		dateFormatter.dateFormat = "MM/dd/yy"
		
		
		
		if dashView.capitalGraph != nil {
			dashView.capitalGraph.updateData(User.banks, balance: User.capital)
			dashView.capitalGraph.nameLabel.text = "Capital"
			
			dashView.capitalGraph.nameLabel.font = dashView.capitalGraph.nameLabel.font.withSize(CGFloat(25))
			dashView.capitalGraph.balanceLabel.font = dashView.capitalGraph.balanceLabel.font.withSize(CGFloat(35))
		}
		
		if dashView.debtGraph != nil {
			dashView.debtGraph.updateData(User.debts + User.cards, balance: User.debt)
			dashView.debtGraph.nameLabel.text = "Debt"
		}
		
		if dashView.billsGraph != nil {
			dashView.billsGraph.updateData(User.bills, balance: User.bill)
			dashView.billsGraph.nameLabel.text = "Bills"
		}
		
		if dashView.budgetGraph != nil {
			dashView.budgetGraph.updateData(User.budgets + User.goals, balance: User.budget)
			dashView.budgetGraph.nameLabel.text = "Budget"
		}
		
		if dashView.assetGraph != nil {
			dashView.assetGraph.updateData(User.assets, balance: User.asset)
			dashView.assetGraph.nameLabel.text = "Assets"
		}
		
		if dashView.netGraph != nil {
			let accounts: [Account] = User.banks + User.debts + User.cards + User.assets + User.bills
			
			dashView.netGraph.updateData(accounts, balance: User.net)
			dashView.netGraph.nameLabel.text = "Net"
			
			dashView.netGraph.nameLabel.font = dashView.netGraph.nameLabel.font.withSize(CGFloat(25))
			dashView.netGraph.balanceLabel.font = dashView.netGraph.balanceLabel.font.withSize(CGFloat(45))
		}
		
		dashView.userNameLabel.text = User.name
		
		dashView.usableLabel.text = Currency.format( User.usable.description)
		
		if User.usable < Double(0) {
			dashView.usableLabel.textColor = Color.nText
			self.dashView.view.backgroundColor = Color.nBack
		} else {
			dashView.usableLabel.textColor = Color.pText
			self.dashView.view.backgroundColor = Color.pBack
		}
		
		self.transferButton.backgroundColor = UIColor.gray
		self.incomeButton.backgroundColor = Color.pButton
		self.spendButton.backgroundColor = Color.nButton
		
        
        
//        for m in User.merchants {
//            let cx = User.currentLocation[0]
//            let cy = User.currentLocation[1]
//            let x = Float(0.0)//m.location[0]
//            let y = Float(0.0)//m.location[1]
//                
//				
//            if x + 0.0004 >= cx && x - 0.0004 <= cx {
//                if y + 0.0004 >= cy && y - 0.0004 <= cy {
//                    locationLabel.text = "Location = " + m.name
//                    return
//                }
//            }
//        }
//        
//        locationLabel.text = "Location = \(User.currentLocation[0]) \(User.currentLocation[1])"
		
    }
	
    
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		if GroupData.get("launchedBefore") == nil {
			firstLaunch()
			GroupData.set("launchedBefore", data: "1" as AnyObject)
		}
		
		
		
		
		graphTap.addTarget(self, action: #selector(graphTapped(_:)))
		self.view.addGestureRecognizer(graphTap)
		self.view.isUserInteractionEnabled = true
		
		swipeUp.addTarget(self, action: #selector(addTransaction(_:)))
		self.view.addGestureRecognizer(swipeUp)
		self.view.isUserInteractionEnabled = true
		
		swipeDown.addTarget(self, action: #selector(addIncome(_:)))
		self.view.addGestureRecognizer(swipeDown)
		self.view.isUserInteractionEnabled = true
		
		swipeLeft.addTarget(self, action: #selector(goToBudgeting(_:)))
		self.view.addGestureRecognizer(swipeLeft)
		self.view.isUserInteractionEnabled = true
		
		swipeRight.addTarget(self, action: #selector(goToBilling(_:)))
		self.view.addGestureRecognizer(swipeRight)
		self.view.isUserInteractionEnabled = true
		
		spendButton.layer.shadowColor = UIColor.black.cgColor
		spendButton.layer.shadowOpacity = 0.7
		spendButton.layer.shadowOffset = CGSize(width: 0.0, height: -4.0)
		spendButton.layer.shadowRadius = 5
		spendButton.layer.cornerRadius = 5
		
		incomeButton.layer.shadowColor = UIColor.black.cgColor
		incomeButton.layer.shadowOpacity = 0.7
		incomeButton.layer.shadowOffset = CGSize(width: -1.0, height: -3.0)
		incomeButton.layer.shadowRadius = 5
		incomeButton.layer.cornerRadius = 5
		
		transferButton.layer.shadowColor = UIColor.black.cgColor
		transferButton.layer.shadowOpacity = 0.7
		transferButton.layer.shadowOffset = CGSize(width: 1.0, height: -3.0)
		transferButton.layer.shadowRadius = 5
		transferButton.layer.cornerRadius = 5
		
		update()
	}
	
	func graphTapped(_ sender: AnyObject){
		let point = graphTap.location(in: self.dashView.view)
		let view = self.dashView.hitTest(point, with: nil)
		
		print((view?.center.x.description)! + "," + (view?.center.y.description)!)
		
		if (view == dashView.debtGraph.view) {
			Ecosystem.logLocal("Debt")
            goToDebt()
		} else if (view == dashView.billsGraph.view) {
			Ecosystem.logLocal("Bills")
			goToBill()
		} else if (view == dashView.budgetGraph.view) {
			Ecosystem.logLocal("Budget")
			goToBudget()
		} else if (view == dashView.capitalGraph.view) {
			Ecosystem.logLocal("Banks")
			goToBank()
		} else if (view == dashView.assetGraph.view) {
			Ecosystem.logLocal("Asset")
			goToAsset()
		} else if (view == dashView.netGraph.view) {
			Ecosystem.logLocal("Net")
        } else if (view?.center.equalTo(dashView.placeNameLabel.center))! {
            print("Location")
            let alert = UIAlertController(title: "New Place", message: "What would you like to call this place?", preferredStyle: .alert)
            
            alert.addTextField { (textField) in
                textField.placeholder = "Place Name"
				textField.textAlignment = .center
            }
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                let textField = alert.textFields![0]
				
				let plc = Place(Database.getNextPlaceId(), textField.text!, lat: Double(User.currentLocation[0]), long: Double(User.currentLocation[1]), radius: 20)
				
				User.places.append(plc)
				
				plc.save()
            }))
			
			alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
			
			if dashView.placeNameLabel.text == "Tap to Name Place" {
				self.present(alert, animated: true, completion: nil)
			}
		} else if (view?.center.equalTo(dashView.userNameLabel.center))! {
			let alertController = UIAlertController(title: "Options", message: "", preferredStyle: .alert)
			
			let checkingAction = UIAlertAction(title: "Logout", style: .destructive, handler: {
				alert -> Void in
				
				UIView.animate(withDuration: 0.3, animations: { () -> Void in
					self.dropFirst()
					
				}) { (Finished) -> Void in
					
				}
				
				UIView.animate(withDuration: 0.7, animations: { () -> Void in
					self.dropLast()
					
				}) { (Finished) -> Void in
					
					User.logout()
				}
				
			})
			
			alertController.addAction(checkingAction)
			
			let savingsAction = UIAlertAction(title: "Feedback", style: .default, handler: {
					alert -> Void in
				
				print("Present Feedback")
				
				let feedbackController = UIAlertController(title: "Feedback", message: "", preferredStyle: .alert)
				
				feedbackController.addTextField { (textField : UITextField!) -> Void in
					textField.placeholder = "Message"
					textField.textAlignment = .center
				}
				
				let sendAction = UIAlertAction(title: "Send", style: .default, handler: {
					alert -> Void in
					
					
					let firstTextField = feedbackController.textFields![0] as UITextField
					
					Database.sendFeedback(firstTextField.text!)
					
				})
				
				let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
				feedbackController.addAction(cancelAction)
				
				feedbackController.addAction(sendAction)
				
				self.present(feedbackController, animated: true, completion: nil)
				
			})
				
			alertController.addAction(savingsAction)
			
			let appAction = UIAlertAction(title: "Ryde Account", style: .default, handler: {
				alert -> Void in
				
				Ecosystem.Ryde("")
				
			})
			
			alertController.addAction(appAction)
			
			
			let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
			alertController.addAction(cancelAction)
			
			self.present(alertController, animated: true, completion: nil)
			
		}
		
	}
	
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        User.currentLocation = [Float(locValue.latitude),Float(locValue.longitude)]
		
		var currentPlace: Place!
		
		for p in User.places {
			let area: CLLocation  = CLLocation(latitude: CLLocationDegrees(p.lat), longitude: CLLocationDegrees(p.long))
			let current: CLLocation = CLLocation(latitude: CLLocationDegrees(User.currentLocation[0]), longitude: CLLocationDegrees(User.currentLocation[1]))
			
			let dist: CLLocationDistance = area.distance(from: current)
			
			if dist < p.radius {
				currentPlace = p
				break
			}
		}
		
		if currentPlace != nil {
			dashView.placeNameLabel.text! = currentPlace!.name
		} else {
			dashView.placeNameLabel.text! = "Tap to Name Place"
		}
    }
	
	
    func appReopened(){
        checkContext()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		
		
		//run code on enter forground in app delegate
		if firstIn {
			animIn()
			firstIn = false
		} else {
			animIn()
		}
	}
    
    override func viewDidAppear(_ animated: Bool) {
        checkContext()
    }

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
		
		print("!!!!DYING!!!!")
	}
	

	func firstLaunch() {
        self.locationManager.requestAlwaysAuthorization()
	}
    
    func checkContext() {
        if (GroupData.get("widgetButton") == nil) {
            return
        }
        let action = GroupData.get("widgetButton") as! String?
        GroupData.clear("widgetButton")
        if action != nil {
            switch action! {
            case "budget":
                print("Go to budget")
				goToBudgeting(self)
                break
            case "bills":
                print("Go to bills")
				goToBilling(self)
                break
            case "debt":
                print("Go to reports")
				goToDebt()
                break
            case "addTrans":
                print("Add Transaction")
                break
            default:
                print("INVALID WIDGET BUTTON")
                break
            }
            //testLabel.text = getData(key: "widgetButton")!
            
        } else {
            //testLabel.text = ""
        }
    }
	
	func rotated() {
		if UIDevice.current.orientation.isLandscape {
			//print("Landscape")
			rotateOut()
		} else {
			//print("Portrait")
		}
	}
	
	func presentFirst () {
		self.dashView.capitalGraph.frame.origin.y -= 700
		self.dashView.debtGraph.frame.origin.y -= 685
		self.dashView.billsGraph.frame.origin.y -= 675
		self.dashView.budgetGraph.frame.origin.y -= 665
		self.dashView.assetGraph.frame.origin.y -= 655
		self.dashView.netGraph.frame.origin.y -= 645
	}
	
	func presentLast () {
			self.dashView.userNameLabel.frame.origin.x += 200
			self.dashView.usableLabel.frame.origin.x -= 300
			
			self.dashView.placeNameLabel.frame.origin.x -= 200
			
			self.dashView.incomeButton.frame.origin.x += 400
			self.dashView.transferButton.frame.origin.y -= 400
			self.dashView.spendButton.frame.origin.x -= 400
	}
	
	func withdrawFirst () {
			self.dashView.userNameLabel.frame.origin.x -= 200
			self.dashView.usableLabel.frame.origin.x += 300
			
			self.dashView.placeNameLabel.frame.origin.x += 200
			
			self.dashView.incomeButton.frame.origin.x -= 400
			self.dashView.transferButton.frame.origin.y += 400
			self.dashView.spendButton.frame.origin.x += 400
	}
	
	
	func dropFirst () {
		withdrawFirst()
	}
	
	func dropLast () {
		self.dashView.capitalGraph.frame.origin.y += self.view.frame.height
		self.dashView.debtGraph.frame.origin.y += self.view.frame.height
		self.dashView.billsGraph.frame.origin.y += self.view.frame.height
		self.dashView.budgetGraph.frame.origin.y += self.view.frame.height
		self.dashView.assetGraph.frame.origin.y += self.view.frame.height
		self.dashView.netGraph.frame.origin.y += self.view.frame.height
	}
	
	func raiseFirst () {
		withdrawFirst()
	}
	
	func raiseLast () {
		self.dashView.capitalGraph.frame.origin.y -= self.view.frame.height
		self.dashView.debtGraph.frame.origin.y -= self.view.frame.height
		self.dashView.billsGraph.frame.origin.y -= self.view.frame.height
		self.dashView.budgetGraph.frame.origin.y -= self.view.frame.height
		self.dashView.assetGraph.frame.origin.y -= self.view.frame.height
		self.dashView.netGraph.frame.origin.y -= self.view.frame.height
	}
	
	func pushLeftFirst () {
		withdrawFirst()
	}
	
	func pushLeftLast () {
		self.dashView.capitalGraph.frame.origin.x -= self.view.frame.width
		self.dashView.debtGraph.frame.origin.x -= self.view.frame.width
		self.dashView.billsGraph.frame.origin.x -= self.view.frame.width
		self.dashView.budgetGraph.frame.origin.x -= self.view.frame.width
		self.dashView.assetGraph.frame.origin.x -= self.view.frame.width
		self.dashView.netGraph.frame.origin.x -= self.view.frame.width
	}
	
	func pushRightFirst () {
		withdrawFirst()
	}
	
	func pushRightLast () {
		self.dashView.capitalGraph.frame.origin.x += self.view.frame.width
		self.dashView.debtGraph.frame.origin.x += self.view.frame.width
		self.dashView.billsGraph.frame.origin.x += self.view.frame.width
		self.dashView.budgetGraph.frame.origin.x += self.view.frame.width
		self.dashView.assetGraph.frame.origin.x += self.view.frame.width
		self.dashView.netGraph.frame.origin.x += self.view.frame.width
	}
	
	func rotateOut () {
		LoadingController.setNav(10)
		self.dismiss(animated: false, completion: nil)
	}
	
	func animIn () {
		self.dashView.placeNameLabel.isHidden = true
		
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.presentFirst()
			
		}) { (Finished) -> Void in
		}
		
		UIView.animate(withDuration: 1.0, animations: { () -> Void in
			self.presentLast()
			
		}) { (Finished) -> Void in
			self.view.layoutIfNeeded()
			self.dashView.placeNameLabel.isHidden = false
		}
		
		
	}
	
	func animUp () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.raiseFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.raiseLast()
			
		}) { (Finished) -> Void in
			GroupData.set("newTranConfig", data: "0")
			self.dismiss(animated: false, completion: nil)
		}
	}
	
	func animDown () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.dropFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.dropLast()
			
		}) { (Finished) -> Void in
			GroupData.set("newTranConfig", data: "2")
			self.dismiss(animated: false, completion: nil)
		}
	}
	
	func goToBank() {
		LoadingController.setNav(4)
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
	}
	
	func goToDebt() {
		LoadingController.setNav(5)
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
	}
	
	func goToBill() {
		LoadingController.setNav(6)
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
	}
	
	func goToBudget() {
		LoadingController.setNav(7)
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
	}
	
	func goToAsset() {
		LoadingController.setNav(8)
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
	}
	
	func goToBudgeting(_ sender: Any) {
		LoadingController.setNav(3)
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			
		}
	}
	
	func goToBilling(_ sender: Any) {
		LoadingController.setNav(2)
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.pushRightFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			
			self.pushRightLast()
			
		}) { (Finished) -> Void in
			
		}
	}
	
}


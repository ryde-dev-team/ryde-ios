//
//  SheetView.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/27/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class SheetView: UIViewController, UITableViewDataSource, UITableViewDelegate {
	
	@IBOutlet var accountList: UITableView!
	
	var accounts: [Account]!
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		NotificationCenter.default.addObserver(self, selector: #selector(SheetView.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
		
		Timer.scheduledTimer(timeInterval: 0.1,
		                                    target: self,
		                                    selector: #selector(self.animate),
		                                    userInfo: nil,
		                                    repeats: true)
	}
	
	@IBOutlet var detailView: UIView!
	@IBOutlet var nameLabel: UILabel!
	@IBOutlet var capitalLabel: UILabel!
	@IBOutlet var debtLabel: UILabel!
	@IBOutlet var billsLabel: UILabel!
	@IBOutlet var budgetLabel: UILabel!
	@IBOutlet var assetLabel: UILabel!
	
	@IBOutlet var uCapital: UILabel!
	@IBOutlet var uBudget: UILabel!
	@IBOutlet var usableLabel: UILabel!
	
	@IBOutlet var nCapital: UILabel!
	@IBOutlet var nDebt: UILabel!
	@IBOutlet var nAsset: UILabel!
	@IBOutlet var nBill: UILabel!
	@IBOutlet var netLabel: UILabel!
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		accountList.delegate = self
		accountList.dataSource = self
		
		accounts = User.banks + User.debts + User.cards + User.bills + User.budgets + User.goals + User.assets
		
		if User.usable < Double(0) {
			self.view.backgroundColor = Color.nBack
			accountList.backgroundColor = Color.nBack
			detailView.backgroundColor = Color.nChartFill
		} else {
			self.view.backgroundColor = Color.pBack
			accountList.backgroundColor = Color.pBack
			detailView.backgroundColor = Color.pChartFill
		}
		
		nameLabel.text = User.name
		capitalLabel.text = "Capital: " + Currency.format( User.capital.description)!
		debtLabel.text = "Debt: " + Currency.format( User.debt.description)!
		billsLabel.text = "Bills: " + Currency.format( User.bill.description)!
		budgetLabel.text = "Budget: " + Currency.format( User.budget.description)!
		assetLabel.text = "Asset: " + Currency.format( User.asset.description)!
		
		uCapital.text = "Capital: " + Currency.format( User.capital.description)!
		uBudget.text = "-Budget: " + Currency.format( User.budget.description)!
		usableLabel.text = "= Usable: " + Currency.format( User.usable.description)!
		
		nCapital.text = "Capital: " + Currency.format( User.capital.description)!
		nDebt.text = "+Debt: " + Currency.format( User.debt.description)!
		nAsset.text = "+Asset: " + Currency.format( User.asset.description)!
		nBill.text = "+Bill: " + Currency.format( User.bill.description)!
		netLabel.text = "= Net: " + Currency.format(User.net.description)!
		
		
		
		detailView.layer.shadowColor = UIColor.black.cgColor
		detailView.layer.shadowOpacity = 0.7
		detailView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
		detailView.layer.shadowRadius = 5
		
	}
	
	func animate() {
		for i in 0..<accounts.count {
			if accountList.cellForRow(at: [0,i]) != nil {
				if !(accountList.cellForRow(at: [0,i]) as! SheetCell).graph.idle {
					(accountList.cellForRow(at: [0,i]) as! SheetCell).graph.animate()
					return
				}
				
			}
		}
		
	}
	
	var firstIn = true
	
	override func viewWillAppear(_ animated: Bool) {
		
		
		//run code on enter forground in app delegate
		if !firstIn {
			animIn()
		} else {
			animIn()
			firstIn = false
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		print("!!!!DYING!!!!")
	}
	
	func rotated() {
		if !UIDevice.current.orientation.isPortrait {
			//print("Landscape")
		} else {
			LoadingController.setNav(1)
			self.animOut()
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return accounts.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "SheetCell", for: indexPath as IndexPath) as! SheetCell
		
		let account = accounts[indexPath.row]
		
		cell.name.text = account.name
		cell.balance.text = Currency.format(account.balance.description)
		
		cell.graph.updateData([account], balance: account.balance)
		cell.graph.nameLabel.text = ""
		
		cell.graph.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
		
		if account.balance < Double(0) {
			cell.balance.textColor = Color.nListText
			cell.backgroundColor = Color.nBack
		} else {
			cell.balance.textColor = Color.pListText
			cell.backgroundColor = Color.pBack
		}
		
		cell.layer.masksToBounds = false
		cell.clipsToBounds = false
		
		return cell
		
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
	}
	
	func animIn () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.inFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			self.inLast()
		}) { (Finished) -> Void in
			
		}
	}
	
	func animOut () {
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.outFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outLast()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		
	}
	
	func inFirst () {
		self.accountList.frame.origin.x -= 450
	}
	
	func inLast () {
		self.detailView.frame.origin.x += 250
	}
	
	func outFirst () {
		self.accountList.frame.origin.x += 450
	}
	
	func outLast () {
		self.detailView.frame.origin.x -= 350
	}
	
	
}

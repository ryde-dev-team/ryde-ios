//
//  LocalHistoryCell.swift
//  Ryde
//
//  Created by Ryan Dean on 1/19/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class LocalHistoryCell: UITableViewCell {
	
	@IBOutlet var tableViewCell: UITableViewCell!
	@IBOutlet var descriptionLabel: UILabel!
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		_ = Bundle.main.loadNibNamed("LocalHistoryCell", owner: self, options: nil)![0] as! UIView
		self.addSubview(tableViewCell)
		tableViewCell.frame = self.bounds
	}
}

//
//  RydeUser.swift
//  Ryde
//
//  Created by Ryan Dean on 1/18/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import Foundation

extension User {
	
	enum MovementStates {
		case still
		case walking
		case running
		case driving
	}
	
	
	static var doLogout = 0
	
	static var stateChangeEpoch: TimeInterval = Date().timeIntervalSince1970
	static var fromLocation: [Float] = [0.0,0.0]
	static var lastMovementState: MovementStates = .still
	
	static func load() {
		verifyAccess()
		Ecosystem.logLocal("Load Pyle Info")
		
		if downloadUserInfo() == false {
			Ecosystem.logLocal("Server Logout")
			logout()
		}
		
		
		places = Database.places()
	}
}

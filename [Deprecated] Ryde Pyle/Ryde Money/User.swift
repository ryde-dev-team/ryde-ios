//
//  User.swift
//  Ryde Money
//
//  Created by Ryan Dean on 10/29/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import Foundation
import UIKit

extension Bool {
	init<T : Integer>(_ integer: T){
		self.init(integer != 0)
	}
}

class User {
	static var name: String = ""
	static var rydeId: Int = -1
	static var authority: Int = 99
	static var playerId: String = ""
	static var email: String = ""
	static var sessionKey: String = ""
	
	
	static var capital: Double = 0.0 //Sum of checking and savings
	static var checking: Double = 0.0 //Sum of checking accounts
	static var savings: Double = 0.0 //SUm of savings accounts
	static var debt: Double = 0.0 //Sum of all debt and credit cards
	static var bill: Double = 0.0
	static var budget: Double = 0.0
	static var asset: Double = 0.0 //Sum of all assets
	static var usable: Double = 0.0
	static var net: Double = 0.0
	
	static var banks: [Account] = [] //A bank is created for every checking and savings accounts
	static var debts: [Account] = [] //Every debt account
	static var assets: [Account] = [] //Owned items and investment accounts
	static var cards: [Account] = [] //All credit cards
	
	static var bills: [Account] = [] //Every bill
	static var goals: [Account] = [] //All created goals
	static var budgets: [Account] = [] //All created budgets
	
	static var merchants: [Account] = [] //Collection of all past merchants
	
	static var currentBills: [Bill] = []
    
    static var currentLocation: [Float] = [0.0,0.0]
	
	static var minTran = 9999999
	static var maxTran = 0
	
	static var doLogout = 0
	
	static var viewing = 0 //0 - Root, 1 - Billing, 2 - Budgeting, 3 - Money Out, 4 - Money In, 5 - Other, 6 - Transfer
	
	static func login (username: String, password: String) -> Bool{
		let loginResponse = Database.login(username, pass: password)
		
		if loginResponse == nil {
			return false
		}
		
		sessionKey = loginResponse![0]
		rydeId = Int(loginResponse![1])!
		
		return downloadUserInfo()
	}
	
	static func logout () {
		Database.logout(sessionKey)
		
		sessionKey = ""
		rydeId = -1
		authority = 99
	}
	
	static func update() {
		
		capital = 0
		checking = 0
		savings = 0
		debt = 0
		asset = 0
		bill = 0
		budget = 0
		usable = 0
		net = 0
		
		
		for b in banks {
			capital += b.balance
			if b.type == 0 {
				checking += b.balance
			} else {
				savings += b.balance
			}
		}
		
		usable += capital
		net += capital
		
		for d in debts {
			debt += d.balance
		}
		
		for c in cards {
			debt += c.balance
		}
		
		net += debt
		
		for a in assets {
			asset += a.balance
		}
		
		net += asset
		
		for bi in bills {
            //bi.checkDue()
            
			bill += bi.balance
		}
		
		net += bill
		
		for g in goals {
			budget += g.balance
		}
		
		for b in budgets {
			budget += b.balance
		}
		
		usable -= budget
		
	}
	
	static func load() {
		verifyAccess()
		print("Loading...")
		
		if downloadUserInfo() == false {
			logout()
		}
		
		
		banks = Database.accounts(0) + Database.accounts(1)
		debts = Database.accounts(2)
		cards = Database.accounts(3)
		budgets = Database.accounts(4)
		bills = Database.accounts(5)
		goals = Database.accounts(6)
		assets = Database.accounts(7)
		
		currentBills = Database.bills()
		
		let accounts = banks + debts + cards + budgets + bills + goals + assets
		
		for a in accounts {
			a.transactions = Database.transactions(a)
		}
		
		update()
		return
	}
	
	static func save() {
		print("Saving...")
		
		let accounts: [Account] = banks + debts + assets + cards + bills + goals + budgets
		
		for account in accounts {
			account.save()
		}
		
		for bill in currentBills {
			bill.save()
		}
		
	}
	
	
	
	
	
	
    
    static func addLocationName(name: String, location: [Float]){
        var found = false
        for m in merchants {
            if m.name == name {
                found = true
                //m.addLocation(location: location)
                break
            }
        }
        
        if !(found) {
            //merchants.append(Merchant(name: name,location: location))
        }
    }
	
	static func record(tran: Transaction, accountId: Int, categoryId: Int, config: Int) {
		print("Record transaction")
		
		var account: Account!
		var category: Account!
		
		let accounts: [Account] = banks + debts + assets + cards + bills + goals + budgets
		
		for ac in accounts {
			if ac.id == accountId {
				account = ac
			}
			if ac.id == categoryId {
				category = ac
			}
		}
		
		if account == nil {
			print("[TODO] Make alert concerning no account")
			return
		}
		
		
		switch (config) {
		case 0:
			if category == nil {
				print("[TODO] Make alert concerning no category")
				return
			}
			print(account.name + ": " + Currency.format( tran.value.description)!)
			account.adjustBalance(tran: tran)
			
			let cloneTran = Transaction(id: tran.id + 1, time: tran.timestamp, value: tran.value, memo: tran.memo, cleared: 0, reconciled: 0)
			print(category.name + ": " + Currency.format( cloneTran.value.description)!)
			category.adjustBalance(tran: cloneTran)
			break
		case 1:
			if category == nil {
				print("[TODO] Make alert concerning no category")
				return
			}
			print(category.name + ": " + Currency.format( tran.value.description)!)
			category.adjustBalance(tran: tran)
			
			let cloneTran = Transaction(id: tran.id + 1, time: tran.timestamp, value: -tran.value, memo: tran.memo, cleared: 0, reconciled: 0)
			print(account.name + ": " + Currency.format( cloneTran.value.description)!)
			account.adjustBalance(tran: cloneTran)
			break
		case 2:
			print(account.name + ": " + Currency.format( tran.value.description)!)
			account.adjustBalance(tran: tran)
			break
		case 3:
			print(account.name + ": " + Currency.format( tran.value.description)!)
			account.adjustBalance(tran: tran)
			break
		case 4:
			account.adjustBalance(tran: tran)
		case 5:
			if category == nil {
				print("[TODO] Make alert concerning no category")
				return
			}
			category.adjustBalance(tran: tran)
			let cloneTran = Transaction(id: tran.id + 1, time: tran.timestamp, value: -tran.value, memo: tran.memo, cleared: 0, reconciled: 0)
			account.adjustBalance(tran: cloneTran)
		case 6:
			account.adjustBalance(tran: tran)
		default:
			print("INVALID CONFIG FOR USER transaction RECORD")
		}
		
		if account != nil {
			account.save()
		}
		
		if category != nil {
			category.save()
		}
	}
	
	static func verifyAccess () {
		let access = Database.systemVerify()
		
		if access != nil {
			if access! >= authority {
				doLogout = 0
			} else {
				doLogout = 1
			}
		} else {
			doLogout = 1
		}
	}
	
	static func downloadUserInfo () -> Bool{
		let info = Database.userInfo(rydeId)
		
		if info == nil {
			return false
		}
		
		name = info![0]
		authority = Int(info![1])!
		return true
	}

}

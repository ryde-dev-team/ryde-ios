//
//  Bill.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/29/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import Foundation

class Bill {
	var id: Int!
	var tranId: Int!
	
	var paid = 0
	var late = 0
	
	var due: TimeInterval = Date().timeIntervalSince1970
	
	init(id: Int, tranId: Int, paid: Int, due:TimeInterval, late: Int) {
		self.id = id
		self.tranId = tranId
		self.paid = paid
		self.due = due
		self.late = late
	}
	
	func save() {
		print("Saving Bill [" + id.description + "] " + tranId.description)
	
		Database.saveBill(self)
	}
	
	func pay() {
		paid = 1
		
		var list: [Bill] = []
		
		for b in User.currentBills {
			if b.paid == 0 {
				list.append(b)
			}
		}
		
		User.currentBills = list
	}
	
	func deleteSlim () {
		Database.deleteBill(self)
	}
}

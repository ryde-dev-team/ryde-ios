//
//  budgetingCell.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/22/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class BudgetingCell: UITableViewCell {
	
	@IBOutlet var amount: UITextField!
	@IBOutlet var name: UITextField!
	@IBOutlet var back: UIView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}
	
	func update() {
		if Currency.remove( amount.text!) != nil {
			var per = Float(Double(Currency.remove( amount.text!)!) / User.budget)
			
			
			
			if Float(Currency.remove( amount.text!)!) < Float(0) {
				per = -per
				name.textColor = UIColor(colorLiteralRed: 1 - (3.0 * per), green: 1 - (3.0 * per), blue: 1 - (3.0 * per), alpha: 1.0)
				amount.textColor = UIColor(colorLiteralRed: 1 - (3.0 * per), green: 25/255, blue: 25/255, alpha: 1.0)
				back.backgroundColor = UIColor(colorLiteralRed: 3.0 * per, green: 0/255, blue: 0/255, alpha: 1.0)
			} else if Float(Currency.remove( amount.text!)!) > Float(0) {
				name.textColor = UIColor(colorLiteralRed: 1 - (3.0 * per), green: 1 - (3.0 * per), blue: 1 - (3.0 * per), alpha: 1.0)
				amount.textColor = UIColor(colorLiteralRed: (5.0 * per), green: 255/255, blue: (5.0 * per), alpha: 1.0)
				back.backgroundColor = UIColor(colorLiteralRed: 0/255, green: 3.0 * per, blue: 0/255, alpha: 1.0)
			} else {
				name.textColor = UIColor.darkText
				amount.textColor = UIColor.darkText
				back.backgroundColor = UIColor.darkGray
			}
			
			if per.description == "nan" {
				name.textColor = UIColor.white
			}
			
			
		} else {
			amount.textColor = Color.pListText
			back.backgroundColor = Color.pListBack
		}
		
		if User.usable < Double(0) {
			self.backgroundColor = Color.nBack
		} else {
			self.backgroundColor = Color.pBack
		}
		
		back.layer.shadowColor = UIColor.black.cgColor
		back.layer.shadowOpacity = 0.7
		back.layer.shadowOffset = CGSize(width: -1.0, height: 3.0)
		back.layer.shadowRadius = 3
		
		back.layer.shouldRasterize = false
		
		self.clipsToBounds = false
		self.layer.masksToBounds = false
	}
}

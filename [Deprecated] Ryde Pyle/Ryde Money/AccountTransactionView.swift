//
//  BankTransactionView.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/4/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class AccountTransactionView: UIViewController, UITableViewDataSource, UITableViewDelegate {
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		
		Timer.scheduledTimer(timeInterval: 0.1,
		                     target: self,
		                     selector: #selector(self.animate),
		                     userInfo: nil,
		                     repeats: true)
	}
	
	
	@IBOutlet var swipeRight: UISwipeGestureRecognizer!
	@IBOutlet var balanceLabel: UILabel!
	@IBOutlet var accountGraph: LineGraphModule!
	@IBOutlet var transactionTable: UITableView!
	@IBOutlet var nameLabel: UILabel!
	@IBOutlet var settleButton: UIButton!
	@IBOutlet var spendButton: UIButton!
	
	@IBOutlet var clearedLabel: UILabel!
	@IBOutlet var reconciledLabel: UILabel!
	
	
	
	var account: Account?
	var accountType = 0
	var accountNum = 0
	
	@IBAction func reconcile(_ sender: Any) {
		// create the alert
		let alert = UIAlertController(title: "Settle Cleared Transactions", message: "Settling cleared transactions will bring your settled balance of " + Currency.format( account!.reconciledBalance.description)! + " to " + Currency.format( account!.clearedBalance.description)!, preferredStyle: UIAlertControllerStyle.alert)
		
		// add the actions (buttons)
		alert.addAction(UIAlertAction(title: "Settle", style: UIAlertActionStyle.default, handler: { action in
			
			for t in self.account!.transactions {
				if t.reconciled == 0 && t.cleared == 1 {
					t.reconcile()
				}
			}
			
			
			
			self.clearedLabel.text = Currency.format( self.account!.clearedBalance.description)! + " CLR"
			self.reconciledLabel.text = "SET " + Currency.format( self.account!.reconciledBalance.description)!
			self.transactionTable.reloadData()
		}))
		alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
		
		// show the alert
		self.present(alert, animated: true, completion: nil)
		
		
		
	}
	
	@IBAction func deleteAccount(_ sender: Any) {
		let alertController = UIAlertController(title: "Delete " + account!.name, message: "This will permanently delete " + account!.transactions.count.description + " transactions and the account " + account!.name, preferredStyle: .alert)
		
		let checkingAction = UIAlertAction(title: "Delete", style: .destructive, handler: {
			alert -> Void in
			
			
			self.account!.delete()
			
			User.load()
			
			self.dismiss(animated: true, completion: nil)
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		
		alertController.addAction(checkingAction)
		alertController.addAction(cancelAction)
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	
	func animate () {
		if !accountGraph.idle {
			accountGraph.animate()
		}
		
	}
	
	func backToBank(_ sender: AnyObject) {
		animOut()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return account!.transactions.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionDetailCell", for: indexPath as IndexPath) as! TransactionDetailCell
		
		if indexPath.row < account!.transactions.count {
			let tran = account!.transactions[account!.transactions.count - (indexPath.row + 1)]
			
			
			
			let dateFormatter = DateFormatter()
			
			dateFormatter.dateFormat = "MM/dd/yy"
			cell.dateLabel.text = dateFormatter.string(from: Date(timeIntervalSince1970: tran.timestamp))
			
			cell.merchantLabel.text = tran.memo
			cell.priceLabel.text = Currency.format( tran.value.description)
			
			if tran.value < 0 {
				cell.priceLabel.textColor = UIColor.red
			} else {
				cell.priceLabel.textColor = UIColor.green
			}
			
			
			
			if tran.cleared == 1 {
				cell.cleared = true
			}
			
			if tran.hidden {
				cell.disabled = true
			}
			
			if tran.reconciled == 1 || tran.deleted == 1 {
				cell.reconciled = true
			}
			
		}
		
		cell.update()
		
		if cell.reconciled {
			cell.backgroundColor = self.view.backgroundColor
		}
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let c = tableView.cellForRow(at: indexPath) as! TransactionDetailCell
		let tran = account!.transactions[account!.transactions.count - (indexPath.row + 1)]
		
		let alertController = UIAlertController(title: "Transaction Options", message: "For: " + tran.memo, preferredStyle: .alert)
		
		var tit = "Clear"
		if c.cleared {
			tit = "Unclear"
		}
		
		let checkingAction = UIAlertAction(title: tit, style: .default, handler: {
			alert -> Void in
			
			c.cleared = !c.cleared
			self.account!.transactions[self.account!.transactions.count - (indexPath.row + 1)].toggleClear()
			c.update()
			self.clearedLabel.text = Currency.format( self.account!.clearedBalance.description)! + " CLR"
			self.reconciledLabel.text = "SET " + Currency.format( self.account!.reconciledBalance.description)!
		})
		
		
		
		alertController.addAction(checkingAction)
		
		if !c.cleared {
			let savingsAction = UIAlertAction(title: "Delete", style: .destructive, handler: {
				alert -> Void in
			
				tran.delete()
				self.balanceLabel.text = Currency.format( self.account!.balance.description)
				self.transactionTable.reloadData()
			})
			
			alertController.addAction(savingsAction)
		}
		
		
		
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		alertController.addAction(cancelAction)
		
		self.present(alertController, animated: true, completion: nil)
		
		
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		swipeRight.addTarget(self, action: #selector(backToBank(_:)))
		self.view.addGestureRecognizer(swipeRight)
		self.view.isUserInteractionEnabled = true
		
		transactionTable.delegate = self
		transactionTable.dataSource = self
		
		let at = GroupData.get("accountType")! as! String
		let an = GroupData.get("accountNum")! as! String
		
		accountType = Int(at)!
		accountNum = Int(an)!
		
		self.settleButton.backgroundColor = Color.pButton
		self.spendButton.backgroundColor = Color.nButton
		
		settleButton.layer.shadowColor = UIColor.black.cgColor
		settleButton.layer.shadowOpacity = 0.5
		settleButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		settleButton.layer.shadowRadius = 4
		settleButton.layer.cornerRadius = 5
		
		spendButton.layer.shadowColor = UIColor.black.cgColor
		spendButton.layer.shadowOpacity = 0.5
		spendButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		spendButton.layer.shadowRadius = 4
		spendButton.layer.cornerRadius = 5
		
		if accountType == 0 {
			account = User.banks[accountNum]
		} else if accountType == 1 {
			if accountNum < User.debts.count {
				account = User.debts[accountNum]
			} else {
				account = User.cards[accountNum - User.debts.count]
			}
		} else if accountType == 2 {
			account = User.bills[accountNum]
		} else if accountType == 3 {
			if accountNum < User.budgets.count {
				account = User.budgets[accountNum]
			} else {
				account = User.goals[accountNum - User.budgets.count]
			}
		} else if accountType == 4 {
			account = User.assets[accountNum]
		}
		
		
		if account != nil {
		
			balanceLabel.text = Currency.format( account!.balance.description)
			
			clearedLabel.text = Currency.format( account!.clearedBalance.description)! + " CLR"
			
			reconciledLabel.text = "SET " + Currency.format( account!.reconciledBalance.description)!
		
			if account!.balance < 0 {
				balanceLabel.textColor = Color.nText
				self.view.backgroundColor = Color.nBack
				transactionTable.backgroundColor = Color.nBack
			} else {
				balanceLabel.textColor = Color.pText
				self.view.backgroundColor = Color.pBack
				transactionTable.backgroundColor = Color.pBack
			}
		
		
			nameLabel.text = account!.name
		
			accountGraph.updateData([account!], balance: account!.balance)
		}
		
	
	}
	
	var firstIn = true
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if firstIn {
			animIn()
			firstIn = false
		} else {
			
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	func animIn () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.accountGraph.frame.origin.x -= self.view.frame.width
			self.transactionTable.frame.origin.x -= self.view.frame.width
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			self.balanceLabel.frame.origin.x -= self.view.frame.width
			self.clearedLabel.frame.origin.x -= self.view.frame.width
			self.reconciledLabel.frame.origin.x -= self.view.frame.width
			
			self.spendButton.frame.origin.x -= self.view.frame.width
			self.settleButton.frame.origin.x -= self.view.frame.width
		}) { (Finished) -> Void in
			self.view.layoutSubviews()
		}
	}
	
	func animOut () {
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.accountGraph.frame.origin.x += self.view.frame.width
			self.transactionTable.frame.origin.x += self.view.frame.width
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.balanceLabel.frame.origin.x += self.view.frame.width
			self.clearedLabel.frame.origin.x += self.view.frame.width
			self.reconciledLabel.frame.origin.x += self.view.frame.width
			
			self.spendButton.frame.origin.x += self.view.frame.width
			self.settleButton.frame.origin.x += self.view.frame.width
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		
	}
}

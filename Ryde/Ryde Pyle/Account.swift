//
//  Account.swift
//  Ryde Money
//
//  Created by Ryan Dean on 11/20/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import Foundation

class Account {
	var id: Int
	var type: Int //0 - Checking Bank, 1 - Savings Bank, 2 - Debt, 3 - Credit, 4 - Budget, 5 - Bill, 6 - Goal, 7 - Asset
	
	var name: String
	var balance: Double
	
	var clearedBalance: Double
	var reconciledBalance: Double
	
	var limit: Double
	
	var transactions: [Transaction] = []
	
	init(name: String) {
		self.id = -1
		self.type = -1
		self.balance = 0
		self.limit = 999999.99
		self.name = name
		self.clearedBalance = 0
		self.reconciledBalance = 0
	}
	
	init( id: Int, type: Int, name: String, balance: Double, limit: Double, clear: Double, rec: Double) {
		self.id = id
		self.type = type
		self.name = name
		self.balance = balance
		self.limit = limit
		self.clearedBalance = clear
		self.reconciledBalance = rec
	}
	
	init( id: Int, type: Int, name: String, balance: Double, clear: Double, rec: Double) {
		self.id = id
		self.type = type
		self.name = name
		self.balance = balance
		self.limit = 999999.99
		self.clearedBalance = clear
		self.reconciledBalance = rec
	}
	
	func adjustBalance(tran: Transaction) {
		balance += tran.value
		tran.account = self
		transactions.append(tran)
		
		transactions.sort {$0.timestamp < $1.timestamp}
	}
	
	func addTransaction(tran: Transaction) {
		tran.account = self
		transactions.append(tran)
		
		transactions.sort {$0.timestamp < $1.timestamp}
	}
	
	func printTrans() {
		for t in transactions {
			print(t.timestamp.description + " v: " + t.value.description)
		}
	}
	
	func saveTransactions() {
		for transaction in transactions {
			transaction.save()
		}
	}
	
	func save() {
		Ecosystem.logLocal("Saving Account [" + id.description + "] " + name)
		
		Database.saveAccount(self)
		
		for t in transactions {
			t.save()
		}
	}
	
	func saveSlim() {
		Ecosystem.logLocal("[SLIM] Saving Account [" + id.description + "] " + name)
		
		Database.saveAccount(self)
	}
	
	func delete () {
		Ecosystem.logLocal("Delete Account [" + id.description + "] " + name)
		Database.deleteAccount(self)
		
		for t in transactions {
			t.delete()
		}
	}
}

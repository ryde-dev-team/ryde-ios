//
//  LoginView.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 1/7/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class LoginView: UIViewController, UITextFieldDelegate {
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		NotificationCenter.default.addObserver(self, selector: #selector(BudgetingView.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BudgetingView.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
		
		Timer.scheduledTimer(timeInterval: 15,
		                                   target: self,
		                                   selector: #selector(self.checkSystem),
		                                   userInfo: nil,
		                                   repeats: true)
	}
	
	
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var logoLabel: UILabel!
	@IBOutlet var emailField: UITextField!
	@IBOutlet var passwordField: UITextField!
	@IBOutlet var loginButton: UIButton!
	@IBOutlet var versionLabel: UILabel!
	@IBOutlet var motdLabel: UILabel!
	@IBOutlet var loginIssueButton: UIButton!
	
	@IBOutlet var activityInd: UIActivityIndicatorView!
	
	var firstIn = true
	var skip = false
	var access: Int!
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		checkSystem()
		
		if skip {
			titleLabel.isHidden = true
			logoLabel.isHidden = true
			emailField.isHidden = true
			passwordField.isHidden = true
			loginButton.isHidden = true
			loginIssueButton.isHidden = true
			versionLabel.isHidden = true
			motdLabel.isHidden = true
			
			activityInd.isHidden = false
			activityInd.startAnimating()
		} else {
			titleLabel.isHidden = false
			logoLabel.isHidden = false
			emailField.isHidden = false
			passwordField.isHidden = false
			loginButton.isHidden = false
			loginIssueButton.isHidden = false
			versionLabel.isHidden = false
			motdLabel.isHidden = false
			
			activityInd.isHidden = true
			activityInd.stopAnimating()
			
			if firstIn {
				animIn()
				firstIn = false
			}
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if skip {
			User.load()
			self.performSegue(withIdentifier: "login2root", sender: self)
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.view.backgroundColor = Color.pBack
		self.emailField.backgroundColor = Color.pChartBack
		self.passwordField.backgroundColor = Color.pChartBack
		
		self.emailField.delegate = self
		self.passwordField.delegate = self
		
		self.versionLabel.text = System.version()
		self.motdLabel.text = ""
		
		self.loginButton.layer.cornerRadius = 5
		self.loginButton.layer.shadowOpacity = 0.7
		self.loginButton.layer.shadowRadius = 5
		self.loginButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		self.loginButton.layer.shadowColor = UIColor.black.cgColor
		
		self.emailField.layer.masksToBounds = false
		self.emailField.layer.shadowOpacity = 0.7
		self.emailField.layer.shadowRadius = 5
		self.emailField.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		self.emailField.layer.shadowColor = UIColor.black.cgColor
		
		self.passwordField.layer.masksToBounds = false
		self.passwordField.layer.shadowOpacity = 0.7
		self.passwordField.layer.shadowRadius = 5
		self.passwordField.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		self.passwordField.layer.shadowColor = UIColor.black.cgColor
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	func animIn () {
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.inFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1.5, animations: { () -> Void in
			self.inLast()
		}) { (Finished) -> Void in
			
		}
	}
	
	func animOut () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			self.outLast()
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "login2root", sender: self)
		}
	}
	
	func inFirst () {
		self.titleLabel.frame.origin.y -= 800
		self.logoLabel.frame.origin.y -= 650
		
	}
	
	func inLast () {
		self.emailField.frame.origin.y  -= 600
		self.passwordField.frame.origin.y  -= 550
		self.loginButton.frame.origin.y  -= 500
		self.loginIssueButton.frame.origin.y -= 450
		self.versionLabel.frame.origin.y -= 400
		self.motdLabel.frame.origin.y -= 350
	}
	
	func outFirst () {
		self.titleLabel.frame.origin.y += 600
		self.logoLabel.frame.origin.y += 600
		self.loginButton.frame.origin.y  += 600
		self.loginIssueButton.frame.origin.y += 600
		self.versionLabel.frame.origin.y += 600
		self.motdLabel.frame.origin.y += 600
	}
	
	func outLast () {
		self.emailField.frame.origin.y  += 600
		self.passwordField.frame.origin.y  += 600
	}
	
	@IBAction func loginPressed(_ sender: Any) {
		//self.view.endEditing(true)
		if User.login(username: emailField.text!, password: passwordField.text!) {
			if access >= User.authority {
				User.load()
				animOut()
			} else {
				//user not high enough authority
				motdLabel.text = "Your user account has not been given high enough access authority for this version, try updating or logging in again."
			}
		} else {
			logoLabel.text = "Try Again"
		}
	}
	
	@IBAction func loginIssuesPressed(_ sender: Any) {
		let feedbackController = UIAlertController(title: "Report Login Issue", message: "", preferredStyle: .alert)
		
		feedbackController.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = "Message"
			textField.textAlignment = .center
		}
		
		let sendAction = UIAlertAction(title: "Send", style: .default, handler: {
			alert -> Void in
			
			
			let firstTextField = feedbackController.textFields![0] as UITextField
			
			Database.sendFeedback(firstTextField.text!)
			
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		feedbackController.addAction(cancelAction)
		
		feedbackController.addAction(sendAction)
		
		self.present(feedbackController, animated: true, completion: nil)
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.endEditing(true)
		self.view.endEditing(true)
		return false
	}
	
	func keyboardWillShow(notification: NSNotification) {
		
		
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y == 0{
				self.view.frame.origin.y -= keyboardSize.height / 2
				
			}
			else {
				
			}
			
			
		}
		
	}
	
	func keyboardWillHide(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y != 0 {
				self.view.frame.origin.y += keyboardSize.height / 2
				
			}
			else {
				
			}
		}
	}
	
	func checkSystem () {
		var sysInfo = Database.systemVerify()
		
		self.access = Int(sysInfo![0])
		self.motdLabel.text = sysInfo![1]
		
		let verifyResponse = Database.verifySesssion()
		
		if verifyResponse == -1{
			skip = false
			User.doLogout = 1
		}
		
		if access == nil {
			skip = false
			User.doLogout = 1
			motdLabel.text = "This version of the app no longer has access, please update your app."
		}
		
		
		let infoString = Database.userInfo(verifyResponse)
		
		if infoString == nil {
			skip = false
			User.doLogout = 1
		} else {
			User.rydeId = verifyResponse
			User.name = infoString![0]
			User.authority = Int(infoString![1])!
			
			if access >= User.authority {
				User.doLogout = 0
				skip = true
			} else {
				skip = false
				User.doLogout = 1
				motdLabel.text = "Your user account has not been given high enough access authority for this version, try updating or logging in again."
			}
		}
		
		
		
//		if User.verifySession() {
//			if access != nil {
//				if access >= User.authority {
//					User.doLogout = 0
//					skip = true
//				} else {
//					skip = false
//					User.doLogout = 1
//					//user not high enough authority
//					motdLabel.text = "Your user account has not been given high enough access authority for this version, try updating or logging in again."
//				}
//			} else {
//				skip = false
//				User.doLogout = 1
//				//version doesn't have access
//				motdLabel.text = "This version of the app no longer has access, please update your app."
//			}
//		} else {
//			skip = false
//			User.doLogout = 1
//			if access == nil {
//				//no version doesn't have access
//				User.doLogout = 1
//				motdLabel.text = "Unable to access RydeSoft servers."
//			}
//		}
	}
	
}

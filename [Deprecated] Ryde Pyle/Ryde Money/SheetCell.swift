//
//  SheetCell.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/27/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class SheetCell: UITableViewCell {
	
	@IBOutlet var name: UILabel!
	
	@IBOutlet var graph: LineGraphModule!
	
	@IBOutlet var balance: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}
	
	func update() {
		
	}
}

//
//  Math.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 1/13/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class Math {
	static func random(_ from: Float, max: Float) -> Float{
		return Float(arc4random()) / Float(UINT32_MAX) * abs(from - max) + min(from, max)
	}
}

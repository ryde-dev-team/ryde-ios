//
//  TransactionDetailCell.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/6/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class TransactionDetailCell: UITableViewCell {
	
	@IBOutlet var dateLabel: UILabel!
	@IBOutlet var merchantLabel: UILabel!
	@IBOutlet var priceLabel: UILabel!
	
	var disabled = false
	var cleared = false
	var reconciled = false
	
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)

	}
	
	func update() {
		if priceLabel.text != nil {
			if Float(Currency.remove( priceLabel.text!)!) < Float(0) {
				priceLabel.textColor = Color.nListText
				self.contentView.backgroundColor = Color.nListBack
			} else {
				priceLabel.textColor = Color.pListText
				self.contentView.backgroundColor = Color.pListBack
			}
		}
		
		if disabled {
			priceLabel.textColor = UIColor.black
			self.contentView.backgroundColor = UIColor.darkGray
		}
		
		if cleared {
			priceLabel.textColor = UIColor.black
			self.contentView.backgroundColor = UIColor.gray
		}
		
		if reconciled {
			dateLabel.text = ""
			merchantLabel.text = ""
			priceLabel.text = ""
			self.contentView.backgroundColor = UIColor.clear
		}
		
		
		setNeedsDisplay()
	}
}

//
//  User.swift
//  Ryde Money
//
//  Created by Ryan Dean on 10/29/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import Foundation
import UIKit

extension Bool {
	init<T : Integer>(_ integer: T){
		self.init(integer != 0)
	}
}

class User {
	static var name: String = ""
	static var rydeid: Int = -1
	static var authority: Int = 99
	static var playerId: String = ""
	static var email: String = ""
	static var sessionKey: String = ""
	
	
	
    
    static var currentLocation: [Float] = [0.0,0.0]
	static var places: [Place] = []
	
	
	
	static func login (username: String, password: String) -> Bool{
		let loginResponse = Database.login(username, pass: password)
		
		if loginResponse == nil {
			Ecosystem.logLocal("Login Failed")
			return false
		}
		
		sessionKey = loginResponse![0]
		rydeid = Int(loginResponse![1])!
		
		Ecosystem.logLocal("[" + rydeid.description + "] Logged In")
		
		return downloadUserInfo()
	}
	
	static func logout () {
		Database.logout(sessionKey)
		
		sessionKey = ""
		rydeid = -1
		authority = 99
		Ecosystem.logLocal("Logged Out Locally")
	}
	
	static func downloadUserInfo () -> Bool{
		let info = Database.userInfo(rydeid)
		
		if info == nil {
			return false
		}
		
		name = info![0]
		authority = Int(info![1])!
		
		Ecosystem.logLocal(name + " Info Update")
		return true
	}
	
	static func verifyAccess () {
		_ = Database.systemVerify()
	}

}

//
//  ViewController.swift
//  Ryde Money
//
//  Created by Ryan Dean on 10/26/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit
import CoreLocation


class RootView: UIViewController, CLLocationManagerDelegate {
	
	deinit {
		NotificationCenter.default.removeObserver(self)
		updateTimer.invalidate()
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		
		updateTimer = Timer.scheduledTimer(timeInterval: 0.5,
		                                   target: self,
		                                   selector: #selector(self.update),
		                                   userInfo: nil,
		                                   repeats: true)
		
		animateTimer = Timer.scheduledTimer(timeInterval: 0.1,
		                                   target: self,
		                                   selector: #selector(self.animate),
		                                   userInfo: nil,
		                                   repeats: true)
		
		NotificationCenter.default.addObserver(self, selector: #selector(RootView.appReopened), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
		
		
		
		NotificationCenter.default.addObserver(self, selector: #selector(RootView.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
		
		if CLLocationManager.locationServicesEnabled() {
			locationManager.delegate = self
			locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
			locationManager.startUpdatingLocation()
		}
	}
	
	var updateTimer: Timer!
	var animateTimer: Timer!
	
    let locationManager = CLLocationManager()

    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
	
	@IBOutlet var capitalGraph: LineGraphModule!
	@IBOutlet var currentCapitalLabel: UILabel!
	
	
	@IBOutlet var debtGraph: LineGraphModule!
	@IBOutlet var billsGraph: LineGraphModule!
	@IBOutlet var budgetGraph: LineGraphModule!
	@IBOutlet var assetGraph: LineGraphModule!
	@IBOutlet var netGraph: LineGraphModule!
	
	@IBOutlet var swipeUp: UISwipeGestureRecognizer!
	
	@IBOutlet var swipeDown: UISwipeGestureRecognizer!
	
	@IBOutlet var swipeLeft: UISwipeGestureRecognizer!
	
	@IBOutlet var swipeRight: UISwipeGestureRecognizer!
	
	@IBOutlet var graphTap: UITapGestureRecognizer!
	
	@IBOutlet var incomeButton: UIButton!
	@IBOutlet var spendButton: UIButton!
	@IBOutlet var transferButton: UIButton!
	
	@IBOutlet var capitalBalance: UILabel!
	@IBOutlet var debtBalance: UILabel!
	@IBOutlet var billBalance: UILabel!
	@IBOutlet var budgetBalance: UILabel!
	@IBOutlet var assetBalance: UILabel!
	@IBOutlet var netBalance: UILabel!
	
	
	var firstIn = true
	
    @IBAction func addTransaction(_ sender: Any) {
		animUp()
    }
	
	@IBAction func addIncome(_ sender: Any) {
		animDown()
	}
	
	@IBAction func addTransfer(_ sender: Any) {
		User.viewing = 6
		GroupData.set("newTranConfig", data: "1")
		self.performSegue(withIdentifier: "root2addLeft", sender: self)
	}
	
	
	func animate() {
		if !capitalGraph!.idle {
			capitalGraph!.animate()
			return
		}
		if !debtGraph!.idle {
			debtGraph!.animate()
			return
		}
		if !billsGraph!.idle {
			billsGraph!.animate()
			return
		}
		if !budgetGraph!.idle {
			budgetGraph!.animate()
			return
		}
		if !assetGraph!.idle {
			assetGraph!.animate()
			return
		}
		if !netGraph!.idle {
			netGraph!.animate()
			return
		}
		return
	}
    
    func update()
    {
		
		User.update()
		
		if User.doLogout == 1 {
			print("Automatic Logout")
			User.logout()
			self.dismiss(animated: false, completion: nil)
		}
		
		
		let dateFormatter = DateFormatter()
		
		dateFormatter.dateFormat = "MM/dd/yy"
		
		
		
		if capitalGraph != nil {
			capitalGraph.updateData(User.banks, balance: User.capital)
			
			capitalBalance.text! = Currency.format( User.capital.description)!
			
		}
		
		if debtGraph != nil {
			debtGraph.updateData(User.debts + User.cards, balance: User.debt)
			
			debtBalance.text! = Currency.format( User.debt.description)!
			
		}
		
		if billsGraph != nil {
			billsGraph.updateData(User.bills, balance: User.bill)
			
			billBalance.text! = Currency.format( User.bill.description)!
			
		}
		
		if budgetGraph != nil {
			budgetGraph.updateData(User.budgets + User.goals, balance: User.budget)
			
			budgetBalance.text! = Currency.format( User.budget.description)!
			
		}
		
		if assetGraph != nil {
			assetGraph.updateData(User.assets, balance: User.asset)
			
			assetBalance.text! = Currency.format( User.asset.description)!
			
		}
		
		if netGraph != nil {
			let accounts: [Account] = User.banks + User.debts + User.cards + User.assets + User.bills
			
			netGraph.updateData(accounts, balance: User.net)
			netBalance.text! = Currency.format( User.net.description)!
			
		}
		
		nameLabel.text = User.name
		
		currentCapitalLabel.text = Currency.format( User.usable.description)
		
		if User.usable < Double(0) {
			currentCapitalLabel.textColor = Color.nText
			self.view.backgroundColor = Color.nBack
		} else {
			currentCapitalLabel.textColor = Color.pText
			self.view.backgroundColor = Color.pBack
		}
		
		self.transferButton.backgroundColor = UIColor.gray
		self.incomeButton.backgroundColor = Color.pButton
		self.spendButton.backgroundColor = Color.nButton
		
        
        
        for m in User.merchants {
            let cx = User.currentLocation[0]
            let cy = User.currentLocation[1]
            let x = Float(0.0)//m.location[0]
            let y = Float(0.0)//m.location[1]
                
				
            if x + 0.0004 >= cx && x - 0.0004 <= cx {
                if y + 0.0004 >= cy && y - 0.0004 <= cy {
                    locationLabel.text = "Location = " + m.name
                    return
                }
            }
        }
        
        locationLabel.text = "Location = \(User.currentLocation[0]) \(User.currentLocation[1])"
		
    }
	
    
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		if GroupData.get("launchedBefore") == nil {
			firstLaunch()
			GroupData.set("launchedBefore", data: "1" as AnyObject)
		}
		
		
		
		
		graphTap.addTarget(self, action: #selector(graphTapped(_:)))
		self.view.addGestureRecognizer(graphTap)
		self.view.isUserInteractionEnabled = true
		
		swipeUp.addTarget(self, action: #selector(addTransaction(_:)))
		self.view.addGestureRecognizer(swipeUp)
		self.view.isUserInteractionEnabled = true
		
		swipeDown.addTarget(self, action: #selector(addIncome(_:)))
		self.view.addGestureRecognizer(swipeDown)
		self.view.isUserInteractionEnabled = true
		
		swipeLeft.addTarget(self, action: #selector(goToBudgeting(_:)))
		self.view.addGestureRecognizer(swipeLeft)
		self.view.isUserInteractionEnabled = true
		
		swipeRight.addTarget(self, action: #selector(goToBilling(_:)))
		self.view.addGestureRecognizer(swipeRight)
		self.view.isUserInteractionEnabled = true
		
		spendButton.layer.shadowColor = UIColor.black.cgColor
		spendButton.layer.shadowOpacity = 0.7
		spendButton.layer.shadowOffset = CGSize(width: 0.0, height: -4.0)
		spendButton.layer.shadowRadius = 5
		spendButton.layer.cornerRadius = 5
		
		incomeButton.layer.shadowColor = UIColor.black.cgColor
		incomeButton.layer.shadowOpacity = 0.7
		incomeButton.layer.shadowOffset = CGSize(width: -1.0, height: -3.0)
		incomeButton.layer.shadowRadius = 5
		incomeButton.layer.cornerRadius = 5
		
		transferButton.layer.shadowColor = UIColor.black.cgColor
		transferButton.layer.shadowOpacity = 0.7
		transferButton.layer.shadowOffset = CGSize(width: 1.0, height: -3.0)
		transferButton.layer.shadowRadius = 5
		transferButton.layer.cornerRadius = 5
		
		update()
	}
	
	func graphTapped(_ sender: AnyObject){
		let point = graphTap.location(in: self.view)
		let view = self.view.hitTest(point, with: nil)
		
		if (view?.center.equalTo(debtGraph.center))! {
			print("Debt")
            goToDebt()
		} else if (view?.center.equalTo(billsGraph.center))! {
			print("Bills")
			goToBill()
		} else if (view?.center.equalTo(budgetGraph.center))! {
			print("Budget")
			goToBudget()
		} else if (view?.center.equalTo(capitalGraph.center))! {
			print("Banks")
			goToBank()
		} else if (view?.center.equalTo(assetGraph.center))! {
			print("Asset")
			goToAsset()
		} else if (view?.center.equalTo(netGraph.center))! {
			print("Net")
        } else if (view?.center.equalTo(locationLabel.center))! {
            print("Location")
            let alert = UIAlertController(title: "New Location", message: "What would you like to call this place?", preferredStyle: .alert)
            
            alert.addTextField { (textField) in
                textField.text = "Merchant Name"
            }
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                let textField = alert.textFields![0]
                User.addLocationName(name: textField.text!, location: User.currentLocation)
            }))
            
            self.present(alert, animated: true, completion: nil)
		} else if (view?.center.equalTo(nameLabel.center))! {
			let alertController = UIAlertController(title: "Options", message: "", preferredStyle: .alert)
			
			let checkingAction = UIAlertAction(title: "Logout", style: .destructive, handler: {
				alert -> Void in
				
				UIView.animate(withDuration: 0.3, animations: { () -> Void in
					self.dropFirst()
					
				}) { (Finished) -> Void in
					
				}
				
				UIView.animate(withDuration: 0.7, animations: { () -> Void in
					self.dropLast()
					
				}) { (Finished) -> Void in
					print("Logout")
					
					User.logout()
					
					self.dismiss(animated: false, completion: nil)
				}
				
			})
			
			alertController.addAction(checkingAction)
			
			let savingsAction = UIAlertAction(title: "Feedback", style: .default, handler: {
					alert -> Void in
				
				print("Present Feedback")
				
				let feedbackController = UIAlertController(title: "Feedback", message: "", preferredStyle: .alert)
				
				feedbackController.addTextField { (textField : UITextField!) -> Void in
					textField.placeholder = "Message"
					textField.textAlignment = .center
				}
				
				let sendAction = UIAlertAction(title: "Send", style: .default, handler: {
					alert -> Void in
					
					
					let firstTextField = feedbackController.textFields![0] as UITextField
					
					Database.sendFeedback(firstTextField.text!)
					
				})
				
				let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
				feedbackController.addAction(cancelAction)
				
				feedbackController.addAction(sendAction)
				
				self.present(feedbackController, animated: true, completion: nil)
				
			})
				
			alertController.addAction(savingsAction)
			
			
			
			
			let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
			alertController.addAction(cancelAction)
			
			self.present(alertController, animated: true, completion: nil)
			
		}
		
	}
	
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        User.currentLocation = [Float(locValue.latitude),Float(locValue.longitude)]
    }
	
    
    func appReopened(){
        checkContext()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		
		
		//run code on enter forground in app delegate
		if firstIn {
			User.viewing = 0
			animIn()
			firstIn = false
		} else {
			animIn()
		}
	}
    
    override func viewDidAppear(_ animated: Bool) {
        checkContext()
    }

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
		
		print("!!!!DYING!!!!")
	}
	

	func firstLaunch() {
        self.locationManager.requestAlwaysAuthorization()
	}
    
    func checkContext() {
        if (GroupData.get("widgetButton") == nil) {
            return
        }
        let action = GroupData.get("widgetButton") as! String?
        GroupData.clear("widgetButton")
        if action != nil {
            switch action! {
            case "budget":
                print("Go to budget")
				goToBudgeting(self)
                break
            case "bills":
                print("Go to bills")
				goToBilling(self)
                break
            case "debt":
                print("Go to reports")
				goToDebt()
                break
            case "addTrans":
                print("Add Transaction")
                break
            default:
                print("INVALID WIDGET BUTTON")
                break
            }
            //testLabel.text = getData(key: "widgetButton")!
            
        } else {
            //testLabel.text = ""
        }
    }
	
	func rotated() {
		if UIDevice.current.orientation.isLandscape {
			//print("Landscape")
			rotateOut()
		} else {
			//print("Portrait")
		}
	}
	
	func presentFirst () {
		if User.viewing == 0 {
			self.capitalGraph.frame.origin.y -= 700
			self.debtGraph.frame.origin.y -= 685
			self.billsGraph.frame.origin.y -= 675
			self.budgetGraph.frame.origin.y -= 665
			self.assetGraph.frame.origin.y -= 655
			self.netGraph.frame.origin.y -= 645
		} else if User.viewing == 1 {
			self.capitalGraph.frame.origin.x -= self.view.frame.width
			self.debtGraph.frame.origin.x -= self.view.frame.width
			self.billsGraph.frame.origin.x -= self.view.frame.width
			self.budgetGraph.frame.origin.x -= self.view.frame.width
			self.assetGraph.frame.origin.x -= self.view.frame.width
			self.netGraph.frame.origin.x -= self.view.frame.width
		} else if User.viewing == 2 {
			self.capitalGraph.frame.origin.x += self.view.frame.width
			self.debtGraph.frame.origin.x += self.view.frame.width
			self.billsGraph.frame.origin.x += self.view.frame.width
			self.budgetGraph.frame.origin.x += self.view.frame.width
			self.assetGraph.frame.origin.x += self.view.frame.width
			self.netGraph.frame.origin.x += self.view.frame.width
		} else if User.viewing == 3 {
			self.capitalGraph.frame.origin.y += self.view.frame.height
			self.debtGraph.frame.origin.y += self.view.frame.height
			self.billsGraph.frame.origin.y += self.view.frame.height
			self.budgetGraph.frame.origin.y += self.view.frame.height
			self.assetGraph.frame.origin.y += self.view.frame.height
			self.netGraph.frame.origin.y += self.view.frame.height
		} else if User.viewing == 4 {
			self.capitalGraph.frame.origin.y -= self.view.frame.height
			self.debtGraph.frame.origin.y -= self.view.frame.height
			self.billsGraph.frame.origin.y -= self.view.frame.height
			self.budgetGraph.frame.origin.y -= self.view.frame.height
			self.assetGraph.frame.origin.y -= self.view.frame.height
			self.netGraph.frame.origin.y -= self.view.frame.height
		} else if User.viewing == 5 {
			self.capitalGraph.frame.origin.x += self.view.frame.width
			self.debtGraph.frame.origin.x += self.view.frame.width
			self.billsGraph.frame.origin.x += self.view.frame.width
			self.budgetGraph.frame.origin.x += self.view.frame.width
			self.assetGraph.frame.origin.x += self.view.frame.width
			self.netGraph.frame.origin.x += self.view.frame.width
		}
	}
	
	func presentLast () {
		if User.viewing == 0 {
			self.nameLabel.frame.origin.x += 150
			self.currentCapitalLabel.frame.origin.x -= 150
			
			self.locationLabel.frame.origin.x += 800
			
			self.incomeButton.frame.origin.x -= 400
			self.transferButton.frame.origin.x += 400
			self.spendButton.frame.origin.y -= 400
		} else if User.viewing == 1 {
			self.nameLabel.frame.origin.x -= self.view.frame.width
			self.currentCapitalLabel.frame.origin.x -= self.view.frame.width
			
			self.locationLabel.frame.origin.x -= self.view.frame.width
			
			self.incomeButton.frame.origin.x -= self.view.frame.width
			self.transferButton.frame.origin.x -= self.view.frame.width
			self.spendButton.frame.origin.x -= self.view.frame.width
		} else if User.viewing == 2 {
			self.nameLabel.frame.origin.x += self.view.frame.width
			self.currentCapitalLabel.frame.origin.x += self.view.frame.width
			
			self.locationLabel.frame.origin.x += self.view.frame.width
			
			self.incomeButton.frame.origin.x += self.view.frame.width
			self.transferButton.frame.origin.x += self.view.frame.width
			self.spendButton.frame.origin.x += self.view.frame.width
		} else if User.viewing == 3 {
			self.nameLabel.frame.origin.x += 150
			self.currentCapitalLabel.frame.origin.x -= 150
			
			self.locationLabel.frame.origin.x += 800
			
			self.incomeButton.frame.origin.x -= 400
			self.transferButton.frame.origin.x += 400
			self.spendButton.frame.origin.y -= 400
		} else if User.viewing == 4 {
			self.nameLabel.frame.origin.x += 150
			self.currentCapitalLabel.frame.origin.x -= 150
			
			self.locationLabel.frame.origin.x += 800
			
			self.incomeButton.frame.origin.x -= 400
			self.transferButton.frame.origin.x += 400
			self.spendButton.frame.origin.y -= 400
		} else if User.viewing == 5 {
			self.nameLabel.frame.origin.x += self.view.frame.width
			self.currentCapitalLabel.frame.origin.x += self.view.frame.width
			
			self.locationLabel.frame.origin.x += self.view.frame.width
			
			self.incomeButton.frame.origin.x += self.view.frame.width
			self.transferButton.frame.origin.x += self.view.frame.width
			self.spendButton.frame.origin.x += self.view.frame.width
		}
	}
	
	func withdrawFirst () {
		if User.viewing == 0 {
			self.nameLabel.frame.origin.x -= 150
			self.currentCapitalLabel.frame.origin.x += 150
			
			self.locationLabel.frame.origin.x -= 800
			
			self.incomeButton.frame.origin.x += 400
			self.transferButton.frame.origin.x -= 400
			self.spendButton.frame.origin.y += 400
		} else if User.viewing == 1 {
			self.nameLabel.frame.origin.x += self.view.frame.width
			self.currentCapitalLabel.frame.origin.x += self.view.frame.width
			
			self.locationLabel.frame.origin.x += self.view.frame.width
			
			self.incomeButton.frame.origin.x += self.view.frame.width
			self.transferButton.frame.origin.x += self.view.frame.width
			self.spendButton.frame.origin.x += self.view.frame.width
		} else if User.viewing == 2 {
			self.nameLabel.frame.origin.x -= self.view.frame.width
			self.currentCapitalLabel.frame.origin.x -= self.view.frame.width
			
			self.locationLabel.frame.origin.x -= self.view.frame.width
			
			self.incomeButton.frame.origin.x -= self.view.frame.width
			self.transferButton.frame.origin.x -= self.view.frame.width
			self.spendButton.frame.origin.x -= self.view.frame.width
		} else if User.viewing == 3 {
			self.nameLabel.frame.origin.x -= 150
			self.currentCapitalLabel.frame.origin.x += 150
			
			self.locationLabel.frame.origin.x -= 800
			
			self.incomeButton.frame.origin.x += 400
			self.transferButton.frame.origin.x -= 400
			self.spendButton.frame.origin.y += 400
		} else if User.viewing == 4 {
			self.nameLabel.frame.origin.x -= 150
			self.currentCapitalLabel.frame.origin.x += 150
			
			self.locationLabel.frame.origin.x -= 800
			
			self.incomeButton.frame.origin.x += 400
			self.transferButton.frame.origin.x -= 400
			self.spendButton.frame.origin.y += 400
		} else if User.viewing == 5 {
			self.nameLabel.frame.origin.x -= self.view.frame.width
			self.currentCapitalLabel.frame.origin.x -= self.view.frame.width
			
			self.locationLabel.frame.origin.x -= self.view.frame.width
			
			self.incomeButton.frame.origin.x -= self.view.frame.width
			self.transferButton.frame.origin.x -= self.view.frame.width
			self.spendButton.frame.origin.x -= self.view.frame.width
		}
		
	}
	
	
	func dropFirst () {
		withdrawFirst()
	}
	
	func dropLast () {
		self.capitalGraph.frame.origin.y += self.view.frame.height
		self.debtGraph.frame.origin.y += self.view.frame.height
		self.billsGraph.frame.origin.y += self.view.frame.height
		self.budgetGraph.frame.origin.y += self.view.frame.height
		self.assetGraph.frame.origin.y += self.view.frame.height
		self.netGraph.frame.origin.y += self.view.frame.height
	}
	
	func raiseFirst () {
		withdrawFirst()
	}
	
	func raiseLast () {
		self.capitalGraph.frame.origin.y -= self.view.frame.height
		self.debtGraph.frame.origin.y -= self.view.frame.height
		self.billsGraph.frame.origin.y -= self.view.frame.height
		self.budgetGraph.frame.origin.y -= self.view.frame.height
		self.assetGraph.frame.origin.y -= self.view.frame.height
		self.netGraph.frame.origin.y -= self.view.frame.height
	}
	
	func pushLeftFirst () {
		withdrawFirst()
	}
	
	func pushLeftLast () {
		self.capitalGraph.frame.origin.x -= self.view.frame.width
		self.debtGraph.frame.origin.x -= self.view.frame.width
		self.billsGraph.frame.origin.x -= self.view.frame.width
		self.budgetGraph.frame.origin.x -= self.view.frame.width
		self.assetGraph.frame.origin.x -= self.view.frame.width
		self.netGraph.frame.origin.x -= self.view.frame.width
	}
	
	func pushRightFirst () {
		withdrawFirst()
	}
	
	func pushRightLast () {
		self.capitalGraph.frame.origin.x += self.view.frame.width
		self.debtGraph.frame.origin.x += self.view.frame.width
		self.billsGraph.frame.origin.x += self.view.frame.width
		self.budgetGraph.frame.origin.x += self.view.frame.width
		self.assetGraph.frame.origin.x += self.view.frame.width
		self.netGraph.frame.origin.x += self.view.frame.width
	}
	
	func rotateOut () {
		//UIView.animate(withDuration: 0.3, animations: { () -> Void in
			//self.dropFirst()
			
		//}) { (Finished) -> Void in
			
		//}
		
		//UIView.animate(withDuration: 0.5, animations: { () -> Void in
			//self.dropLast()
			
		//}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "root2sheet", sender: self)
		//}
		
	}
	
	func animIn () {
		self.locationLabel.isHidden = true
		
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.presentFirst()
			
		}) { (Finished) -> Void in
		}
		
		UIView.animate(withDuration: 1.0, animations: { () -> Void in
			self.presentLast()
			
		}) { (Finished) -> Void in
			self.view.layoutIfNeeded()
			self.locationLabel.isHidden = false
		}
		
		
	}
	
	func animUp () {
		User.viewing = 3
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.raiseFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.raiseLast()
			
		}) { (Finished) -> Void in
			GroupData.set("newTranConfig", data: "0")
			self.performSegue(withIdentifier: "root2addUp", sender: self)
		}
	}
	
	func animDown () {
		User.viewing = 4
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.dropFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.dropLast()
			
		}) { (Finished) -> Void in
			GroupData.set("newTranConfig", data: "2")
			self.performSegue(withIdentifier: "root2addDown", sender: self)
		}
	}
	
	func goToBank() {
		User.viewing = 5
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "root2bank", sender: self)
		}
	}
	
	func goToDebt() {
		User.viewing = 5
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "root2debt", sender: self)
		}
	}
	
	func goToBill() {
		User.viewing = 5
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "root2bill", sender: self)
		}
	}
	
	func goToBudget() {
		User.viewing = 5
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "root2budget", sender: self)
		}
	}
	
	func goToAsset() {
		User.viewing = 5
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "root2asset", sender: self)
		}
	}
	
	func goToBudgeting(_ sender: Any) {
		User.viewing = 2
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.pushLeftFirst()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "root2budgeting", sender: self)
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			
			self.pushLeftLast()
			
		}) { (Finished) -> Void in
			
		}
	}
	
	func goToBilling(_ sender: Any) {
		User.viewing = 1
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.pushRightFirst()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "root2billing", sender: self)
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			
			self.pushRightLast()
			
		}) { (Finished) -> Void in
			
		}
	}
	
}


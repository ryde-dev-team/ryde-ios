//
//  NewTransactionView.swift
//  Ryde Money
//
//  Created by Ryan Dean on 10/26/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class NewTransactionView: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate, UITextFieldDelegate {
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		
		NotificationCenter.default.addObserver(self, selector: #selector(NewTransactionView.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(NewTransactionView.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
	}
	
	@IBOutlet var doneButton: UIButton!
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var merchantField: UITextField!
    @IBOutlet var accountSelect: UIPickerView!
	@IBOutlet var categorySelect: UIPickerView!
	@IBOutlet var dateSelect: UIDatePicker!
	@IBOutlet var amountField: UITextField!
	@IBOutlet var selectMerchant: UIButton!
	
    @IBOutlet var swipeRight: UISwipeGestureRecognizer!
	
	@IBOutlet var swipeDown: UISwipeGestureRecognizer!
	
	@IBOutlet var swipeUp: UISwipeGestureRecognizer!
	
	var automerchants = [String]()
	var pastmerchants = [String]()
	
	var currentAmount: String = ""
	
	var config = 0 //0 - Spend, 1 - Transfer, 2 - Income
	
	var firstIn = true
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if firstIn {
			animIn()
			firstIn = false
		} else {
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		categorySelect.delegate = self
		categorySelect.dataSource = self
        
        accountSelect.delegate = self
        accountSelect.dataSource = self
		
		self.merchantField.delegate = self;
		self.amountField.delegate = self;
		
		merchantField.addTarget(self, action: #selector(merchantTyped(_:)), for: UIControlEvents.editingChanged)
		
		amountField.addTarget(self, action: #selector(amountTyped(_:)), for: UIControlEvents.editingChanged)
		
		selectMerchant.setTitle("", for: UIControlState.normal)
		
		pastmerchants = UserDefaults.init(suiteName: "group.ryde-money.test")!.stringArray(forKey: "pastmerchants") ?? [String]()
		
		
        
        swipeRight.addTarget(self, action: #selector(cancelTransaction(_:)))
        self.view.addGestureRecognizer(swipeRight)
        self.view.isUserInteractionEnabled = true
		
		swipeUp.addTarget(self, action: #selector(cancelTransaction(_:)))
		self.view.addGestureRecognizer(swipeUp)
		self.view.isUserInteractionEnabled = true
		
		swipeDown.addTarget(self, action: #selector(cancelTransaction(_:)))
		self.view.addGestureRecognizer(swipeDown)
		self.view.isUserInteractionEnabled = true
		
		switch (GroupData.get("newTranConfig") as! String) {
			case "0":
				print("Spending")
				config = 0
				self.view.backgroundColor = Color.nBack
			case "1":
				print("Transfer")
				config = 1
				self.view.backgroundColor = Color.pBack
			case "2":
				print("Income")
				config = 2
				self.view.backgroundColor = Color.pBack
			case "3":
				print("Debt")
				config = 3
				self.view.backgroundColor = Color.nBack
			case "5":
				print("Pay Debt")
				config = 5
				self.view.backgroundColor = Color.pBack
			case "6":
				print("Asset Loss")
				config = 6
				self.view.backgroundColor = Color.nBack
			default:
				print("INVALID NEW TRANSACTION CONFIG")
		}
		
		

	}
	
	func merchantTyped(_ textField: UITextField) {
		searchAutocompleteEntriesWithSubstring(substring: merchantField.text!)
		if automerchants.count > 0 {
			selectMerchant.setTitle(automerchants[0], for: UIControlState.normal)
		} else {
			selectMerchant.setTitle("", for: UIControlState.normal)
		}
		
		
	}
	
	@IBAction func autoCompleteMerchant(_ sender: AnyObject) {
		selectMerchant.setTitle("", for: UIControlState.normal)
		merchantField.text = selectMerchant.titleLabel?.text
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	func numberOfComponents(in: UIPickerView) -> Int {
		return 1
	}
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.isEqual(categorySelect) {
			
			switch (config) {
				case 0:
					return User.budgets.count + User.goals.count
				case 1:
					return User.banks.count + User.debts.count + User.cards.count
				case 2:
					return 0
				case 3:
					return 0
				case 5:
					return User.debts.count + User.cards.count
				case 6:
					return 0
				default:
					return -1
			}
        } else {
			switch (config) {
			case 0:
				return User.banks.count + User.cards.count
			case 1:
				return User.banks.count + User.debts.count + User.cards.count
			case 2:
				return User.banks.count + User.assets.count
			case 3:
				return User.debts.count + User.cards.count
			case 5:
				return User.banks.count
			case 6:
				return User.assets.count
			default:
				return -1
			}
        }
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.isEqual(categorySelect) {
			switch (config) {
			case 0:
				if row < User.budgets.count {
					return User.budgets[row].name
				} else if row - User.budgets.count < User.goals.count {
					return User.goals[row - User.budgets.count].name
				}
			case 1:
				if row < User.banks.count {
					return User.banks[row].name
				} else if row - User.banks.count < User.debts.count {
					return User.debts[row - User.banks.count].name
				} else if row - User.banks.count - User.debts.count < User.cards.count {
					return User.cards[row - User.banks.count - User.debts.count].name
				}
			case 2:
				return ""
			case 3:
				return ""
			case 5:
				if row < User.debts.count {
					return User.debts[row].name
				} else if row - User.debts.count < User.cards.count {
					return User.cards[row - User.debts.count].name
				}
			case 6:
				return ""
			default:
				return "!INVALID ROW!"
			}
        } else {
			switch (config) {
			case 0:
				if row < User.banks.count {
					return User.banks[row].name
				} else if row - User.banks.count < User.cards.count {
					return User.cards[row - User.banks.count].name
				}
			case 1:
				if row < User.banks.count {
					return User.banks[row].name
				} else if row - User.banks.count < User.debts.count {
					return User.debts[row - User.banks.count].name
				} else if row - User.banks.count - User.debts.count < User.cards.count {
					return User.cards[row - User.banks.count - User.debts.count].name
				}
			case 2:
				if row < User.banks.count {
					return User.banks[row].name
				} else if row - User.banks.count < User.assets.count {
					return User.assets[row - User.banks.count].name
				}
			case 3:
				return (User.debts + User.cards)[row].name
			case 5:
				return User.banks[row].name
			case 6:
				return User.assets[row].name
			default:
				return "!INVALID ROW!"
			}
        }
		return "!INVALID ROW!"
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		amountField.text = Currency.remove(amountField.text!)!.description
		if amountField.isEditing {
			switch (config) {
			case 0:
				if Float(amountField.text!)! > 0 {
					amountField.text = (Float(amountField.text!)! * -1).description
				}
				break
			case 3:
				if Float(amountField.text!)! > 0 {
					amountField.text = (Float(amountField.text!)! * -1).description
				}
				break
			case 6:
				if Float(amountField.text!)! > 0 {
					amountField.text = (Float(amountField.text!)! * -1).description
				}
				break
			default:
				if Float(amountField.text!)! < 0 {
					amountField.text = (Float(amountField.text!)! * -1).description
				}
				break
			}
		}
		amountField.text = Currency.format(amountField.text!)
		
		if Double(Currency.remove(amountField.text!)!) < 0 {
			amountField.textColor = UIColor.red
		} else {
			amountField.textColor = UIColor.green
		}
		self.view.endEditing(true)
		return false
	}
	
	@IBAction func recordTransaction(_ sender: AnyObject) {
		print(merchantField.text!)
        print(accountSelect.selectedRow(inComponent: 0))
		print(categorySelect.selectedRow(inComponent: 0))
		print(dateSelect.date)
		print(Currency.format(amountField.text!)!)
		
		let val = Double(Currency.remove(amountField.text!)!)
		
		print(val)
		
		if val == Double(0) {
			cancelTransaction(sender)
			return
		}
		
		let tran:Transaction = Transaction(time: dateSelect.date.timeIntervalSince1970, value: val, memo: merchantField.text!, cleared: 0, reconciled: 0)
		
		tran.setLocation(lat: 0.0, long: 0.0)
		
		let selected = accountSelect.selectedRow(inComponent: 0)
		let cat = categorySelect.selectedRow(inComponent: 0)
		var accountId = 0
		var categoryId = 0
		
		switch (config) {
		case 0:
			if selected < User.banks.count {
				accountId = User.banks[selected].id
			} else {
				accountId = User.cards[selected - User.banks.count].id
			}
			
			if cat < User.budgets.count {
				categoryId = User.budgets[cat].id
			} else if cat - User.budgets.count < User.goals.count {
				categoryId = User.goals[cat - User.budgets.count].id
			}
		case 1:
			if selected < User.banks.count {
				accountId = User.banks[selected].id
			} else if selected - User.banks.count < User.debts.count {
				accountId = User.debts[selected - User.banks.count].id
			} else if selected - User.banks.count - User.debts.count < User.cards.count {
				accountId = User.debts[selected - User.banks.count - User.debts.count].id
			}
			
			if cat < User.banks.count {
				categoryId = User.banks[cat].id
			} else if cat - User.banks.count < User.debts.count {
				categoryId = User.debts[cat - User.banks.count].id
			} else if cat - User.banks.count - User.debts.count < User.cards.count {
				categoryId = User.cards[cat - User.banks.count - User.debts.count].id
			}
		case 2:
			if selected < User.banks.count {
				accountId = User.banks[selected].id
			} else if selected - User.banks.count < User.assets.count {
				accountId = User.assets[selected - User.banks.count].id
			}
			
			categoryId = -1
		case 3:
			accountId = (User.debts + User.cards)[selected].id
			categoryId = -1
		case 5:
			accountId = User.banks[selected].id
			if cat < User.debts.count {
				categoryId = User.debts[cat].id
			} else if cat - User.debts.count < User.cards.count {
				categoryId = User.cards[cat - User.debts.count].id
			}
		case 6:
			accountId = User.assets[selected].id
			categoryId = -1
		default:
			accountId = -1
			categoryId = -1
		}
		
		User.record(tran: tran, accountId: accountId, categoryId: categoryId, config: config)
		
		
		if !pastmerchants.contains(merchantField.text!) {
			pastmerchants.append(merchantField.text!)
			UserDefaults.init(suiteName: "group.ryde-money.test")!.set(pastmerchants, forKey: "pastmerchants")
		}
		
	
		self.dismiss(animated: true, completion: nil);
	}
	
	func searchAutocompleteEntriesWithSubstring(substring: String)
	{
		automerchants.removeAll(keepingCapacity: false)
		
		for curString in pastmerchants
		{
			let myString:NSString! = curString as NSString
			
			let substringRange :NSRange! = myString.range(of: substring)
			
			if (substringRange.location  == 0)
			{
				automerchants.append(curString)
			}
		}
		
	}
	
	func amountTyped(_ textField: UITextField){
		if amountField.text == nil || Float(amountField.text!) == nil {
			return
		}
		
		
		if Float(amountField.text!)! < 0 {
			amountField.textColor = UIColor.red
		} else {
			amountField.textColor = UIColor.green
		}
		
		//amountField.text = formatCurrency(string: amountField.text!)
	}
	
	func keyboardWillShow(notification: NSNotification) {
		if merchantField.isEditing {
			return
		}
		
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y == 0{
				self.view.frame.origin.y -= keyboardSize.height
			}
			else {
				
			}
		}
		
	}
	
	func keyboardWillHide(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y != 0 {
				self.view.frame.origin.y += keyboardSize.height
			}
			else {
				
			}
		}
	}
	
	func raiseInFirst () {
		self.accountSelect.frame.origin.y -= self.view.frame.height
		self.categorySelect.frame.origin.y -= self.view.frame.height * 1.1
		self.dateSelect.frame.origin.y -= self.view.frame.height * 1.2
		self.selectMerchant.frame.origin.y -= self.view.frame.height
	}
	
	func raiseInLast () {
		self.merchantField.frame.origin.y += self.view.frame.height / 3
		self.titleLabel.frame.origin.y += self.view.frame.height / 3
		
		self.doneButton.frame.origin.y -= self.view.frame.height * 1.5
		self.amountField.frame.origin.y -= self.view.frame.height
	}
	
	func dropInFirst () {
		self.accountSelect.frame.origin.y += self.view.frame.height * 1.2
		self.categorySelect.frame.origin.y += self.view.frame.height * 1.1
		self.dateSelect.frame.origin.y += self.view.frame.height
		self.selectMerchant.frame.origin.y += self.view.frame.height
	}
	
	func dropInLast () {
		self.merchantField.frame.origin.y += self.view.frame.height / 3
		self.titleLabel.frame.origin.y += self.view.frame.height / 3
		
		self.doneButton.frame.origin.y -= self.view.frame.height * 1.5
		self.amountField.frame.origin.y -= self.view.frame.height
	}
	
	func raiseOutFirst () {
		self.merchantField.frame.origin.y -= self.view.frame.height
		self.amountField.frame.origin.y += self.view.frame.height
		self.doneButton.frame.origin.y += self.view.frame.height * 1.3
		self.titleLabel.frame.origin.y -= self.view.frame.height
	}
	
	func raiseOutLast () {
		self.accountSelect.frame.origin.y -= self.view.frame.height * 1.2
		self.categorySelect.frame.origin.y -= self.view.frame.height * 1.4
		self.dateSelect.frame.origin.y -= self.view.frame.height * 1.5
		self.selectMerchant.frame.origin.y -= self.view.frame.height
	}
	
	func dropOutFirst () {
		self.merchantField.frame.origin.y -= self.view.frame.height
		self.amountField.frame.origin.y += self.view.frame.height
		self.doneButton.frame.origin.y += self.view.frame.height * 1.3
		self.titleLabel.frame.origin.y -= self.view.frame.height
	}
	
	func dropOutLast () {
		self.accountSelect.frame.origin.y += self.view.frame.height * 1.4
		self.categorySelect.frame.origin.y += self.view.frame.height * 1.2
		self.dateSelect.frame.origin.y += self.view.frame.height
		self.selectMerchant.frame.origin.y += self.view.frame.height * 1.5
	}
	
	
	
	func cancelTransaction(_ sender: AnyObject) {
		
		switch (config) {
		case 2:
			UIView.animate(withDuration: 0.5, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.raiseOutFirst()
				
			}) { (Finished) -> Void in
				self.dismiss(animated: false, completion: nil)
			}
			
			UIView.animate(withDuration: 0.7, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.raiseOutLast()
				
			}) { (Finished) -> Void in
				self.dismiss(animated: false, completion: nil)
			}
			break
		case 0:
			UIView.animate(withDuration: 0.5, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.dropOutFirst()
				
			}) { (Finished) -> Void in
				self.dismiss(animated: false, completion: nil)
			}
			
			UIView.animate(withDuration: 0.7, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.dropOutLast()
				
			}) { (Finished) -> Void in
				self.dismiss(animated: false, completion: nil)
			}
			break
		default:
			UIView.animate(withDuration: 0.5, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.dropOutFirst()
				
			}) { (Finished) -> Void in
				self.dismiss(animated: false, completion: nil)
			}
			
			UIView.animate(withDuration: 0.7, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.dropOutLast()
				
			}) { (Finished) -> Void in
				self.dismiss(animated: false, completion: nil)
			}
			break
		}
		
		
		
		
	}
	
	func animIn () {
		switch (config) {
		case 0:
			UIView.animate(withDuration: 0.5, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.raiseInFirst()
				
			}) { (Finished) -> Void in
				
			}
			
			UIView.animate(withDuration: 0.7, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.raiseInLast()
				
			}) { (Finished) -> Void in
				
			}
			break
		case 2:
			UIView.animate(withDuration: 0.5, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.dropInFirst()
				
			}) { (Finished) -> Void in
				
			}
			
			UIView.animate(withDuration:0.7, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.dropInLast()
				
			}) { (Finished) -> Void in
				
			}
			break
		default:
			UIView.animate(withDuration: 0.5, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.raiseInFirst()
				
			}) { (Finished) -> Void in
				
			}
			
			UIView.animate(withDuration: 0.7, animations: { () -> Void in
				//self.view.frame = self.view.frame.offsetBy(dx: 0.0, dy: self.view.frame.height)
				
				self.raiseInLast()
				
			}) { (Finished) -> Void in
				
			}
			break
		}
		
		
	}
	
	
}

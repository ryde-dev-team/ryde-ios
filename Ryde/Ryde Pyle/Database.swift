//
//  Database.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 1/13/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import Foundation
import CryptoSwift

extension String {
	
	func customEncrypt (key: String) throws -> String {
		let data = self.data(using: String.Encoding.utf8)
		let enc = try data!.encrypt(cipher: ChaCha20(key: key, iv: Database.appKey))
		return enc.base64EncodedString()
	}
	
	func customDecrypt (key: String) throws -> String {
		let data = Data(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0))
		let dec = try data!.decrypt(cipher: ChaCha20(key: key, iv: Database.appKey))
		return String(data: dec, encoding: String.Encoding.utf8)!
	}
	
}

class Database {
	static var appKey: String = "bqif74#k"
	
	static var URL_sessionRequest = "http://mdm.tcschools.com/scripts/ryde-pyle/requestSession.php"
	static var URL_sessionVerify = 	"http://mdm.tcschools.com/scripts/ryde-pyle/verifySession.php"
	static var URL_sessionRemove = 	"http://mdm.tcschools.com/scripts/ryde-pyle/removeSession.php"
	static var URL_user = 			"http://mdm.tcschools.com/scripts/ryde-pyle/user.php"
	static var URL_account = "http://mdm.tcschools.com/scripts/ryde-pyle/account.php"
	static var URL_accountByType = 	"http://mdm.tcschools.com/scripts/ryde-pyle/accountByType.php"
	static var URL_transactions = "http://mdm.tcschools.com/scripts/ryde-pyle/transactions.php"
	static var URL_transactionsByAccount = "http://mdm.tcschools.com/scripts/ryde-pyle/transactionByAccount.php"
	static var URL_bills = "http://mdm.tcschools.com/scripts/ryde-pyle/bills.php"
	static var URL_systemVerify = "http://mdm.tcschools.com/scripts/ryde-pyle/verifySystem.php"
	static var URL_feedback = "http://mdm.tcschools.com/scripts/ryde-pyle/feedback.php"
	static var URL_place = "http://mdm.tcschools.com/scripts/ryde/place.php"
	
	
	static func debug () {
		URL_systemVerify = "http://mdm.tcschools.com/scripts/ryde/verifySystem.php"
	}
	
	static func encrypt(_ string: String, key: String) -> String?{
		var r: String!
		do {
			r = try string.customEncrypt(key: key)
		} catch {
			r = "[ENCRYPTION ERROR]"
		}
		return r
	}
	
	static func decrypt(_ string: String, key: String) -> String?{
		var r: String!
		do {
			r = try string.customDecrypt(key: key)
		} catch {
			r = "[ENCRYPTION ERROR]"
		}
		return r
	}
	
	static func generateKey() -> String {
		let length = 32
		let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		let len = UInt32(letters.length)
		
		var randomString = ""
		
		for _ in 0 ..< length {
			let rand = arc4random_uniform(len)
			var nextChar = letters.character(at: Int(rand))
			randomString += NSString(characters: &nextChar, length: 1) as String
		}
		
		return randomString
	}
	
	static func verifySesssion() -> Int{
		print("Checking for key cache")
		if GroupData.get("sessionKey") == nil {
			print("No key cache found")
			return -1
		}
		
		let unverifiedKey = GroupData.get("sessionKey") as! String
		
		let phpRequest = URL_sessionVerify + "?key=" + unverifiedKey
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if r["response"] == nil || r["rydeid"] == nil {
				return -1
			}
			
			let response = r["response"]!
			let rydeid = Int(r["rydeid"]!)!
			
			if response != "VERIFIED" {
				return -1
			}
			
			User.rydeid = rydeid
			User.sessionKey = unverifiedKey
			
			Ecosystem.logLocal("Session Verified")
			return rydeid
		}
		return -1
	}
	
	static func userInfo(_ id: Int) -> [String]? {
		if id < 0 {
			return nil
		}
		
		
		let phpRequest = URL_user + "?id=" + id.description
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if r["name"] == nil || r["authority"] == nil {
				return nil
			}
			
			let name = r["name"]!
			let authority = r["authority"]!
			
			
			return [name,authority]
		}
		
		return nil
	}
	
	static func login(_ user: String, pass: String) -> [String]? {
		let loginKey = generateKey()
		var passCrypt = encrypt(pass, key: loginKey)
		
		if passCrypt == nil {
			return nil
		}
		
		while passCrypt!.contains("+") {
			passCrypt = encrypt(pass, key: loginKey)
		}
		
		Ecosystem.logLocal(user + ":" + passCrypt!)
		
		let phpRequest = URL_sessionRequest + "?key=" + loginKey + "&hash=" + passCrypt! + "&email=" + user.lowercased()
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if r["response"] == nil || r["rydeid"] == nil {
				return nil
			}
			
			let response = r["response"]
			let id = r["rydeid"]
			
			if response == "BAD HASH" {
				return nil
			}
			
			Ecosystem.logLocal("Session Key: " + response!)
			GroupData.set("sessionKey", data: response!)
			User.sessionKey = response!
			return [response!,id!]
		}
		
		return nil
	}
	
	static func logout(_ key: String) {
		GroupData.clear("sessionKey")
		
		Ecosystem.logLocal("Remote Logout")
		let phpRequest = URL_sessionRemove + "?key=" + key
		_ = Network.dataOfJson(url: phpRequest)
	}
	
	
	
	static func systemVerify() -> [String]? {
		var access: Int!
		var motd: String!
		
		if Network.dataOfJson(url: URL_systemVerify + "?build=default") != nil {
			for r in Network.dataOfJson(url: URL_systemVerify + "?build=default")! {
				if r["access"] != nil {
					access = Int(r["access"]!)!
				}
				if r["motd"] != nil {
					motd = r["motd"]!
				}
			}
		}
		
		if access == nil {
			if Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build()) != nil {
				for r in Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build())! {
					if r["access"] != nil {
						access = Int(r["access"]!)!
					}
					if r["motd"] != nil {
						motd = r["motd"]!
					}
				}
			}
		} else {
			if Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build()) != nil {
				for r in Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build())! {
					if r["access"] != nil {
						if access != nil {
							if access > Int(r["access"]!)! {
								access = Int(r["access"]!)!
							}
						}
					}
				}
			} else {
				motd = "This version is no longer valid, please update."
				access = nil
			}
		}
		
		
		if motd! == "" {
			if Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build()) != nil {
				for r in Network.dataOfJson(url: URL_systemVerify + "?build=" + System.build())! {
					if r["motd"] != nil {
						motd = r["motd"]!
					}
				}
			} else {
				motd = ""
			}
		}
		
		return [(access ?? -1).description,motd]
	}
	
	static func getNextFeedbackId() -> Int{
		var highest = 0
		
		for t in Network.dataOfJson(url: URL_feedback)! {
			if Int(t["id"]!)! >= highest {
				highest = Int(t["id"]!)! + 1
			}
		}
		
		return highest
	}
	
	static func sendFeedback(_ text: String) {
		Ecosystem.logLocal("Feedback Sent")
		
		let id = getNextFeedbackId()
		let phpRequest = URL_feedback + "?action=insert"
		let phpData = "id="+id.description+"&rydeid="+User.rydeid.description+"&epoch="+Date().timeIntervalSince1970.description+"&message="+text
		Network.sendJson(url: phpRequest, data: phpData)
	}
	
	static func getNextPlaceId() -> Int{
		var highest = 0
		
		for t in Network.dataOfJson(url: URL_place)! {
			if Int(t["id"]!)! >= highest {
				highest = Int(t["id"]!)! + 1
			}
		}
		
		return highest
	}
	
	
	
	
	static func savePlace(_ plc: Place) {
		let phpRequest = URL_place + "?rydeid=" + User.rydeid.description
		let phpUpdate = URL_place + "?action=update"
		let phpInsert = URL_place + "?action=insert"
		
		let phpData = "id="+plc.id.description+"&name="+plc.name+"&lat="+plc.lat.description+"&long="+plc.long.description+"&radius="+plc.radius.description+"&rydeid="+User.rydeid.description
		
		for r in Network.dataOfJson(url: phpRequest)! {
			if Int(r["id"]!)! == plc.id {
				Network.sendJson(url: phpUpdate, data: phpData)
				return
			}
		}
		
		Network.sendJson(url: phpInsert, data: phpData)
	}
	
	static func places() -> [Place]{
		let phpRequest = URL_place + "?rydeid=" + User.rydeid.description
		
		var plcs: [Place] = []
		
		for r in Network.dataOfJson(url: phpRequest)! {
			let plc = Place(Int(r["id"]!)!,r["name"]!,lat:Double(r["lat"]!)!,long:Double(r["long"]!)!,radius:Double(r["radius"]!)!)
			
			print("//////// - " + plc.name)
			
			plcs.append(plc)
		}
		
		return plcs
	}
}

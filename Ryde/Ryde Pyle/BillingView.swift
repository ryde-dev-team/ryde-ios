//
//  BillingView.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/29/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class BillingView: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
	
	@IBOutlet var swipeRight: UISwipeGestureRecognizer!
	
	@IBOutlet var billList: UITableView!
	@IBOutlet var billLeft: UILabel!
	@IBOutlet var titleLabel: UILabel!
	
	@IBOutlet var listTop: NSLayoutConstraint!
	
	static var dismissTo = 1
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		NotificationCenter.default.addObserver(self, selector: #selector(BillingView.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BillingView.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
		
		Timer.scheduledTimer(timeInterval: 1.0,
		                     target: self,
		                     selector: #selector(self.update),
		                     userInfo: nil,
		                     repeats: true)
	}
	
	var firstIn = true
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if firstIn {
			animIn()
			firstIn = false
		} else {
			
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		swipeRight.addTarget(self, action: #selector(backToBill(_:)))
		self.view.addGestureRecognizer(swipeRight)
		self.view.isUserInteractionEnabled = true
		
		billList.delegate = self
		billList.dataSource = self
		
		User.currentBills.sort {$0.due < $1.due}
		
		update()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	func backToBill(_ sender: AnyObject) {
		LoadingController.setNav(BillingView.dismissTo)
		animOut()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return User.currentBills.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "BillingCell", for: indexPath as IndexPath) as! BillingCell
		
		let bill = User.currentBills[indexPath.row]
		
		let accounts: [Account] = User.banks + User.debts + User.assets + User.cards + User.bills + User.goals + User.budgets
		
		var tran: Transaction!
		
		for a in accounts {
			for t in a.transactions {
				if t.id == bill.tranId {
					tran = t
					break
				}
			}
			if tran != nil {
				break
			}
		}
		
		if tran != nil {
			cell.name.text = tran.memo
			cell.amount.text = Currency.format(tran.value.description)
			
			cell.amount.delegate = self
			
			cell.update()
			
			var per: Float = Float((Date().timeIntervalSince1970 - tran.timestamp) / (bill.due - tran.timestamp))
			
			
			if per > 1.0 {
				per = 1.0
			}
			
			cell.amount.textColor = UIColor(colorLiteralRed: 1.0 - (1.0 * per), green: 25/255, blue: 25/255, alpha: 1.0)
			cell.back.backgroundColor = UIColor(colorLiteralRed: 1.0 * per, green: 1.0 - (1.0 * per), blue: 0/255.0, alpha: 1.0)
			
			
			
			if per < 0.0 {
				per = -per
				if per > 1.0 {
					per = 1.0
				}
				cell.amount.textColor = UIColor(colorLiteralRed: 255/255, green: 1.0 - (1.0 * per), blue: 1.0 - (1.0 * per), alpha: 1.0)
				cell.name.textColor = UIColor(colorLiteralRed: 1.0 - (1.0 * per), green: 1.0 - (1.0 * per), blue: 1.0 - (1.0 * per), alpha: 1.0)
				cell.back.backgroundColor = UIColor(colorLiteralRed: 1.0 * per, green: 0.0, blue: 0/255.0, alpha: 1.0)
			}
			
			if tran.hidden {
				cell.isHidden = true
			}
		}
			
		
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		_ = tableView.cellForRow(at: indexPath) as! BillingCell
		
		GroupData.set("billSelected", data: Int(indexPath.row))
		
		LoadingController.setNav(12)
		
		self.dismiss(animated: false, completion: nil)
		
		print(indexPath.description)
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.endEditing(true)
		self.view.endEditing(true)
		return false
	}
	
	func keyboardWillShow(notification: NSNotification) {
		
		
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y == 0{
				self.view.frame.origin.y -= keyboardSize.height
				listTop.constant += 1
			}
			else {
				
			}
			
			
		}
		
	}
	
	func keyboardWillHide(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if view.frame.origin.y != 0 {
				self.view.frame.origin.y += keyboardSize.height
				listTop.constant -= 1
			}
			else {
				
			}
		}
	}
	
	func update() {
		
		
		billLeft.text = Currency.format(User.usable.description)
		
		if User.usable < 0 {
			billLeft.textColor = Color.nText
			self.view.backgroundColor = Color.nBack
			billList.backgroundColor = Color.nBack
		} else {
			billLeft.textColor = Color.pText
			self.view.backgroundColor = Color.pBack
			billList.backgroundColor = Color.pBack
		}
	}
	
	func animIn () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.inFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			self.inLast()
		}) { (Finished) -> Void in
			
		}
	}
	
	func animOut () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outLast()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			self.outFirst()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		
	}
	
	func inFirst () {
		self.billList.frame.origin.x += self.view.frame.width
	}
	
	func inLast () {
		self.billLeft.frame.origin.x += self.view.frame.width
		self.titleLabel.frame.origin.x += self.view.frame.width
	}
	
	func outFirst () {
		self.billLeft.frame.origin.x -= self.view.frame.width
		self.titleLabel.frame.origin.x -= self.view.frame.width
	}
	
	func outLast () {
		self.billList.frame.origin.x -= self.view.frame.width
	}
}

//
//  DashView.swift
//  Ryde
//
//  Created by Ryan Dean on 1/17/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit
import MapKit

class DashController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {//, UITableViewDataSource, UITableViewDelegate {
	
	deinit {
		updateTimer.invalidate()
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		
		updateTimer = Timer.scheduledTimer(timeInterval: 0.5,
		                                   target: self,
		                                   selector: #selector(self.update),
		                                   userInfo: nil,
		                                   repeats: true)
		
	}
	
	var updateTimer: Timer!
	
	@IBOutlet var nameLabel: UILabel!
	@IBOutlet var locationLabel: UILabel!
	
	@IBOutlet var map: MKMapView!
	@IBOutlet var accountHistoryList: UITableView!
	
	@IBOutlet var subscriptionButton: PlanView!
	@IBOutlet var pyleButton: UIButton!
	let locationManager = CLLocationManager()
	
	var lastLoc: [Float] = []
	
	@IBOutlet var placeNameLabel: UIButton!
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		self.locationManager.requestAlwaysAuthorization()
		
		nameLabel.text = User.name
		
		
		map.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
		map.layer.cornerRadius = 15
		map.layer.shadowOpacity = 0.7
		map.layer.shadowRadius = 3
		map.layer.shadowColor = UIColor.black.cgColor
		map.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
		map.delegate = self
		
		
		
		
		if CLLocationManager.locationServicesEnabled() {
			locationManager.delegate = self
			locationManager.desiredAccuracy = kCLLocationAccuracyBest
			locationManager.startUpdatingLocation()
		}
		
		pyleButton.imageView!.layer.cornerRadius = 15
		pyleButton.layer.cornerRadius = 15
		pyleButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
		pyleButton.layer.shadowColor = UIColor.black.cgColor
		pyleButton.layer.shadowRadius = 3
		pyleButton.layer.shadowOpacity = 0.7
		
		Ecosystem.logLocal("Dash Loaded")
		
		subscriptionButton.view.layer.cornerRadius = 15
		subscriptionButton.backgroundColor = UIColor.clear
		subscriptionButton.view.layer.shadowOpacity = 0.7
		subscriptionButton.view.layer.shadowRadius = 3
		subscriptionButton.view.layer.shadowColor = UIColor.black.cgColor
		subscriptionButton.view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
		
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		for p in User.places {
			self.pinPlace(p)
		}
	}
	
	func update () {
		accountHistoryList.reloadData()
		accountHistoryList.setNeedsDisplay()
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		let locValue:CLLocationCoordinate2D = manager.location!.coordinate
		
		lastLoc = User.currentLocation
		
		User.currentLocation = [Float(locValue.latitude),Float(locValue.longitude)]
		
		
		var movementState: User.MovementStates = .still
		
		if lastLoc.count > 0 {
			let diflat = User.currentLocation[0] - lastLoc[0]
			let diflon = User.currentLocation[1] - lastLoc[1]
			
			if abs(diflat) > 0.000005 || abs(diflon) > 0.000005 {
				movementState = .walking
			}
			
			if abs(diflat) > 0.00003 || abs(diflon) > 0.00003 {
				movementState = .running
			}
			
			if abs(diflat) > 0.00007 || abs(diflon) > 0.00007 {
				movementState = .driving
			}
		}
		
		locationLabel.text = "LAT: " + User.currentLocation[0].description + " LONG: " + User.currentLocation[1].description
		
		if movementState != User.lastMovementState {
			var stateString = ""
			let currentInterval = Date().timeIntervalSince1970
			let diff: Double = (currentInterval - User.stateChangeEpoch)
			
			switch User.lastMovementState {
			case .walking:
				stateString = "Walked from "
			case .running:
				stateString = "Ran from "
			case .driving:
				stateString = "Drove from "
			default:
				stateString = "Still at "
			}
			
			Ecosystem.logLocal(String(format: stateString + " " + locationLabel.text! + " for %.2f seconds.", Float(diff)))
			User.lastMovementState = movementState
			User.stateChangeEpoch = currentInterval
		}
		
		
		
		switch movementState {
		case .walking:
			locationLabel.text!.append(" /// Walking")
		case .running:
			locationLabel.text!.append(" /// Running")
		case .driving:
			locationLabel.text!.append(" /// Driving")
		default:
			locationLabel.text!.append(" /// Still")
		}
		
		
		
		if movementState == .walking || movementState == .still {
			var currentPlace: Place!
			
			for p in User.places {
				let area: CLLocation  = CLLocation(latitude: CLLocationDegrees(p.lat), longitude: CLLocationDegrees(p.long))
				let current: CLLocation = CLLocation(latitude: CLLocationDegrees(User.currentLocation[0]), longitude: CLLocationDegrees(User.currentLocation[1]))
				
				let dist: CLLocationDistance = area.distance(from: current)
				
				if dist < p.radius {
					currentPlace = p
					break
				}
			}
			
			if currentPlace != nil {
				locationLabel.text!.append(" /// " + currentPlace!.name)
				placeNameLabel.setTitle(currentPlace!.name, for: .normal)
				placeNameLabel.isEnabled = false
			} else {
				locationLabel.text!.append(" /// Unknown")
				placeNameLabel.setTitle("Unknown", for: .normal)
				placeNameLabel.isEnabled = true
			}
		}
		
		
		let latDelta: CLLocationDegrees = 0.0005
		let lonDelta: CLLocationDegrees = 0.0005
		let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
		let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(locValue.latitude, locValue.longitude)
		let region: MKCoordinateRegion = MKCoordinateRegionMake(location, span)
		self.map.setRegion(region, animated: true)
		self.map.showsUserLocation = true
	}
	
	@IBAction func addPlace(_ sender: Any) {
		let feedbackController = UIAlertController(title: "Add Place", message: "", preferredStyle: .alert)
		
		feedbackController.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = "Place Name"
			textField.textAlignment = .center
		}
		
		let sendAction = UIAlertAction(title: "Name", style: .default, handler: {
			alert -> Void in
			
			
			let firstTextField = feedbackController.textFields![0] as UITextField
			
			let plc = Place(Database.getNextPlaceId(), firstTextField.text!, lat: Double(User.currentLocation[0]), long: Double(User.currentLocation[1]), radius: 20)
			
			User.places.append(plc)
			
			plc.save()
			
			self.pinPlace(plc)
			
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		feedbackController.addAction(cancelAction)
		
		feedbackController.addAction(sendAction)
		
		self.present(feedbackController, animated: true, completion: nil)
	}
	
	
	
	@IBAction func openPyle(_ sender: Any) {
		
		Ecosystem.Pyle("")
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
//	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//		return EcoController.localHistory().count
//	}
//	
//	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//		let cell = tableView.dequeueReusableCell(withIdentifier: "LocalHistoryCell", for: indexPath as IndexPath) as! LocalHistoryCell
//		
//		cell.descriptionLabel.text = EcoController.localHistory()[(EcoController.localHistory().count-1) - indexPath.row]
//		
//		return cell
//	}
	
	@IBAction func clearLocalAccountHistory(_ sender: Any) {
		Ecosystem.clearLocalHistory()
	}
	
	
	func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
		let overlayRenderer : MKCircleRenderer = MKCircleRenderer(overlay: overlay);
		overlayRenderer.lineWidth = 1.0
		overlayRenderer.strokeColor = UIColor.red
		return overlayRenderer
	}
	
	func pinPlace(_ plc: Place) {
		let dropPin = MKPointAnnotation()
		dropPin.coordinate = CLLocationCoordinate2D(latitude: plc.lat, longitude: plc.long)
		dropPin.title = plc.name
		
		let rad = MKCircle(center: dropPin.coordinate, radius: plc.radius)
		
		self.map.add(rad)
		self.map.addAnnotation(dropPin)
	}
	
}

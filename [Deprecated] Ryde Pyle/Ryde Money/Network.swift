//
//  Network.swift
//  Dank Memes the Card Game
//
//  Created by Ryan Dean on 6/25/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import Foundation

class Network{
    static func dataOfJson(url: String) -> Array<Dictionary<String,String>>? {
        
        let data = NSData(contentsOf: NSURL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL)
        do {
			
            let jsonArray = try JSONSerialization.jsonObject(with: data! as Data, options: [JSONSerialization.ReadingOptions.mutableContainers, JSONSerialization.ReadingOptions.allowFragments]) as? Array<Dictionary<String, String>>
            
            
            //print("json := \(jsonArray!)")
            return jsonArray!;
            
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil;
        }
    }
    
    static func sendJson(url: String, data: String){
        
        let request = NSMutableURLRequest(url: NSURL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL)
        request.httpMethod = "POST"
        let postString = data
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            _ = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            //print("responseString = \(responseString)")
        }
        task.resume()
    }
}

//
//  LocalHistoryTable.swift
//  Ryde
//
//  Created by Ryan Dean on 1/19/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class LocalHistoryTable: UITableView, UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return Ecosystem.localHistory().count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "LocalHistoryCell", for: indexPath as IndexPath) as! LocalHistoryCell
		
		cell.descriptionLabel.text = Ecosystem.localHistory()[(Ecosystem.localHistory().count-1) - indexPath.row]
		
		cell.backgroundColor = UIColor.gray
		cell.descriptionLabel.textColor = UIColor.black
		
		cell.layer.cornerRadius = 5
		cell.layer.borderColor = UIColor.black.cgColor
		cell.layer.borderWidth = 1
		
		return cell
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		self.delegate = self
		self.dataSource = self
		
		self.backgroundColor = UIColor.clear
	}
}

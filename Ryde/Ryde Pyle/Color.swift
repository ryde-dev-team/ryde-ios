//
//  Color.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 12/27/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class Color {
	static let pText = UIColor(colorLiteralRed: (0/255), green: (0/255), blue: (0/255), alpha: 1)
	static let nText = UIColor(colorLiteralRed: (0/255), green: (0/255), blue: (0/255), alpha: 1)
	
	static let pBack = UIColor(colorLiteralRed: (0/255), green: (200/255), blue: (0/255), alpha: 1)
	static let nBack = UIColor(colorLiteralRed: (175/255), green: (0/255), blue: (0/255), alpha: 1)
	
	static let pChartBack = UIColor(colorLiteralRed: (25/255), green: (200/255), blue: (25/255), alpha: 1)
	static let nChartBack = UIColor(colorLiteralRed: (200/255), green: (25/255), blue: (25/255), alpha: 1)
	
	static let pChartHigh = UIColor(colorLiteralRed: (75/255), green: (200/255), blue: (75/255), alpha: 1)
	static let nChartHigh = UIColor(colorLiteralRed: (200/255), green: (75/255), blue: (75/255), alpha: 1)
	
	static let pChartLine = UIColor(colorLiteralRed: (75/255), green: (255/255), blue: (75/255), alpha: 1)
	static let nChartLine = UIColor(colorLiteralRed: (255/255), green: (75/255), blue: (75/255), alpha: 1)
	
	static let pChartFill = UIColor(colorLiteralRed: (75/255), green: (255/255), blue: (75/255), alpha: 0.25)
	static let nChartFill = UIColor(colorLiteralRed: (255/255), green: (75/255), blue: (75/255), alpha: 0.25)
	
	static let chartZero = UIColor(colorLiteralRed: (0/255), green: (0/255), blue: (0/255), alpha: 1.0)
	
	static let pButton = UIColor(colorLiteralRed: (75/255), green: (255/255), blue: (75/255), alpha: 0.7)
	static let nButton = UIColor(colorLiteralRed: (255/255), green: (75/255), blue: (75/255), alpha: 0.7)
	
	static let pListBack = UIColor(colorLiteralRed: (0/255), green: (175/255), blue: (0/255), alpha: 1)
	static let nListBack = UIColor(colorLiteralRed: (175/255), green: (0/255), blue: (0/255), alpha: 1)
	
	static let pListText = UIColor(colorLiteralRed: (75/255), green: (255/255), blue: (75/255), alpha: 1)
	static let nListText = UIColor(colorLiteralRed: (255/255), green: (75/255), blue: (75/255), alpha: 1)
	
	static let n_lineGraphBalance = UIColor(colorLiteralRed: (255/255), green: (75/255), blue: (75/255), alpha: 1.0)
	static let p_lineGraphBalance = UIColor(colorLiteralRed: (75/255), green: (255/255), blue: (75/255), alpha: 1.0)
}

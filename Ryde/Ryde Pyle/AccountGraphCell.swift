//
//  AccountTableVIewCell.swift
//  Ryde Money
//
//  Created by ryan.dean on 11/1/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class AccountGraphCell: UITableViewCell {
    @IBOutlet var accountGraph: LineGraphModule!
    @IBOutlet weak var accountName: UILabel!
	@IBOutlet var accountBalance: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

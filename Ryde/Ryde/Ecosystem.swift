//
//  EcoController.swift
//  Ryde
//
//  Created by Ryan Dean on 1/18/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class Ecosystem {
	static let pyle = URL(string: "ryde-pyle://")
	static let ryde = URL(string: "ryde://")
	
	
	
	static func Pyle(_ arg: String!) {
		UIApplication.shared.open(pyle!, options: [:], completionHandler: { (success) in
			print("Open Pyle with Aruguments [" + arg + "]")
		})
	}
	
	static func Ryde(_ arg: String!) {
		UIApplication.shared.open(ryde!, options: [:], completionHandler: { (success) in
			print("Open Ryde with Aruguments [" + arg + "]")
		})
	}
	
	static func localHistory () -> [String] {
		if GroupData.get("rydeLocalHistory") == nil {
			return []
		}
		return GroupData.get("rydeLocalHistory") as! [String]
	}
	
	static func logLocal (_ text: String) {
		var his = localHistory()
		his.append(text)
		GroupData.set("rydeLocalHistory", data: his)
	}
	
	static func clearLocalHistory () {
		GroupData.clear("rydeLocalHistory")
	}
}

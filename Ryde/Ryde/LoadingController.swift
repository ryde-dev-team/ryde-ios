//
//  ViewController.swift
//  Ryde
//
//  Created by Ryan Dean on 1/17/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class LoadingController: UIViewController {
	
	var loadingView: LoadingView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		loadingView = self.view as! LoadingView
		
		self.loadingView.view.backgroundColor = Color.pBack
	}
	
	func setText(_ text: String) {
		self.loadingView.descriptionLabel.text = text
		self.loadingView.descriptionLabel.setNeedsDisplay()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		setText("Verifying Database Session")
		User.rydeid = Database.verifySesssion()
		
		if User.rydeid > -1 {
			setText("Key Verified - Loading User")
			User.load()
			setText("User Loaded")
			LoadingController.setNav(1)
		} else {
			setText("No Key Cache Found - Login Required")
			LoadingController.setNav(0)
		}
		navigate(GroupData.get("rydeNav") as! Int?)
		GroupData.clear("rydeNav")
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	func navigate(_ id: Int?) {
		var id = id
		if id == nil {
			id = 0
		}
		
		switch id! {
		case 1:
			goToDash()
		default:
			goToLogin()
		}
		
		
	}
	
	func goToLogin () {
		Ecosystem.logLocal("Go to Login")
		self.performSegue(withIdentifier: "login", sender: self)
	}
	
	func goToDash () {
		Ecosystem.logLocal("Go to Dash")
		self.performSegue(withIdentifier: "dash", sender: self)
	}
	
	static func setNav(_ nav: Int) {
		GroupData.set("rydeNav", data: nav)
	}
}


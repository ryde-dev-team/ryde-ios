//
//  Format.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 1/13/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class Currency {
	static func format(_ string: String) -> String?{
		let numberFromField = Double(string) ?? Double(0)
		return formatter().string(from: NSNumber(value: numberFromField))
	}
	
	static func remove(_ string:String) -> Double? {
		if !string.contains("$") {
			return NSNumber(value: Double(string) ?? Double(0)) as Double?
		} else {
			return (formatter().number(from: string) ?? 0) as Double?
		}
	}
	
	static func validate(_ text: String?) -> String{
		if text == nil {
			return "$0.00"
		} else {
			if text! == "" || text! == "$" {
				return "$0.00"
			} else if remove(text!) == nil {
				return "$0.00"
			} else {
				return text!
			}
		}
	}
	
	static func formatter () -> NumberFormatter {
		let formatter = NumberFormatter()
		formatter.numberStyle = NumberFormatter.Style.currency
		formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
		formatter.currencySymbol = "$"
		formatter.decimalSeparator = "."
		return formatter
	}
}

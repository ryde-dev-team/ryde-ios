//
//  Login.swift
//  Ryde
//
//  Created by Ryan Dean on 1/18/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit

class LoginView: UIView {
	
	@IBOutlet var view: UIView!
	@IBOutlet var logoLabel: UILabel!
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var emailField: UITextField!
	@IBOutlet var passwordField: UITextField!
	@IBOutlet var loginButton: UIButton!
	@IBOutlet var loginIssueButton: UIButton!
	@IBOutlet var motdLabel: UILabel!
	@IBOutlet var versionLabel: UILabel!
	
	var access: Int!
	
	var skip = false
	
	var nav = false
	var feedback: UIAlertController!
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		_ = Bundle.main.loadNibNamed("LoginView", owner: self, options: nil)![0] as! UIView
		self.addSubview(view)
		view.frame = self.bounds
	}
	
	
	@IBAction func loginPressed(_ sender: Any) {
		//self.view.endEditing(true)
		if User.login(username: self.emailField.text!, password: self.passwordField.text!) {
			if access >= User.authority {
				animOut()
			} else {
				//user not high enough authority
				self.motdLabel.text = "Your user account has not been given high enough access authority for this version, try updating or logging in again."
			}
		} else {
			self.logoLabel.text = "Try Again"
			Ecosystem.logLocal("Failed Login")
		}
	}
	
	@IBAction func loginIssuesPressed(_ sender: Any) {
		let feedbackController = UIAlertController(title: "Report Login Issue", message: "", preferredStyle: .alert)
		
		feedbackController.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = "Message"
			textField.textAlignment = .center
		}
		
		let sendAction = UIAlertAction(title: "Send", style: .default, handler: {
			alert -> Void in
			
			
			let firstTextField = feedbackController.textFields![0] as UITextField
			
			Database.sendFeedback(firstTextField.text!)
			
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		feedbackController.addAction(cancelAction)
		
		feedbackController.addAction(sendAction)
		
		feedback = feedbackController
	}
	
	func checkSystem () {
		var sysInfo = Database.systemVerify()
		
		self.access = Int(sysInfo![0])
		self.motdLabel.text = sysInfo![1]
		
		let verifyResponse = Database.verifySesssion()
		
		if verifyResponse == -1{
			skip = false
			User.doLogout = 1
		}
		
		if access == nil {
			skip = false
			User.doLogout = 1
			self.motdLabel.text = "This version of the app no longer has access, please update your app."
		}
		
		
		let infoString = Database.userInfo(verifyResponse)
		
		if infoString == nil {
			skip = false
			User.doLogout = 1
		} else {
			User.rydeid = verifyResponse
			User.name = infoString![0]
			User.authority = Int(infoString![1])!
			
			if access >= User.authority {
				User.doLogout = 0
				skip = true
			} else {
				skip = false
				User.doLogout = 1
				self.motdLabel.text = "Your user account has not been given high enough access authority for this version, try updating or logging in again."
			}
		}
	}
	
	
	func animIn () {
		UIView.animate(withDuration: 0.7, animations: { () -> Void in
			self.inFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1.5, animations: { () -> Void in
			self.inLast()
		}) { (Finished) -> Void in
			
		}
	}
	
	func animOut () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outFirst()
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			self.outLast()
		}) { (Finished) -> Void in
			self.nav = true
			//self.performSegue(withIdentifier: "login2root", sender: self)
		}
	}
	
	func inFirst () {
		self.titleLabel.frame.origin.y -= 800
		self.logoLabel.frame.origin.y -= 650
		
	}
	
	func inLast () {
		self.emailField.frame.origin.y  -= 600
		self.passwordField.frame.origin.y  -= 550
		self.loginButton.frame.origin.y  -= 500
		self.loginIssueButton.frame.origin.y -= 450
		self.versionLabel.frame.origin.y -= 400
		self.motdLabel.frame.origin.y -= 350
	}
	
	func outFirst () {
		self.titleLabel.frame.origin.y += 600
		self.logoLabel.frame.origin.y += 600
		self.loginButton.frame.origin.y  += 600
		self.loginIssueButton.frame.origin.y += 600
		self.versionLabel.frame.origin.y += 600
		self.motdLabel.frame.origin.y += 600
	}
	
	func outLast () {
		self.emailField.frame.origin.y  += 600
		self.passwordField.frame.origin.y  += 600
	}
}

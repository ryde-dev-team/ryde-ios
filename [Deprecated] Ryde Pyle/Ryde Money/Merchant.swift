//
//  Merchant.swift
//  Ryde Money
//
//  Created by Ryan Dean on 10/29/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import Foundation

class Merchant {
	var id = 0
	var name: String
	var location: [Float] = []
	
	
	init(name: String) {
		self.name = name
	}
	
	init(name: String, location:[Float]) {
		self.name = name
		self.location = location
	}
	
	func addLocation(location:[Float]) {
		self.location = location
	}
	
}

//
//  DebtView.swift
//  Ryde Money
//
//  Created by ryan.dean on 11/1/16.
//  Copyright © 2016 rydesoft. All rights reserved.
//

import UIKit

class BankView: UIViewController, UITableViewDataSource, UITableViewDelegate{
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		Timer.scheduledTimer(timeInterval: 1.0,
		                     target: self,
		                     selector: #selector(self.update),
		                     userInfo: nil,
		                     repeats: true)
		
		Timer.scheduledTimer(timeInterval: 0.1,
		                     target: self,
		                     selector: #selector(self.animate),
		                     userInfo: nil,
		                     repeats: true)
	}
	
	
	@IBOutlet var swipeRight: UISwipeGestureRecognizer!
	@IBOutlet weak var debtGraph: LineGraphModule!
	@IBOutlet weak var accountTable: UITableView!
	@IBOutlet var cashLabel: UILabel!
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var addBankButton: UIButton!
	
	var viewing = 0
	
	@IBAction func addBank(_ sender: Any) {
		let alertController = UIAlertController(title: "New Bank Account", message: "", preferredStyle: .alert)
		
		let checkingAction = UIAlertAction(title: "Checking", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertController.textFields![0] as UITextField
			
			let acc = Account(id: Database.getNextAccountId(), type: 0, name: firstTextField.text!, balance: 0, clear: 0, rec: 0)
			User.banks.append(acc)
			acc.save()
			self.accountTable.reloadData()
		})
		
		let savingsAction = UIAlertAction(title: "Savings", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertController.textFields![0] as UITextField
			
			let acc = Account(id: Database.getNextAccountId(), type: 1, name: firstTextField.text!, balance: 0, clear: 0, rec: 0)
			User.banks.append(acc)
			acc.save()
			self.accountTable.reloadData()
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		
		alertController.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = "Account Name"
			textField.textAlignment = .center
		}
		
		alertController.addAction(checkingAction)
		alertController.addAction(savingsAction)
		alertController.addAction(cancelAction)
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		swipeRight.addTarget(self, action: #selector(backToRoot(_:)))
		self.view.addGestureRecognizer(swipeRight)
		self.view.isUserInteractionEnabled = true
		
		self.addBankButton.backgroundColor = UIColor.gray
		
		addBankButton.layer.shadowColor = UIColor.black.cgColor
		addBankButton.layer.shadowOpacity = 0.5
		addBankButton.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
		addBankButton.layer.shadowRadius = 4
		addBankButton.layer.cornerRadius = 5
		
		accountTable.delegate = self
		accountTable.dataSource = self
		
		update()
	}
	
	func animate() {
		if !debtGraph.idle {
			debtGraph.animate()
			return
		}
		
		
		let accounts: [Account] = User.banks
		
		for i in 0..<accounts.count {
			if accountTable.cellForRow(at: [0,i]) != nil {
				if !(accountTable.cellForRow(at: [0,i]) as! AccountGraphCell).accountGraph.idle {
					(accountTable.cellForRow(at: [0,i]) as! AccountGraphCell).accountGraph.animate()
					return
				}
				
			}
		}
		
	}
	
	var firstIn = true
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if firstIn {
			animIn()
			firstIn = false
		} else {
			animIn()
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		accountTable.reloadData()
		accountTable.setNeedsDisplay()
		
		//run code on enter forground in app delegate
	}
	
	@IBAction func payDebt(_ sender: Any) {
		
		accountTable.reloadData()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return User.banks.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "AccountGraphCell", for: indexPath as IndexPath) as! AccountGraphCell
		
		if indexPath.row < User.banks.count {
			
			let debt = User.banks[indexPath.row]
			
			cell.accountName.text = debt.name + ""
			
			cell.accountBalance.text = Currency.format(debt.balance.description)
			
			cell.accountGraph.updateData([debt], balance: debt.balance)
		} else {
			let debt = User.banks[indexPath.row - User.debts.count]
			
			cell.accountName.text = debt.name + ""
			
			cell.accountBalance.text = Currency.format(debt.balance.description)
			
			cell.accountGraph.updateData([debt], balance: debt.balance)
		}
		
		if User.capital < 0 {
			cell.backgroundColor = Color.nBack
		} else {
			cell.backgroundColor = Color.pBack
		}
		
		cell.layer.masksToBounds = false
		cell.clipsToBounds = false
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		GroupData.set("accountType", data: "0")
		GroupData.set("accountNum", data: indexPath[1].description)
		self.animAccount()
	}
	
	func update() {
		User.update()
		
		if debtGraph != nil {
			debtGraph.updateData(User.banks, balance: User.capital)
		}
		
		cashLabel.text = Currency.format(User.capital.description)
		
		if User.capital < 0 {
			cashLabel.textColor = Color.nText
			self.view.backgroundColor = Color.nBack
			accountTable.backgroundColor = Color.nBack
		} else {
			cashLabel.textColor = Color.pText
			self.view.backgroundColor = Color.pBack
			accountTable.backgroundColor = Color.pBack
		}
		
		accountTable.setNeedsDisplay()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	@IBAction func viewBank(_ sender: Any) {
		
	}
	
	
	func backToRoot(_ sender: AnyObject) {
		animOut()
	}
	
	func inFirst () {
		self.debtGraph.frame.origin.x -= self.view.frame.width
		self.accountTable.frame.origin.x -= self.view.frame.width
		
		
		self.addBankButton.frame.origin.x -= self.view.frame.width
	}
	
	func inLast () {
		self.cashLabel.frame.origin.x -= self.view.frame.width
		self.titleLabel.frame.origin.x -= self.view.frame.width
	}
	
	func outFirst () {
		self.cashLabel.frame.origin.x += self.view.frame.width
		self.titleLabel.frame.origin.x += self.view.frame.width
	}
	
	func outLast () {
		self.debtGraph.frame.origin.x += self.view.frame.width
		self.accountTable.frame.origin.x += self.view.frame.width
		
		
		self.addBankButton.frame.origin.x += self.view.frame.width
	}
	
	
	func animIn () {
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			if self.viewing == 0 {
				self.inFirst()
			} else {
				self.outFirst()
			}
			
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 1, animations: { () -> Void in
			if self.viewing == 0 {
				self.inLast()
			} else {
				self.outLast()
			}
		}) { (Finished) -> Void in
			self.viewing = 0
			self.view.layoutSubviews()
		}
	}
	
	func animOut () {
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.outFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.outLast()
			
		}) { (Finished) -> Void in
			self.dismiss(animated: false, completion: nil)
		}
		
		
	}
	
	func animAccount () {
		viewing = 1
		UIView.animate(withDuration: 0.3, animations: { () -> Void in
			self.inFirst()
		}) { (Finished) -> Void in
			
		}
		
		UIView.animate(withDuration: 0.5, animations: { () -> Void in
			self.inLast()
			
		}) { (Finished) -> Void in
			self.performSegue(withIdentifier: "bank2transaction", sender: self)
		}
		
		
	}
}

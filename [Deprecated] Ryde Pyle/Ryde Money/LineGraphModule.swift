//
//  LineGraphModule.swift
//  Ryde Pyle
//
//  Created by Ryan Dean on 1/6/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import UIKit
import CoreGraphics

class LineGraphModule: UIView {
	var data: [CGFloat]!
	
	var minimumValue: CGFloat!
	var maximumValue: CGFloat!
	
	@IBOutlet var max: UILabel!
	@IBOutlet var min: UILabel!
	
	var highlighted: Bool = false
	var blinking: Bool = false
	var bouncing: Bool = false
	var redraw: Bool = true
	var idle: Bool = false
	
	var zeroLine: CAShapeLayer = CAShapeLayer()
	var lines: [CAShapeLayer] = []
	var fills: [CAShapeLayer] = []
	
	
	var height: CGFloat = 0
	var width: CGFloat = 0
	
	var hLineCount = 0
	var hSpacing: CGFloat = 0
	
	var vLineCount = 0
	var vSpacing: CGFloat = 0
	
	var zeroX: CGFloat = 0
	
	var drawCount = Float(0)
	
	var animTime = Float(25)
	
	var inititalLayerCount = 0
	
	var maximumDataCount = 100
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		
		self.layer.cornerRadius = 3.0
		self.layer.masksToBounds = false
		self.layer.shadowColor = UIColor.black.cgColor
		self.layer.shadowOpacity = 0.8
		self.layer.shadowOffset = CGSize(width: 0.0, height: -1.0)
		self.layer.shadowRadius = 3
		
		self.backgroundColor = UIColor.clear
		//self.clipsToBounds = true
		
	}
	
	override func didMoveToSuperview() {
		//print("yes")
		self.translatesAutoresizingMaskIntoConstraints = false
	}
	
	func updateData (_ accounts: [Account], balance: Double) {
		
		var fullHistory: [Transaction] = []
		for a in accounts {
			for h in a.transactions {
				if !h.hidden {
					fullHistory.append(h)
				}
			}
		}
		
		fullHistory.sort {$0.timestamp > $1.timestamp}
		
		while fullHistory.count > Int(maximumDataCount) {
			fullHistory.removeLast()
		}
		
		calculateData(list: fullHistory, currentBalance: balance)
	}
	
	func calculateData( list: [Transaction], currentBalance: Double) {
		var oldCount: Int!
		if data != nil {
			oldCount = data.count
		}
		
		data = []
		var adjBal: CGFloat = CGFloat(currentBalance)
		data.append(adjBal)
		for f in list {
			adjBal -= CGFloat(f.value)
			data.append(adjBal)
		}
		data.reverse()
		
		if oldCount != nil {
			if data.count != oldCount {
				clear()
				setNeedsDisplay()
				drawCount = 0
			}
		}
		
		if data.count <= 1 {
			self.layer.backgroundColor = UIColor.darkGray.cgColor
			if highlighted {
				self.layer.backgroundColor = UIColor.gray.cgColor
			}
			return
		}
		
		for n in data {
			if maximumValue == nil || minimumValue == nil {
				maximumValue = n
				minimumValue = n
			}
			if n > maximumValue {
				maximumValue = n
			}
			if n < minimumValue {
				minimumValue = n
			}
		}
		
		if Float(data[data.count-1]) < Float(0) {
			self.layer.backgroundColor = Color.nChartBack.cgColor
		} else {
			self.layer.backgroundColor = Color.pChartBack.cgColor
		}
		
		if highlighted {
			if Float(data[data.count-1]) < Float(0) {
				self.layer.backgroundColor = Color.nChartHigh.cgColor
			} else {
				self.layer.backgroundColor = Color.pChartHigh.cgColor
			}
		}
		
	}
	
	func animate() {
		if !redraw && !highlighted && !blinking && !bouncing {
			idle = true
			return
		} else {
			idle = false
		}
		
		//print("[GRAPH] Animate")
		if redraw {
			//print("[GRAPH] Redraw")
			stroke()
		}
		
	}
	
	override func draw(_ dirtyRect: CGRect) {
		width = dirtyRect.size.width
		height = dirtyRect.size.height
		
		if data != nil && maximumValue != nil && minimumValue != nil {
			if data.count > 0 {
				
				max.text = Currency.format(maximumValue.description)
				min.text = Currency.format(minimumValue.description)
				
				hLineCount = Int(maximumValue - minimumValue)
				vLineCount = data.count - 1
				
				hSpacing = width / CGFloat(vLineCount)
				vSpacing = height / CGFloat(hLineCount)
				
				zeroX = height + (minimumValue * vSpacing)
				
				
			}
		} else {
			max.text = ""
			min.text = ""
		}
		
		idle = false
		redraw = true
	}
	
	func reset () {
		fills = []
		lines = []
		drawCount = 0
	}
	
	func clear () {
		zeroLine.removeAllAnimations()
		zeroLine.removeFromSuperlayer()
		
		for l in lines {
			l.removeFromSuperlayer()
		}
		
		for l in fills {
			l.removeFromSuperlayer()
		}

	}
	
	func stroke () {
		
		if drawCount == 0 {
			//print("[GRAPH] Started stroking")
			clear()
			reset()
			drawZero()
			drawPath()
			drawFill()
			inititalLayerCount = layer.sublayers!.count
		}
		
		let i: Int = Int(layer.sublayers!.count - inititalLayerCount)
		
		//print("[GRAPH] Strokes completed: " + i.description)
		if i >= (lines.count + fills.count) {
			//print("[GRAPH] Completed stroking")
			redraw = false
			drawCount = 0
			//print("[GRAPH] setNeedsDisplay()")
			//setNeedsDisplay()
			return
		}
		
		var j = i
		//print("[GRAPH] Starting from stroke " + j.description)
		
		//print("line: " + j.description)
		
		while j <= Int(drawCount) {
			if j >= lines.count {
				//print("[GRAPH] setNeedsDisplay()")
				//setNeedsDisplay()
				break
			}
			
			let line = lines[j]
			
			
			
			let anim = CABasicAnimation(keyPath: "strokeEnd")
			anim.duration = Double(animTime / Float(10.0))
			anim.fromValue = 0
			anim.toValue = 1
			line.add(anim, forKey: "strokeEnd")
			
			
			//print("[GRAPH] [LINE " + j.description + "] Inserting to layer index 0")
			layer.insertSublayer(line, at: 0)
			j += 1
		}
		
		while j <= Int(drawCount) {
			let k = j - lines.count
			if k >= fills.count {
				//print("[GRAPH] setNeedsDisplay()")
				//setNeedsDisplay()
				break
			}
			
			let fill = fills[(fills.count - 1) - k]
			
			//print("[GRAPH] [FILL " + k.description + "] Inserting to layer index 0")
			layer.insertSublayer(fill, at: 0)
			j += 1
		}
		
		if j < lines.count {
			drawCount += Float(Float(lines.count + fills.count) / animTime)
		} else {
			drawCount += Float(Float(lines.count + fills.count) / (animTime/Float(3.0)))
		}
		
		//drawCount += Float(Float(lines.count + fills.count) / animTime)
		//print("[GRAPH] Increase drawCount to " + drawCount.description)
		
	}
	
	func drawZero () {
		if zeroX < 0 || zeroX > height {
			return
		}
		
		let zeroPath = UIBezierPath()
		
		zeroPath.lineCapStyle = .butt
		zeroPath.move(to: CGPoint(x: width, y: zeroX))
		zeroPath.addLine(to: CGPoint(x: 0, y: zeroX))
		
		let dashes: [NSNumber] = [5,5]
		let layer = CAShapeLayer()
		
		layer.frame = self.bounds
		layer.path = zeroPath.cgPath
		layer.strokeColor = Color.chartZero.cgColor
		layer.lineWidth = 1.0
		layer.lineDashPattern = dashes
		layer.lineDashPhase = 0.0
		
		let anim = CABasicAnimation(keyPath: "strokeEnd")
		anim.duration = Double(animTime / Float(10.0))
		anim.fromValue = 0
		anim.toValue = 1
		layer.add(anim, forKey: "strokeEnd")
		
		zeroLine = layer
		
		self.layer.insertSublayer(zeroLine, at: 0)
	}
	
	func drawPath () {
		let layer = CAShapeLayer()
		
		for i in 0..<vLineCount {
			if i >= data.count - 1 {
				break
			}
			
			let path = UIBezierPath()
			
			path.lineCapStyle = .butt
			path.move(to: CGPoint(x: xFor(i+1),y: yFor(i+1)))
			path.addLine(to: CGPoint(x: xFor(i), y: yFor(i)))
			
			layer.frame = self.bounds
			if layer.path != nil {
				path.append(UIBezierPath(cgPath: layer.path!))
			}
			layer.path = path.cgPath
			layer.lineWidth = 1.0
			layer.shadowColor = UIColor.black.cgColor
			layer.shadowOpacity = 1.0
			layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
			layer.shadowRadius = 3
			
			//THIS IS OUR ISSUE WITH THE LINES
			
			if data[data.count-1] < CGFloat(0) {
				layer.strokeColor = Color.nChartLine.cgColor
			} else {
				layer.strokeColor = Color.pChartLine.cgColor
			}
			
			
			
//			if Float(data[i]) > Float(data[i+1]) {
//				layer.strokeColor = Color.nChartLine.cgColor
//				if i > 0 {
//					if Float(data[i-1]) < Float(data[i]) {
//						lines.append(layer)
//						layer = CAShapeLayer()
//					} else {
//						lines.append(layer)
//						layer = CAShapeLayer()
//					}
//				} else {
//					lines.append(layer)
//					layer = CAShapeLayer()
//				}
//			} else {
//				layer.strokeColor = Color.pChartLine.cgColor
//				if i > 0 {
//					if Float(data[i-1]) < Float(data[i]) {
//						lines.append(layer)
//						layer = CAShapeLayer()
//					} else {
//						lines.append(layer)
//						layer = CAShapeLayer()
//					}
//				} else {
//					lines.append(layer)
//					layer = CAShapeLayer()
//				}
//			}
			
			
		}
		
		if layer.path != nil {
			lines.append(layer)
		}
	}
	
	func drawFill () {
		let fillBuffer: CGFloat = 0.0
		var layer = CAShapeLayer()
		
		for i in 0..<vLineCount {
			if i >= data.count - 1 {
				break
			}
			
			let path = UIBezierPath()
			
			path.lineCapStyle = .butt
			path.move(to: CGPoint(x: xFor(i+1),y: yFor(i+1) - fillBuffer))
			if zeroX < 0 {
				path.addLine(to: CGPoint(x: xFor(i+1), y: 0))
				path.addLine(to: CGPoint(x: xFor(i), y: 0))
			} else if zeroX > height {
				path.addLine(to: CGPoint(x: xFor(i+1), y: height))
				path.addLine(to: CGPoint(x: xFor(i), y: height))
			} else {
				path.addLine(to: CGPoint(x: xFor(i+1), y: zeroX))
				path.addLine(to: CGPoint(x: xFor(i), y: zeroX))
			}
			
			path.addLine(to: CGPoint(x: xFor(i), y: yFor(i)))
			
			
			
			layer.frame = self.bounds
			if layer.path != nil {
				path.append(UIBezierPath(cgPath: layer.path!))
			}
			layer.path = path.cgPath
			layer.lineWidth = 1.0
			layer.strokeColor = nil
			
			if Float(data[i]) > Float(data[i+1]) {
				layer.fillColor = Color.nChartFill.withAlphaComponent(CGFloat(Float(1.3) * Float(Float(Float(i) * 0.7)/Float(data.count)))).cgColor
				if i > 0 {
					if Float(data[i-1]) < Float(data[i]) {
						fills.append(layer)
						layer = CAShapeLayer()
					} else {
						fills.append(layer)
						layer = CAShapeLayer()
					}
				} else {
					fills.append(layer)
					layer = CAShapeLayer()
				}
			} else {
				layer.fillColor = Color.pChartFill.withAlphaComponent(CGFloat(Float(1.3) * Float(Float(Float(i) * 0.7)/Float(data.count)))).cgColor
				if i > 0 {
					if Float(data[i-1]) < Float(data[i]) {
						fills.append(layer)
						layer = CAShapeLayer()
					} else {
						fills.append(layer)
						layer = CAShapeLayer()
					}
				} else {
					fills.append(layer)
					layer = CAShapeLayer()
				}
			}
			
		}
		if layer.path != nil {
			fills.append(layer)
		}
	}
	
	func xFor (_ i: Int) -> CGFloat {
		return CGFloat(Float(i) * Float(hSpacing))
	}
	
	func yFor (_ i: Int) -> CGFloat {
		return CGFloat(zeroX - CGFloat(Float(self.data[i]) * Float(vSpacing)))
	}
	
}

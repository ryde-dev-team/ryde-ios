//
//  Place.swift
//  Ryde
//
//  Created by Ryan Dean on 1/21/17.
//  Copyright © 2017 rydesoft. All rights reserved.
//

import Foundation
import MapKit

class Place {
	var id: Int!
	var name: String!
	var lat: Double!
	var long: Double!

	var radius: CLLocationDistance!
	
	init(_ id: Int, _ name: String, lat: Double, long: Double, radius: Double) {
		self.name = name
		self.lat = lat
		self.long = long
		self.id = id
		
		self.radius = radius as CLLocationDistance
	}
	
	func save () {
		Ecosystem.logLocal("Saving Place [" + id.description + "] " + name)
		
		Database.savePlace(self)
	}
}
